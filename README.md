# Private API

Private API for the project Věnná Města Českých Královen.

## Swagger

https://app.swaggerhub.com/apis/vancudan/VMCK-Private-API/3.0.1

## Known issues

- AfterInsert methods in Entities seems to be not working.
Aka it seems than AfterInsert method only updates the js object
but this change is not propagated into database.
Hence the method currently used in XYModel.create() is used
(create, populate these columns and save changes => 2 commits)

- migration:generate will try to put something like these in
every single generated migration, watch out for that.
As of the moment of writing this (29th June, 2020) this issue was found being
reported in 2018 and even 3 days ago, looks like solution is still pending
(or issue is not to be solved at all).

up method:

`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'::real[]`
`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'::real[]`

down method:

`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'`
`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'`

- if you try to use .attach() function in tests to test file upload,
and you misspell the filename (or do it on purpose), the tests fill just fall on timeout.
No error message whatsoever.

- when using {x,y,z} = await something(); (such as in TDObjectController ->
const {...} = await this.validate3DObjectUpload();)
the names declared in const {...} MUST match those in the function.
If that's not the case an "unhandled error" will show up when running tests.
Two of those errors will cause the whole app to crash.

- when there's an unused variable/etc. tests also throw 'unhandled error' - 2 of which causes a crash
    -> basically whenever there's any kind of error the tests just crashes, would be nice to solve 

- when returning a result in route, `json()` (or `send()`) method has to be called,
if only `status()` is called the response is not sent and tests will crash on timeout

- when running tests one by one, they all run smoothly. But once they're ran all at once
 via `docker-compose exec private-api yarn test`, there's a massive crash on:
 `QueryFailedError: relation "migrations" already exists`

- while using empty array as validators - [] - the Route is never found
and any call in tests returns 404, some validator has to be present every time

- uploading the exactly same file (Model/Texture/Asset) in multiple occasions is a massive error.
Should not be done ever, api SHOULD return sth like 400.

- ObjectId in typeorm and ObjectId imported from mongodb are two different classes with the same name.
aka quite a few things won't work. Currently this is to be seen in `TextureModel->getById()`

- creating an Entity with primaryKey of type ObjectId (varchar in postgres) causes each new record
to be created with id enclosed in quotes. i.e id `5f0380be4322970b40ce3b43` would be 
saved in the database as `"5f0380be4322970b40ce3b43"`.
which would cause the comparison of the strings to fail, hence not finding
the record in validation (getById()) and thus failing on Unique constraint