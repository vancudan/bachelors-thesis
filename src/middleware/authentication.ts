import { RequestHandler } from 'express';
import { verify } from 'jsonwebtoken';
import { User } from '../entity/User';
import UserModel from '../models/UserModel';

type Authenticator = (secret?: string) => RequestHandler;

interface TokenData {
    username: string;
}

const errorMessage = 'Access token is missing or invalid';

const authentication: Authenticator = (secret) => async (request, response, next) => {
    const authorizationHeader = request.header('Authorization');
    if (!secret || !authorizationHeader) return response.status(401).send(errorMessage);

    try {
        const token = authorizationHeader.replace('Bearer ', '');
        const { username } = verify(token, secret) as TokenData;
        const userModel = new UserModel();
        const user: User | undefined = await userModel.getByUsername(username);
        if (user === undefined) fail();
        request.user = user;
        request.token = token;
        next();
    } catch (error) {
        response.status(401).send(errorMessage);
    }
};

export default authentication;
