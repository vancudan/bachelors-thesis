import { getRepository } from 'typeorm';
import { Effect } from '../entity/Effect';

export default class EffectModel {
    private readonly effectRepository = getRepository(Effect);

    public getById(id: number) {
        return this.effectRepository.findOne(id);
    }

    public getAll() {
        return this.effectRepository.find();
    }
}
