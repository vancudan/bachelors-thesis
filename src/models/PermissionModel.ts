import { getRepository } from 'typeorm';
import { Permission } from '../entity/Permission';

export default class PermissionModel {
    private readonly permissionRepository = getRepository(Permission);

    public getById(id: number) {
        return this.permissionRepository.findOne(id);
    }

    public getAll() {
        return this.permissionRepository.find();
    }
}
