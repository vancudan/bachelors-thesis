import { getRepository } from 'typeorm';
import { Comment } from '../entity/Comment';

export default class CommentModel {
    private readonly commentRepository = getRepository(Comment);

    public getById(id: number) {
        return this.commentRepository.findOne(id);
    }

    public getAll() {
        return this.commentRepository.find();
    }
}
