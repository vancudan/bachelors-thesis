import { sign } from 'jsonwebtoken';
import { getRepository } from 'typeorm';
import { User, UserStatus } from '../entity/User';

export default class UserModel {
    private readonly userRepository = getRepository(User);

    public async create(name: string, username: string): Promise<User> {
        // Create JWT
        const secret = process.env.JWT_SECRET!;
        const token = sign({ username }, secret);
        // create user with it
        return this.userRepository.save({ username, token, name });
    }

    public getByUsername(username: string) {
        return this.userRepository.findOne(username);
    }

    public getAll() {
        return this.userRepository.find();
    }

    public update(user: User, name: string, username: string, role: string, status: string) {
        return this.userRepository.update({ username: user.username }, { username, name, role, status });
    }

    public delete(user: User) {
        user.status = UserStatus.DELETED;
        return this.userRepository.update({ username: user.username }, user);
    }
}
