import fs from 'fs';
import md5File from 'md5-file';
import { Collection, Db, GridFSBucket, MongoClient, ObjectID } from 'mongodb';
import Inject from '../di/Inject';
import FileNotFoundError from './errors/FileNotFoundError';

export interface FileData {
    path: string;
    name: string;
}

export interface File {
    _id: ObjectID;
    length: number;
    chunkSize: number;
    uploadDate: Date;
    filename: string;
    md5: string; // @deprecated
}

export interface FileInfo {
    id: ObjectID;
    filename: string;
    uploadDate: Date;
}

const fileInfo = ({ _id, filename, uploadDate }: File): FileInfo => ({ id: _id, filename, uploadDate });

@Inject(MongoClient)
export default class FileModel {
    private readonly db: Db;
    private readonly bucket: GridFSBucket;
    private readonly filesCollection: Collection<File>;

    constructor(private readonly client: MongoClient) {
        this.db = this.client.db();
        this.bucket = new GridFSBucket(this.db);
        this.filesCollection = this.db.collection('fs.files');
    }

    private md5File(filepath: string) {
        return new Promise<string>((resolve, reject) =>
            md5File(filepath, (err, hash) => {
                if (err) reject(err);
                resolve(hash);
            }),
        );
    }

    // TODO rename to findOrCreate and make a 'create' method as in other models
    public async save(file: FileData) {
        const md5 = await this.md5File(file.path);
        const existingFile = await this.filesCollection.findOne({ md5 });
        if (!existingFile) {
            return fileInfo(
                await new Promise<File>((resolve, reject) =>
                    fs
                        .createReadStream(file.path)
                        .pipe(this.bucket.openUploadStream(file.name))
                        .on('error', reject)
                        .on('finish', resolve),
                ),
            );
        }
        return fileInfo(existingFile);
    }

    public async load(id: string) {
        const { id: _id, filename } = await this.get(id);
        return {
            filename,
            stream: this.bucket.openDownloadStream(_id),
        };
    }

    public async get(id: string) {
        const file = await this.filesCollection.findOne(ObjectID.createFromHexString(id));
        if (!file) throw new FileNotFoundError();
        return fileInfo(file);
    }
}
