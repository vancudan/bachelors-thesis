import { ObjectID } from 'mongodb';
import { getRepository } from 'typeorm';
import { Texture } from '../entity/Texture';
import { User } from '../entity/User';
import { TexturesUrl } from '../routes/Urls';

export default class TextureModel {
    private readonly textureRepository = getRepository(Texture);

    public async create(filename: string, fileSize: number, id: ObjectID, user: User): Promise<Texture> {
        // TODO this searches for a unique pair filename-fileSize, needs fix
        const texture = await this.textureRepository.save({ id: id.toHexString(), filename, fileSize, user });
        texture.href = `${TexturesUrl}/${texture.id}`;
        await this.textureRepository.update({ id: texture.id }, { href: `${TexturesUrl}/${texture.id}` });
        return texture;
    }

    public getById(id: string) {
        return this.textureRepository.findOne({
            where: {
                id,
            },
        });
    }

    public getAll() {
        return this.textureRepository.find();
    }

    public update(object: Texture) {
        return this.textureRepository.update({ id: object.id }, object);
    }

    public delete(id: string) {
        return this.textureRepository.delete({ id });
    }
}
