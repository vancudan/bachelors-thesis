import { getRepository } from 'typeorm';
import { Chunk } from '../entity/Chunk';

export default class ChunkModel {
    private readonly chunkRepository = getRepository(Chunk);

    public getById(id: number) {
        return this.chunkRepository.findOne(id);
    }

    public getAll() {
        return this.chunkRepository.find();
    }
}
