import { getRepository } from 'typeorm';
import { Asset } from '../entity/Asset';
import { Structure } from '../entity/Structure';
import { User } from '../entity/User';
import { StructureUrl } from '../routes/Urls';

export default class StructureModel {
    private readonly structureRepository = getRepository(Structure);

    public async create(city: string, name: string, description: string, location: number[] | null, user: User): Promise<Structure> {
        const structure = await this.structureRepository.save({ city, name, description, location, user });
        structure.href = `${StructureUrl}/${structure.id}`;
        // TODO needs fix - this can't go on
        structure.allVariants = [];
        structure.assets = [];
        await this.structureRepository.update({ id: structure.id }, { href: structure.href });
        return structure;
    }

    public getById(id: string) {
        return this.structureRepository.findOne(id);
    }

    public getAll(publicStructure: boolean) {
        return this.structureRepository.find({
            where: { public: publicStructure },
            order: { name: 'ASC' },
        });
    }

    public getByUsername(username: string, publicStructure: boolean) {
        return this.structureRepository.find({
            where: { username, public: publicStructure },
            order: { name: 'ASC' },
        });
    }

    public delete(id: string) {
        /*
         * TODO check assets if they should be deleted as well
         *  or are the objects relations deleted with the record?
         * */
        return this.structureRepository.delete({ id });
    }

    public async update(structure: Structure) {
        // if not done this way the update would try to change 'allVariants' - not an entity column -> throws error
        const { allVariants, assets, ...entityColumns } = structure;
        /*
         * updating ManyToMany relationship is a pain
         * both OneToMany and ManyToOne contains method 'set'
         * though ManyToMany only has 'add' + 'addAndRemove'
         * so we're using it as follows:
         * take the former assets that were added and -replace- them with new ones
         * */

        /*
         * getting former assets
         * */
        const formerAssets: Asset[] = await this.structureRepository
            .createQueryBuilder()
            .relation(Structure, 'assets')
            .of(structure)
            .loadMany();

        /*
         * setting new assets
         * */
        await this.structureRepository
            .createQueryBuilder()
            .relation(Structure, 'assets')
            .of(structure)
            .addAndRemove(assets, formerAssets);

        /*
         * return info about change
         * */
        return this.structureRepository.update({ id: structure.id }, entityColumns);
    }
}
