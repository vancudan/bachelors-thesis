import { getRepository } from 'typeorm';
import { Asset } from '../entity/Asset';
import { Model } from '../entity/Model';
import { TDObject, TDObjectStatus } from '../entity/TDObject';
import { Texture } from '../entity/Texture';
import { User, UserRole } from '../entity/User';
import { TDObjectsUrl } from '../routes/Urls';

const olderVersionsRecursiveQuery = `
WITH RECURSIVE "TDObject_list" AS (
    SELECT
        "id",
        "name",
        "current",
        "createdDate",
        "href",
        "version",
        "structureId",
        "transformation",
        "username",
        "previousId",
        "nextId"
    FROM "TDObject" WHERE id = $1
    UNION
    SELECT
        current."id",
        current."name",
        current."current",
        current."createdDate",
        current."href",
        current."version",
        current."structureId",
        current."transformation",
        current."username",
        current."previousId",
        current."nextId"
    FROM "TDObject" current
        INNER JOIN "TDObject_list" previous ON previous."previousId" = current."id"
        WHERE previous."previousId" IS NOT NULL
) SELECT * FROM "TDObject_list" ORDER BY version DESC;
`;

const newerVersionsRecursiveQuery = `
WITH RECURSIVE "TDObject_list" AS (
    SELECT
        "id",
        "name",
        "current",
        "createdDate",
        "href",
        "version",
        "structureId",
        "transformation",
        "username",
        "previousId",
        "nextId"
    FROM "TDObject" WHERE id = $1
    UNION
    SELECT
        next."id",
        next."name",
        next."current",
        next."createdDate",
        next."href",
        next."version",
        next."structureId",
        next."transformation",
        next."username",
        next."previousId",
        next."nextId"
    FROM "TDObject" next
        INNER JOIN "TDObject_list" current ON next."id" = current."nextId"
        WHERE current."nextId" IS NOT NULL
) SELECT * FROM "TDObject_list" ORDER BY version DESC;
`;

export default class TDObjectModel {
    private readonly objectRepository = getRepository(TDObject);

    public async create(name: string, structureId: string, model: Model, user: User): Promise<TDObject> {
        const tdobject = await this.objectRepository.save({ name, structureId, model, user });
        tdobject.href = `${TDObjectsUrl}/${tdobject.id}`;
        // TODO fix this ugly ****
        tdobject.assets = [];
        tdobject.textures = [];
        await this.objectRepository.update({ id: tdobject.id }, { href: tdobject.href });
        return tdobject;
    }

    public getAll(): Promise<TDObject[]> {
        return this.objectRepository.find({
            where: { current: true },
            order: { name: 'ASC' },
        });
    }

    public getById(id: string): Promise<TDObject | undefined> {
        return this.objectRepository.findOne(id);
    }

    /* public getByFormat(format: TDObjectFormat) {
        return this.objectRepository.find({ where: { format, current: true } });
    }*/

    public getByUsername(username: string): Promise<TDObject[]> {
        return this.objectRepository.find({
            where: { username, current: true },
            order: { name: 'ASC' },
        });
    }

    public getWaitingForApproval(user: User): Promise<TDObject[]> {
        let result: Promise<TDObject[]>;

        if (user.role === UserRole.APPROVER) {
            result = this.objectRepository.find({
                where: [
                    { current: true, isApprovedByApprover: false, status: TDObjectStatus.SUBMITTED },
                    { current: true, isApprovedByApprover: false, status: TDObjectStatus.FIXED },
                ],
                order: { name: 'ASC' },
            });
        } else if (user.role === UserRole.HISTORIAN) {
            result = this.objectRepository.find({
                where: [
                    { current: true, isApprovedByHistorian: false, status: TDObjectStatus.SUBMITTED },
                    { current: true, isApprovedByHistorian: false, status: TDObjectStatus.FIXED },
                ],
                order: { name: 'ASC' },
            });
        } else if (user.role === UserRole.ADMIN) {
            result = this.objectRepository.find({
                where: [
                    { current: true, status: TDObjectStatus.SUBMITTED },
                    { current: true, status: TDObjectStatus.FIXED },
                ],
                order: { name: 'ASC' },
            });
        } else {
            // user.status === MODELLER
            result = this.objectRepository.find({
                where: [
                    { current: true, status: TDObjectStatus.UNFINISHED },
                    { current: true, status: TDObjectStatus.DECLINED },
                ],
                order: { name: 'ASC' },
            });
        }

        return result;
    }

    /**
     * @returns 'raw' SQL results, hence only previousId and nextId are present, not previous or next objects themselves
     */
    public async getAllVersions(object: TDObject): Promise<TDObject[]> {
        const olderVersions: TDObject[] = await this.objectRepository.query(olderVersionsRecursiveQuery, [object.id]);
        const newerVersions: TDObject[] = await this.objectRepository.query(newerVersionsRecursiveQuery, [object.id]);
        /*
         * olderVersions finds the object (param) and all its older variants, sorted (DESC) by version
         * newerVersions finds the object (param) and all its newer variants, sorted (DESC) by version
         * aka first obj in olderVersions and last obj in newerVersions are the same -> object (param)
         *
         * => remove the identical object and concat the arrays and we have full object history
         * */
        newerVersions.pop();
        return newerVersions.concat(olderVersions);
    }

    public delete(id: string) {
        return this.objectRepository.delete({ id });
    }

    public async update(tdObject: TDObject) {
        /*
         * removing non-column values -> required for sql update -> 'no column called "next"'
         * */
        const { assets, textures, model, structure, user, next, previous, ...entityColumns } = tdObject;

        /*
         * ! same as in StructureModel.ts !
         *
         * updating ManyToMany relationship is a pain
         * both OneToMany and ManyToOne contains method 'set'
         * though ManyToMany only has 'add' + 'addAndRemove'
         * so we're using it as follows:
         * take the former assets and textures that were added and -replace- them with new ones
         * */

        /*
         * getting former assets
         * */
        const formerAssets: Asset[] = await this.objectRepository
            .createQueryBuilder()
            .relation(TDObject, 'assets')
            .of(tdObject)
            .loadMany();

        /*
         * setting new assets
         * */
        await this.objectRepository
            .createQueryBuilder()
            .relation(TDObject, 'assets')
            .of(tdObject)
            .addAndRemove(assets, formerAssets);

        /*
         * getting former textures
         * */
        const formerTextures: Texture[] = await this.objectRepository
            .createQueryBuilder()
            .relation(TDObject, 'textures')
            .of(tdObject)
            .loadMany();

        /*
         * setting new textures
         * */
        await this.objectRepository
            .createQueryBuilder()
            .relation(TDObject, 'textures')
            .of(tdObject)
            .addAndRemove(textures, formerTextures);

        /*
         * return info about change
         * */
        return this.objectRepository.update({ id: tdObject.id }, entityColumns);
    }

    public async createNextVersion(previousObject: TDObject, newUser: User, newModel: Model) {
        const { id, createdDate, user, username, href, model, textures, assets, previous, ...newObjectBase } = previousObject;
        const newVersion = [...previousObject.version]; // one of the few ways to make a deep copy of an array
        newVersion[2] += 1;
        /*
         * TODO use-case: new version with only a different texture
         *   -> same model, same assets, different textures
         *       -> gonna be pain
         *   -> idea: model.id (primaryKey) & model._id (mongoReference)
         *       -> mongoReference -> for saving space
         *
         * Model : TDObject -> one to many relationship
         * -> avoiding duplicities
         * 1) new version, only 1 texture changes
         * 2) new version, only 1 asset changes
         * 3) new version, only model file changes
         * -> TODO write some tests
         * */
        const newObjectData = {
            ...newObjectBase,
            previous: previousObject,
            version: newVersion,
            user: newUser,
            model: newModel,
        };
        const tdobject: TDObject = await this.objectRepository.save(newObjectData);
        tdobject.href = `${TDObjectsUrl}/${tdobject.id}`;
        tdobject.assets = assets;
        tdobject.textures = textures;
        /*
         * new version upload affecting TDObjectStatus:
         * unfinished -> unfinished  -- no need for change
         * submitted -> unfinished   -- both will have to check it either way
         * fixed -> fixed        -- both will have to check it either way
         * declined -> declined  -- same as unfinished mostly
         * approved -> unfinished (approved version changed, why would anyone do that though?)
         * */
        if (tdobject.status === TDObjectStatus.SUBMITTED || tdobject.status === TDObjectStatus.APPROVED) {
            tdobject.status = TDObjectStatus.UNFINISHED;
            tdobject.isApprovedByApprover = false;
            tdobject.isApprovedByHistorian = false;
        }
        await this.objectRepository.save(tdobject);

        await this.objectRepository.update({ id: previousObject.id }, { current: false, next: tdobject });
        return tdobject;
    }
}
