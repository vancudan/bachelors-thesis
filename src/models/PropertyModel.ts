import { getRepository } from 'typeorm';
import { Property } from '../entity/Property';

export default class PropertyModel {
    private readonly propertyRepository = getRepository(Property);

    public getById(id: number) {
        return this.propertyRepository.findOne(id);
    }

    public getAll() {
        return this.propertyRepository.find();
    }
}
