import { ObjectID } from 'mongodb';
import { getRepository } from 'typeorm';
import { Model } from '../entity/Model';
import { User } from '../entity/User';
import { ModelsUrl } from '../routes/Urls';

export default class ModelModel {
    private readonly modelRepository = getRepository(Model);

    public async create(filename: string, fileSize: number, id: ObjectID, user: User): Promise<Model> {
        // TODO this searches for a unique pair filename-fileSize, needs fix
        const model = await this.modelRepository.save({ id: id.toHexString(), filename, fileSize, user });
        model.href = `${ModelsUrl}/${model.id}`;
        await this.modelRepository.update({ id: model.id }, { href: `${ModelsUrl}/${model.id}` });
        return model;
    }

    public getById(id: ObjectID) {
        return this.modelRepository.findOne({
            where: {
                id: id.toHexString(),
            },
        });
    }

    public getAll() {
        return this.modelRepository.find();
    }

    public update(object: Model) {
        return this.modelRepository.update({ id: object.id }, object);
    }

    public delete(id: string) {
        return this.modelRepository.delete({ id });
    }
}
