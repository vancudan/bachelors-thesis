import { ObjectID } from 'mongodb';
import { getRepository } from 'typeorm';
import { Asset } from '../entity/Asset';
import { User } from '../entity/User';
import { AssetsUrl } from '../routes/Urls';

export default class AssetModel {
    private readonly assetRepository = getRepository(Asset);

    public async create(filename: string, fileSize: number, id: ObjectID, user: User): Promise<Asset> {
        // TODO this searches for a unique pair filename-fileSize, needs fix
        const asset = await this.assetRepository.save({ id: id.toHexString(), filename, fileSize, user });
        asset.href = `${AssetsUrl}/${asset.id}`;
        await this.assetRepository.update({ id: asset.id }, { href: `${AssetsUrl}/${asset.id}` });
        return asset;
    }

    public getById(id: string) {
        return this.assetRepository.findOne({
            where: {
                id,
            },
        });
    }

    public getAll() {
        return this.assetRepository.find();
    }

    public update(object: Asset) {
        return this.assetRepository.update({ id: object.id }, object);
    }

    public delete(id: string) {
        return this.assetRepository.delete({ id });
    }
}
