import { getRepository } from 'typeorm';
import { Map } from '../entity/Map';

export default class MapModel {
    private readonly mapRepository = getRepository(Map);

    public getById(id: number) {
        return this.mapRepository.findOne(id);
    }

    public getAll() {
        return this.mapRepository.find();
    }
}
