import { getRepository } from 'typeorm';
import { Plugin } from '../entity/Plugin';

export default class PluginModel {
    private readonly pluginRepository = getRepository(Plugin);

    public getById(id: number) {
        return this.pluginRepository.findOne(id);
    }

    public getAll() {
        return this.pluginRepository.find();
    }
}
