export default class Service {
    public readonly properties: { [index: string]: string } = {};

    public readonly parameters: string[] = [];

    // was originally 'constructor' but certain TS versions does not permit that
    public constructor(public readonly construct: Function) {}

    get name() {
        return this.construct.name;
    }

    public addDependency(name: string, property?: string) {
        if (property) {
            this.properties[property] = name;
        } else {
            this.parameters.push(name);
        }
    }
}
