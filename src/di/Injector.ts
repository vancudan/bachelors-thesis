import Service from './Service';

class Injector {
    private services: { [index: string]: Service } = {};
    private instances: { [index: string]: Object } = {};

    public register(service: Function | Object) {
        const constructor = typeof service === 'function' ? service : service.constructor;
        const name = constructor.name;
        if (!this.services[name]) this.services[name] = new Service(constructor);
        return this.services[name];
    }

    public registerServiceInstance(name: string, instance: Object) {
        this.instances[name] = instance;
        return instance;
    }

    public get(name: string) {
        return this.instances[name] ? this.instances[name] : this.create(name);
    }

    public create(name: string, recursive = false) {
        const service = this.services[name];

        const getDependency = (dependency: string) => (recursive ? this.create(dependency, recursive) : this.get(dependency));

        // Gather constructor dependencies
        const dependencies: Object[] = [];
        for (const dependency of service.parameters) {
            dependencies.push(getDependency(dependency));
        }

        // Create instance of the service
        const constructor: any = this.services[name].construct;
        const instance = new constructor(...dependencies);

        // Set property dependencies of the service to the service instance
        for (const property of Object.keys(service.properties)) {
            const dependency = service.properties[property];
            Reflect.set(instance, property, getDependency(dependency));
        }

        // Save and return the service instance
        this.instances[name] = instance;
        return this.instances[name];
    }
}

const injector = new Injector();
export default injector;
