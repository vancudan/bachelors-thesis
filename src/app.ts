// import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import express from 'express';
import formidable from 'express-formidable';
import glob from 'glob';
import injector from './di/Injector';
import authentication from './middleware/authentication';
import router from './routes/Router';

// Create Express server
const app = express();

// Express configuration
app.set('port', process.env.PORT || 3000);
app.use(cors()); // CORS support
app.use(compression()); // Compresses requests
// these bodyParser methods were changed (deprecated) in Express 4.16.0
// app.use(bodyParser.json()); // Support application/json type data
// app.use(bodyParser.urlencoded({ extended: false })); // Support application/x-www-form-urlencoded post data
// these are to be used for the same behaviour (in Express >= 4.16.0)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(formidable()); // Support multipart/form-data post data
app.use(authentication(process.env.JWT_SECRET)); // JWT authentication

// Load application controllers
const controllerFiles = glob.sync(`${process.env.MAPPING}*`, { cwd: 'src/', nocase: true });
for (const controllerFile of controllerFiles) {
    const controllerName = `./${controllerFile}`.replace('.ts', '');
    import(controllerName).then((controller) => injector.register(controller['default']));
}

// Set application routing rules
app.use('/', router);

export default app;
