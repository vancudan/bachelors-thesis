export enum MimeType {
    TDS = 'application/x-3ds',
    BINARY = 'application/octet-stream',
    BLENDER = 'application/x-blender',
    JPG_IMAGE = 'image/jpeg',
    MULTIPART_FORM_DATA = 'multipart/form-data',
    OBJ = 'application/x-tgif',
    PNG_IMAGE = 'image/png',
    GLB_BINARY = 'model/gltf-binary',
    GLTF_JSON = 'model/gltf+json',
}

export enum TDObjectFormat {
    TDS = '3DS',
    BLEND = 'BLEND',
    FBX = 'FBX',
    MAX = 'MAX',
    OBJ = 'OBJ',
    SFB = 'SFB',
    GLB = 'GLB',
    GLTF = 'GLTF',
}

export const MimeTypeTDFormatsRelations = {
    [TDObjectFormat.TDS]: MimeType.TDS,
    [TDObjectFormat.BLEND]: MimeType.BLENDER,
    [TDObjectFormat.FBX]: MimeType.BINARY,
    [TDObjectFormat.MAX]: MimeType.BINARY,
    [TDObjectFormat.OBJ]: MimeType.OBJ,
    [TDObjectFormat.SFB]: MimeType.BINARY,
    [TDObjectFormat.GLB]: MimeType.GLB_BINARY,
    [TDObjectFormat.GLTF]: MimeType.GLTF_JSON,
};
