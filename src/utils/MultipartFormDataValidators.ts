import { Request } from 'express';
import { Fields, File, Files } from 'formidable';
import { includes } from './EnumUtils';
import { Extension } from './Extension';
import { MimeType } from './MimeType';

type ValidatorFunction = (field: string) => boolean;

export function validateFile(request: Request, name: string, mimeType: MimeType, extensions?: Extension[]): File | false {
    const files: Files | undefined = request.files;
    if (!files || !files[name]) return false;

    const file = files[name];
    const fileExtension = file.name
        .toLowerCase()
        .split('.')
        .pop();
    if (file.type !== mimeType || (!!extensions && !includes(extensions, fileExtension))) return false;

    return file;
}

export function validateField(request: Request, name: string, validators: ValidatorFunction[]): string | false {
    const fields: Fields | undefined = request.fields;
    if (!fields || !fields[name]) return false;

    const field = fields[name] as string;
    if (!validators.reduce<boolean>((previous, validator) => previous && validator(field), true)) return false;

    return field;
}
