export interface StringObject {
    [index: string]: string;
}

export const toStringArray = (anEnum: StringObject) =>
    Object.keys(anEnum)
        .filter((key) => isNaN(Number(key)))
        .map((key) => anEnum[key]);

export const includes = <E extends string>(enumValues: E[], value?: string) =>
    enumValues.reduce<boolean>((previous, current) => previous || current === value, false);
