import { Request } from 'express';
import { Fields } from 'formidable';

export interface Location {
    lon: number;
    lat: number;
    alt: number;
}

/*
 * validates location upload
 * Location -> parsed and returned
 * false -> location was not present
 * null -> location was present but in bad format
 *
 * */
export function validateLocation(request: Request): Location | false | null {
    const fields: Fields | undefined = request.fields;
    if (!fields || !fields['location']) return false;

    const gps = JSON.parse(fields['location'] as string);
    if (!gps) return false;
    if (!gps['lon'] || !gps['lat'] || !gps['alt']) return null;
    if (isNaN(gps['lon']) || isNaN(gps['lat']) || isNaN(gps['alt'])) return null;

    return gps;
}
