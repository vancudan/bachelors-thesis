import { TDObjectFormat } from './MimeType';

export enum Extension {
    TDS = '3ds',
    BLEND = 'blend',
    FBX = 'fbx',
    JPEG = 'jpeg',
    JPG = 'jpg',
    MAX = 'max',
    OBJ = 'obj',
    PNG = 'png',
    SFB = 'sfb',
    GLB = 'glb',
    GLTF = 'gltf',
}

export const ExtensionsTDFormatsRelations = {
    [TDObjectFormat.TDS]: [Extension.TDS],
    [TDObjectFormat.BLEND]: [Extension.BLEND],
    [TDObjectFormat.FBX]: [Extension.FBX],
    [TDObjectFormat.MAX]: [Extension.MAX],
    [TDObjectFormat.OBJ]: [Extension.OBJ],
    [TDObjectFormat.SFB]: [Extension.SFB],
    [TDObjectFormat.GLB]: [Extension.GLB],
    [TDObjectFormat.GLTF]: [Extension.GLTF],
};
