import { param } from 'express-validator';
import Inject from '../di/Inject';
import { User, UserRole, UserStatus } from '../entity/User';
import AssetModel from '../models/AssetModel';
import FileModel from '../models/FileModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { AssetsUrl } from '../routes/Urls';
import FileDownloadController from './FileDownloadController';

const idValidator = param('id', 'Invalid ID of Asset supplied').isMongoId();

/*
 * Creating Assets is done via POST to structures/:id/assets or 3DObjects/:id:/assets
 * aka there should be no "hanging" asset files -> all are to be added to a structure / TDObject
 * */
@Inject(FileModel)
export default class AssetsController extends FileDownloadController {
    @Inject(AssetModel)
    private assetModel: AssetModel;

    public constructor(fileModel: FileModel) {
        super(fileModel);
    }

    private getAsset() {
        return this.assetModel.getById(this.request.params.id);
    }

    /*
     * TODO probably some sort of filtering?
     * */
    @Route(`${AssetsUrl}`, [], [Method.GET])
    public async listAssetFiles() {
        const assets = await this.assetModel.getAll();
        this.response.status(200).json(assets);
    }

    /*
     * initializes asset file download from the mongo database
     * */
    @Route(`${AssetsUrl}/:id`, [idValidator], [Method.GET])
    public async downloadAssetFile() {
        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        // 404 is handled 'inside' the downloadFile method
        await this.downloadFile(this.request.params.id, 'Asset file with given ID not found');
    }

    /*
     * uploads a new asset file and replaces the old one
     * ! complicated !
     * */
    @Route(`${AssetsUrl}/:id`, [idValidator], [Method.PUT])
    public async updateAssetFile() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * deletes the asset file
     * */
    @Route(`${AssetsUrl}/:id`, [idValidator], [Method.DELETE])
    public async deleteAssetFile() {
        const asset = await this.getAsset();
        if (!asset) return this.response.status(404).send('Asset file with given ID not found');

        const user: User = this.request.user;
        if (
            user.status !== UserStatus.ACTIVE ||
            user.role === UserRole.COMMON ||
            (user.username !== asset.username && user.role !== UserRole.ADMIN)
        ) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        await this.assetModel.delete(asset.id);
        this.response.status(204).send('Asset deleted');
    }
}
