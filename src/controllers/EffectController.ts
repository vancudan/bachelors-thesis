import { param } from 'express-validator';
import Inject from '../di/Inject';
import EffectModel from '../models/EffectModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { EffectsUrl } from '../routes/Urls';
import Controller from './Controller';

const idValidator = param('id', 'Invalid ID of Effect supplied').isUUID();

/*
 * Effects are thought to be basically only descriptions of what the (Blender?) plugins do with textures
 * Textures will be 'assigned' effects based on which were used during their generation
 * effects will have a record of which plugin were used
 * */
export default class EffectController extends Controller {
    @Inject(EffectModel)
    private effectModel: EffectModel;

    /*
     * returns all effects that can be used to generate textures
     * TODO add ordering?
     * TODO consult with a graphics engineer, what infos are required etc
     * */
    @Route(`${EffectsUrl}/`, [], [Method.GET])
    public async listEffects() {
        const effects = await this.effectModel.getAll();
        this.response.status(200).json(effects);
    }

    /*
     * creates an effect to a specific (blender?) plugin
     * TODO one effect can be done by multiple plugins - adding to M:N relations in a similar way to properties
     * */
    @Route(`${EffectsUrl}/`, [], [Method.POST])
    public async createEffect() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * returns an effect detail
     * TODO is this endpoint necessary? what else will it return?
     *  */
    @Route(`${EffectsUrl}/:id`, [idValidator], [Method.GET])
    public async retrieveEffectDetail() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * updates an effect - new effect description?
     * */
    @Route(`${EffectsUrl}/:id`, [idValidator], [Method.PUT])
    public async updateEffect() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * deletes an effect - should be quite rare
     * */
    @Route(`${EffectsUrl}/:id`, [idValidator], [Method.DELETE])
    public async deleteEffect() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }
}
