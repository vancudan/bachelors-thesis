import { param, query } from 'express-validator';
import validator from 'validator';
import Inject from '../di/Inject';
import { Asset } from '../entity/Asset';
import { Structure } from '../entity/Structure';
import { TDObject } from '../entity/TDObject';
import { User, UserRole, UserStatus } from '../entity/User';
import AssetModel from '../models/AssetModel';
import FileModel from '../models/FileModel';
import StructureModel from '../models/StructureModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { StructureUrl } from '../routes/Urls';
import { Location, validateLocation } from '../utils/JsonValidators';
import { MimeType } from '../utils/MimeType';
import { validateField, validateFile } from '../utils/MultipartFormDataValidators';
import Controller from './Controller';

const idValidator = param('id', 'Invalid ID of Structure supplied').isUUID();

interface StructureResponse {
    id: string;
    public: boolean;
    city: string;
    name: string;
    description: string;
    createdDate: Date;
    href: string | null;
    // the number[] variant will never happen, but again TS will not allow it otherwise
    location: Location | number[] | null;
    username: string;
    user: User;
    allVariants: TDObject[];
    assets: Asset[];
}

export default class StructureController extends Controller {
    @Inject(AssetModel)
    private assetModel: AssetModel;

    @Inject(FileModel)
    private fileModel: FileModel;

    @Inject(StructureModel)
    private structureModel: StructureModel;

    private getStructure() {
        return this.structureModel.getById(this.request.params.id);
    }

    private createResponseWithLocation(structure: Structure): StructureResponse {
        let response;
        if (structure.location !== null) {
            response = {
                ...structure,
                // done this way so only lon lat alt keys are present and others are ignored - in case more are sent
                location: {
                    lon: structure.location[0],
                    lat: structure.location[1],
                    alt: structure.location[2],
                },
            };
        } else {
            response = structure;
        }
        return response;
    }

    private validateStructureUpload() {
        let valid = true;
        let message = 'Everything is valid';
        const name = validateField(this.request, 'name', [(value) => !validator.isEmpty(value)]);
        const city = validateField(this.request, 'city', [(value) => !validator.isEmpty(value)]);
        const description = validateField(this.request, 'description', [(value) => !validator.isEmpty(value)]);
        const location = validateLocation(this.request);

        if (!name || !city || !description) {
            valid = false;
            message = 'Invalid input data: name, city and description are required';
            return { valid, message, name, city, description, location };
        }

        /*
         * checking max allowed length for the string values.
         * check 'Structure' entity in case of a change
         * */
        if (city.length >= 50) {
            valid = false;
            message = "Value for 'city' is too long, max 50 is allowed.";
        } else if (name.length >= 100) {
            valid = false;
            message = "Value for 'name' is too long, max 100 is allowed.";
        } else if (description.length >= 1000) {
            valid = false;
            message = "Value for 'description' is too long, max 1000 is allowed.";
        }
        if (!valid) return { valid, message, name, city, description, location };

        if (location === null) {
            // error while parsing, return 400
            valid = false;
            message = 'Invalid location data. "lon", "lan", "alt" (all numbers) required';
            return { valid, message, name, city, description, location };
        }

        return { valid, message, name, city, description, location };
    }

    /*
     * returns a list of all structures.
     * if username is provided then returns only structures created by that user
     * returns empty list on non-existent user
     * list is ordered (ASC) by structure.name - UpperCase letters first
     * public is taken as 'false' if not said otherwise
     * */
    @Route(
        StructureUrl,
        [
            query('username')
                .optional()
                .isString(),
            query('public')
                .optional()
                .isBoolean(),
        ],
        [Method.GET],
    )
    public async default() {
        // TODO add searching?
        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        const username: string = this.request.query.username as string;
        let publicStructures: string | boolean = this.request.query.public as string;
        publicStructures = publicStructures === 'true';
        /*
         * query params are always sent as strings
         * this is pretty much only way I found to get a boolean out of it
         * */

        const objects = await (() => {
            if (!!username) return this.structureModel.getByUsername(username, publicStructures);
            return this.structureModel.getAll(publicStructures);
        })();

        /*
         * this can be done the same way as in TDObjectController
         * (check the 'omitCurrent' function)
         * but it would be less comprehensible, so I chose this way
         * */
        const responses: StructureResponse[] = [];
        objects.forEach((structure) => responses.push(this.createResponseWithLocation(structure)));
        return this.response.json(responses);
    }

    /*
     * route for creating structures
     * required params: city, name, description
     * optional: location
     *
     * returns created structure
     * */
    @Route(StructureUrl, [], [Method.POST])
    public async createNewStructure() {
        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE || user.role === UserRole.COMMON) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        // TODO allow a list of assets to be sent on creation -> added automatically, no need to add them one by one
        let loc = null;
        const { valid, message, name, city, description, location } = await this.validateStructureUpload();
        /*
         * in the case of !name || !city || !description the valid is already false and appropriate message is set
         * these values are checked in the 'if' only because typescript thinks their type is 'string | false'
         * but the 'false' value is handled in the validate function, so only the string value remains
         *
         * location: cannot use '!location' as 'false' means location was not provided -> is not error (400)
         * */
        if (!valid || !name || !city || !description || location === null) {
            return this.response.status(400).send(message);
        }
        if (location) {
            /*
             * location is present and parsed -> valid
             * this convert between the 'Location' interface and 'number[3]' is cuz of the entity/structure.location type
             * */
            loc = [location['lon'], location['lat'], location['alt']];
        }

        // @ts-ignore: Unresolved variable user
        const structure = await this.structureModel.create(city, name, description, loc, this.request.user);

        return this.response.status(201).json(this.createResponseWithLocation(structure));
    }

    /*
     * retrieve details about specific structure
     * */
    @Route(`${StructureUrl}/:id`, [idValidator], [Method.GET])
    public async retrieveStructure() {
        const structure = await this.getStructure();
        if (!structure) return this.response.status(404).send('Structure with given ID not found');

        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        return this.response.json(this.createResponseWithLocation(structure));
    }

    /*
     * update specific structure
     * required: city, name, description
     * optional: location (will be set to null if not provided)
     *
     * id, createdDate, href, and user are not to be changed
     * public will be changed only during approval process
     * */
    @Route(`${StructureUrl}/:id`, [idValidator], [Method.PUT])
    public async updateStructure() {
        let loc = null;
        const structure = await this.getStructure();
        if (!structure) return this.response.status(404).send('Structure with given ID not found');

        const user: User = this.request.user;
        if (
            user.status !== UserStatus.ACTIVE ||
            user.role === UserRole.COMMON ||
            (user.username !== structure.username && user.role !== UserRole.ADMIN)
        ) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const { valid, message, name, city, description, location } = await this.validateStructureUpload();

        /*
         * in the case of !name || !city || !description the valid is already false and appropriate message is set
         * these values are checked in the 'if' only because typescript thinks their type is 'string | false'
         * but the 'false' value is handled in the validate function, so only the string value remains
         * */
        if (!valid || !name || !city || !description || location === null) {
            return this.response.status(400).send(message);
        }
        if (location) {
            /*
             * location is present and parsed -> valid
             * this convert between the 'Location' interface and 'number[3]' is cuz of the entity/structure.location type
             * */
            loc = [location['lon'], location['lat'], location['alt']];
        }

        // TODO allow to change (default) TDObject via this update

        /*
         * now that everything else is loaded, let's load the assets
         * in this case it's only an array of ids
         * */
        const assets = validateField(this.request, 'assets', [(value) => !validator.isEmpty(value)]);
        if (!assets) return this.response.status(400).send('Assets are required.');

        /*
         * array of ids (string, MongoID) should be sent via JSON.stringify()
         * aka now we try to load them with JSON.parse()
         * */
        const assetsParsedFromString = JSON.parse(assets);
        if (typeof assetsParsedFromString !== 'object') {
            return this.response.status(400).send('Assets are required to be array (of IDs).');
        }

        /*
         * now assets are verified to be an array
         * validation basically ends, from now on the 'non-valid' ids etc. are simply ignored
         * */
        const assetsToUpdate: Asset[] = [];

        /*
         * find Assets  matching to sent IDs (values that were not MongoID are ignored)
         * */
        for (const assetId of assetsParsedFromString) {
            if (typeof assetId !== 'string' || !validator.isMongoId(assetId)) continue;
            const asset: Asset | undefined = await this.assetModel.getById(assetId);
            if (asset === undefined) continue;
            assetsToUpdate.push(asset);
        }

        structure.city = city;
        structure.name = name;
        structure.description = description;
        structure.location = loc;
        structure.assets = assetsToUpdate;
        await this.structureModel.update(structure);

        return this.response.json(this.createResponseWithLocation(structure));
    }

    /*
     * deletes the structure with given id
     * */
    @Route(`${StructureUrl}/:id`, [idValidator], [Method.DELETE])
    public async deleteStructure() {
        const structure = await this.getStructure();
        if (!structure) return this.response.status(404).send('Structure with given ID not found');

        const user: User = this.request.user;
        if (
            user.status !== UserStatus.ACTIVE ||
            user.role === UserRole.COMMON ||
            (user.username !== structure.username && user.role !== UserRole.ADMIN)
        ) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        await this.structureModel.delete(structure.id);
        return this.response.status(204).json();
    }

    /*
     * adds a new asset file to the structure
     * */
    @Route(`${StructureUrl}/:id/assets`, [idValidator], [Method.POST])
    public async uploadNewAsset() {
        const structure: Structure | undefined = await this.getStructure();
        if (!structure) {
            return this.response.status(404).send('Structure with given ID not found.');
        }

        const user: User = this.request.user;
        if (
            user.status !== UserStatus.ACTIVE ||
            user.role === UserRole.COMMON ||
            (user.username !== structure.username && user.role !== UserRole.ADMIN)
        ) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const assetFile = validateFile(this.request, 'asset', MimeType.BINARY);
        if (!assetFile) {
            return this.response.status(400).send('Asset file is required.');
        }

        /*
         * structure found and asset file uploaded
         * */
        const mongoFile = await this.fileModel.save(assetFile);
        if (structure.assets.some((value) => value.id === mongoFile.id.toHexString())) {
            return this.response.status(400).send('This file is already assigned to this TDObject.');
        }

        /*
         * structure does not contain said asset file already
         * create method behaves like FindOrCreate so no problem
         * */
        const asset: Asset = await this.assetModel.create(assetFile.name, assetFile.size, mongoFile.id, user);
        structure.assets.push(asset);
        await this.structureModel.update(structure);

        return this.response.json(asset);
    }
}
