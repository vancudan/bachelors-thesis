import { Request, RequestHandler, Response } from 'express';
import { ErrorFormatter, validationResult } from 'express-validator';
import injector from '../di/Injector';

export default abstract class Controller {
    private _request: Request;
    private _response: Response;

    get request() {
        return this._request;
    }

    get response() {
        return this._response;
    }

    public setRequest(request: Request) {
        this._request = request;
        return this;
    }

    public setResponse(response: Response) {
        this._response = response;
        return this;
    }

    public error(error: any) {
        throw error;
    }

    public static createRoute(name: string, action: string): RequestHandler {
        const errorFormatter: ErrorFormatter = (error) => {
            const { location, param, msg } = error;
            // @ts-ignore
            const message = !error.value ? msg : msg.replace('%value%', error.value);
            return `${location}[${param}]: ${message}`;
        };

        return (request, response) => {
            try {
                validationResult(request).throw();
                const controller: any = injector.create(name);
                if (controller instanceof Controller) controller.setRequest(request).setResponse(response);
                controller[action](request, response);
            } catch (errors) {
                if (errors['formatWith'] && errors['array']) {
                    errors = errors.formatWith(errorFormatter).array();
                }
                response.status(400).send(errors);
            }
        };
    }
}
