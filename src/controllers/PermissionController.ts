import { param } from 'express-validator';
import Inject from '../di/Inject';
import PermissionModel from '../models/PermissionModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { PermissionsUrl } from '../routes/Urls';
import Controller from './Controller';

const idValidator = param('id', 'Invalid ID of Permission supplied').isUUID();

/*
 * pretty much all of these are to be used by admin only
 * */
export default class PermissionController extends Controller {
    @Inject(PermissionModel)
    private permissionModel: PermissionModel;

    /*
     * list all permissions
     * */
    @Route(`${PermissionsUrl}/`, [], [Method.GET])
    public async listPermissions() {
        const permissions = await this.permissionModel.getAll();
        this.response.status(200).json(permissions);
    }

    /*
     * creates a new permission
     * */
    @Route(`${PermissionsUrl}/`, [], [Method.POST])
    public async createPermission() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * returns a permission detail
     * TODO is this endpoint necessary? what else will it return?
     *  */
    @Route(`${PermissionsUrl}/:id`, [idValidator], [Method.GET])
    public async retrievePermissionDetail() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * updates a permission - description?
     * */
    @Route(`${PermissionsUrl}/:id`, [idValidator], [Method.PUT])
    public async updatePermission() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * deletes a permission
     * */
    @Route(`${PermissionsUrl}/:id`, [idValidator], [Method.DELETE])
    public async deletePermission() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }
}
