import { param } from 'express-validator';
import Inject from '../di/Inject';
import PluginModel from '../models/PluginModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { PluginsUrl } from '../routes/Urls';
import Controller from './Controller';

const idValidator = param('id', 'Invalid ID of Plugin supplied').isUUID();

export default class PluginController extends Controller {
    @Inject(PluginModel)
    private pluginModel: PluginModel;

    /*
     * returns all plugins that can be used to generate textures
     * TODO add ordering / filtering by effects? (similar to filtering by properties)
     * TODO consult with a graphics engineer, what infos are required etc
     * */
    @Route(`${PluginsUrl}/`, [], [Method.GET])
    public async listPlugins() {
        const plugins = await this.pluginModel.getAll();
        this.response.status(200).json(plugins);
    }

    /*
     * adds a (Blender?) plugin to the pool of plugins usable to generate textures
     * TODO create a queue of sort to store the requests for texture generation
     * TODO consult graphics engineer with how the plugins can be called/triggered
     * TODO consult masters degree students with plugin storage (/google )
     * */
    @Route(`${PluginsUrl}/`, [], [Method.POST])
    public async createPlugin() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * returns a plugin detail
     * TODO consult which info is to be 'public' and which is to be shown in detail only
     * */
    @Route(`${PluginsUrl}/:id`, [idValidator], [Method.GET])
    public async retrievePluginDetail() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * updates a plugin - probably new file, improved performance?
     * */
    @Route(`${PluginsUrl}/:id`, [idValidator], [Method.PUT])
    public async updatePlugin() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * deletes a plugin - e.g. after comparing multiple plugins one was deemed inferior, and thus deleted
     * */
    @Route(`${PluginsUrl}/:id`, [idValidator], [Method.DELETE])
    public async deletePlugin() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }
}
