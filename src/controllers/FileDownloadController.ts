import FileNotFoundError from '../models/errors/FileNotFoundError';
import FileModel from '../models/FileModel';
import Controller from './Controller';

export default abstract class FileDownloadController extends Controller {
    protected readonly fileModel: FileModel;

    protected constructor(fileModel: FileModel) {
        super();
        this.fileModel = fileModel;
    }

    protected async downloadFile(id: string, fileNotFoundErrorMessage?: string) {
        try {
            const { filename, stream } = await this.fileModel.load(id);
            this.response.attachment(filename);
            stream.pipe(this.response).on('error', () => this.response.sendStatus(500));
        } catch (error) {
            if (error instanceof FileNotFoundError) {
                return !fileNotFoundErrorMessage ? this.response.sendStatus(404) : this.response.status(404).send(fileNotFoundErrorMessage);
            }
            throw error;
        }
    }
}
