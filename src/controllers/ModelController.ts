import { param } from 'express-validator';
import Inject from '../di/Inject';
import { User, UserStatus } from '../entity/User';
import FileModel from '../models/FileModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { ModelsUrl } from '../routes/Urls';
import FileDownloadController from './FileDownloadController';

const idValidator = param('id', 'Invalid ID of Model supplied').isMongoId();

/*
 * Creating Models is done via via creating TDObjects, that is via POST to /3DObjects
 * instance of a Model without TDObject (and vice versa) is not allowed
 * */
@Inject(FileModel)
export default class ModelController extends FileDownloadController {
    public constructor(fileModel: FileModel) {
        super(fileModel);
    }

    /*
     * TODO probably some sort of filtering?
     * */
    @Route(`${ModelsUrl}`, [], [Method.GET])
    public async listModelFiles() {
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * initializes model file download from the mongo database
     * */
    @Route(`${ModelsUrl}/:id`, [idValidator], [Method.GET])
    public async downloadModelFile() {
        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        await this.downloadFile(this.request.params.id, '3D model file with given ID not found');
    }

    /*
     * uploads a new model file
     * TODO check: according to swagger api doc, only admin should be able to perform this operation
     *      when modeller is to use similar feature, he should use the "TDObject -> createNewTDObjectVersion"
     * */
    @Route(`${ModelsUrl}/:id`, [idValidator], [Method.PUT])
    public async uploadModelFile() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * deletes the model file
     * according to swagger api doc, only admin should be able to perform this operation
     * TODO model is required for TDObject -> how do we handle that?
     *      shouldn't this be handled during TDObject deletion only?
     * */
    @Route(`${ModelsUrl}/:id`, [idValidator], [Method.DELETE])
    public async deleteModelFile() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }
}
