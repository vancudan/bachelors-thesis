import { param } from 'express-validator';
import validator from 'validator';
import Inject from '../di/Inject';
import { User, UserRole, UserStatus } from '../entity/User';
import UserModel from '../models/UserModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { UsersUrl } from '../routes/Urls';
import { includes, toStringArray } from '../utils/EnumUtils';
import { validateField } from '../utils/MultipartFormDataValidators';
import Controller from './Controller';

const idValidator = param('id', 'Invalid ID of User supplied').isAlphanumeric();

export default class UserController extends Controller {
    @Inject(UserModel)
    private userModel: UserModel;

    private getUser() {
        return this.userModel.getByUsername(this.request.params.id);
    }

    /*
     * returns a list of all users
     * TODO add ordering / filtering / searching?
     * */
    @Route(`${UsersUrl}`, [], [Method.GET])
    public async listUsers() {
        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        const users = await this.userModel.getAll();
        this.response.status(200).json(users);
    }

    /*
     * creates a new user
     * // TODO user creation - right now it's required for one to already have an account to create more users (auth middleware)
     * */
    @Route(`${UsersUrl}`, [], [Method.POST])
    public async createUser() {
        const name = validateField(this.request, 'name', [(value) => !validator.isEmpty(value)]);
        const username = validateField(this.request, 'username', [(value) => !validator.isEmpty(value) && validator.isAlphanumeric(value)]);

        if (!name || !username) return this.response.status(400).json('Name and username are required');

        const user: User | undefined = await this.userModel.getByUsername(username);
        if (user !== undefined) return this.response.status(400).json('This username is already used.');

        const createdUser = await this.userModel.create(name, username);

        return this.response.status(201).json(createdUser);
    }

    /*
     * returns a user detail
     * as of June 14th 2020 - there's nothing much else to be returned
     * in later phases of the vmck project it's highly likely that there'll be quite some non-public info, but not yet
     * (i.e. date_of_birth)
     *  */
    @Route(`${UsersUrl}/:id`, [idValidator], [Method.GET])
    public async retrieveUserDetail() {
        const user = await this.getUser();
        if (!user) return this.response.status(404).send('User with given username not found');

        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE || (user.username !== requestUser.username && requestUser.role !== UserRole.ADMIN)) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        return this.response.status(200).json(user);
    }

    /*
     * updates a user
     * as of June 14th 2020 - there's only 'name' to be updated
     * though it's highly possible that much more information about users is to be stored
     * */
    @Route(`${UsersUrl}/:id`, [idValidator], [Method.PUT])
    public async updateUser() {
        const user = await this.getUser();
        if (!user) return this.response.status(404).send('User with given username not found');

        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE || (user.username !== requestUser.username && requestUser.role !== UserRole.ADMIN)) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const name = validateField(this.request, 'name', [(value) => !validator.isEmpty(value)]);
        const username = validateField(this.request, 'username', [(value) => !validator.isEmpty(value) && validator.isAlphanumeric(value)]);
        const role = validateField(this.request, 'role', [(value) => !validator.isEmpty(value)]);
        const status = validateField(this.request, 'status', [(value) => !validator.isEmpty(value)]);

        if (!name || !username || !role || !status) {
            return this.response.status(400).json('Name, username, role and status are required');
        }

        if (username !== user.username) {
            const newUsernameUser = await this.userModel.getByUsername(username);
            if (newUsernameUser !== undefined) return this.response.status(400).send('This username is used already');
        }

        // @ts-ignore: Unresolved variable user
        if (this.request.user.role !== UserRole.ADMIN && (role !== user.role || status !== user.status)) {
            return this.response.status(403).send('Only admin is allowed to change user roles and status');
        }

        if (!includes(toStringArray(UserRole), role)) {
            return this.response.status(400).send('Invalid data - this user role does not exist');
        }
        if (!includes(toStringArray(UserStatus), status)) {
            return this.response.status(400).send('Invalid data - this user status does not exist');
        }

        await this.userModel.update(user, name, username, role, status);
        return this.response.status(200).json(await this.userModel.getByUsername(username));
    }

    /*
     * deletes a user
     * TODO solving the ownership of all the TDObjects / models / textures / comments created
     *      only logical delete aka status='deleted', so that the ownership remains?
     *      or hard user delete, and the ownership is transferred? if so, to whom?
     *      NOTE: logical delete, deletedDate, admin script deleting em -> move to archive db or sth
     *          : add logical delete to each and every thing that can be deleted
     *          : + admin-only endpoint xyz/deleted GET, xyz/deleted/:id PUT (update deleted to false)
     * */
    @Route(`${UsersUrl}/:id`, [idValidator], [Method.DELETE])
    public async deleteUser() {
        const user = await this.getUser();
        if (!user) return this.response.status(404).send('User with given username not found');

        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE || (user.username !== requestUser.username && requestUser.role !== UserRole.ADMIN)) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        await this.userModel.delete(user);
        this.response.status(204).send();
    }
}
