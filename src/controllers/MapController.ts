import Inject from '../di/Inject';
import MapModel from '../models/MapModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { MapsUrl } from '../routes/Urls';
import Controller from './Controller';

/*
 * TODO this is super-basic form to work with, check other Controllers for more detail
 *       dividing Map into Chunks and their inner representation is not part of vancudan's bachelors thesis
 *       and is to be done in the future, quite probably by SP1 / SP2 teams on FIT ČVUT
 *       this here is only basic layout to be expanded
 * */
export default class MapController extends Controller {
    @Inject(MapModel)
    private mapModel: MapModel;

    @Route(`${MapsUrl}/`, [], [Method.GET])
    public async listMaps() {
        const maps = await this.mapModel.getAll();
        this.response.status(200).json(maps);
    }
}
