import { param } from 'express-validator';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { CommentsUrl } from '../routes/Urls';
import Controller from './Controller';

const idValidator = param('id', 'Invalid ID of Comment supplied').isNumeric();

export default class CommentController extends Controller {
    /*@Inject(CommentModel)
    private commentModel: CommentModel;*/

    /***********************************************************************************
     *  TODO !!!! all of these are in TDObject controller, should they be here or there?
     ***********************************************************************************/

    /*
     * returns all comments of the TDObject
     * */
    @Route(`${CommentsUrl}/`, [], [Method.GET])
    public async listComments() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * creates a comment to a specific TDObject
     * TODO or Texture?
     * */
    @Route(`${CommentsUrl}/`, [], [Method.POST])
    public async createComment() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * returns a comment detail
     * TODO is this endpoint necessary? what else will it return other than in TDObject detail?
     *  -> RESOLVE: useless
     * */
    @Route(`${CommentsUrl}/:id`, [idValidator], [Method.GET])
    public async retrieveCommentDetail() {
        // TODO check permissions
        // const comment = await this.getComment();
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * updates a comment (typos, additional infos)
     * */
    @Route(`${CommentsUrl}/:id`, [idValidator], [Method.PUT])
    public async updateComment() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * deletes a comment
     * */
    @Route(`${CommentsUrl}/:id`, [idValidator], [Method.DELETE])
    public async deleteComment() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }
}
