import { header, param, query } from 'express-validator';
import validator from 'validator';
import Inject from '../di/Inject';
import { Asset } from '../entity/Asset';
import { TDObject, TDObjectStatus } from '../entity/TDObject';
import { Texture } from '../entity/Texture';
import { User, UserRole, UserStatus } from '../entity/User';
import AssetModel from '../models/AssetModel';
import FileModel from '../models/FileModel';
import ModelModel from '../models/ModelModel';
import StructureModel from '../models/StructureModel';
import TDObjectModel from '../models/TDObjectModel';
import TextureModel from '../models/TextureModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { StructureUrl, TDObjectsUrl } from '../routes/Urls';
import { includes, toStringArray } from '../utils/EnumUtils';
import { Extension, ExtensionsTDFormatsRelations } from '../utils/Extension';
import { MimeType, MimeTypeTDFormatsRelations, TDObjectFormat } from '../utils/MimeType';
import { validateField, validateFile } from '../utils/MultipartFormDataValidators';
import Controller from './Controller';

const multipartFormDataValidator = header('content-type')
    .isMimeType()
    .custom((value: string) => value.startsWith(MimeType.MULTIPART_FORM_DATA));
const idValidator = param('id', 'Invalid ID of 3D object supplied').isUUID();
const structureIdValidator = param('structureId', 'Invalid ID of a Structure supplied').isUUID();
const versionRegexCheck = /^[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}$/;
const versionValidator = param('version', 'Invalid version supplied, required format: x.y.z, max length of each number: 4').matches(
    versionRegexCheck,
);
const uploadWithIdValidators = [idValidator, multipartFormDataValidator];
const versionWithIdValidators = [idValidator, versionValidator];
const commentIdValidator = param('id', 'Invalid ID of Comment supplied').isNumeric();

// TODO fix return codes
export default class TDObjectController extends Controller {
    @Inject(TDObjectModel)
    private objectModel: TDObjectModel;

    @Inject(AssetModel)
    private assetModel: AssetModel;

    @Inject(TextureModel)
    private textureModel: TextureModel;

    @Inject(FileModel)
    private fileModel: FileModel;

    @Inject(ModelModel)
    private modelModel: ModelModel;

    @Inject(StructureModel)
    private structureModel: StructureModel;

    private async approvalProcessModeller(object: TDObject) {
        if (object.status === TDObjectStatus.UNFINISHED) {
            object.status = TDObjectStatus.SUBMITTED;
        } else if (object.status === TDObjectStatus.DECLINED) {
            object.status = TDObjectStatus.FIXED;
        } else {
            return this.response.status(400).json('TDObject was already sent for approval, wait for evaluation.');
        }

        await this.objectModel.update(object);
        return this.response.status(200).json(await this.getTDObject());
    }

    private async approvalProcessApprover(user: User, object: TDObject, approve: boolean, commentText: string | false) {
        if (object.isApprovedByApprover && approve) {
            return this.response
                .status(400)
                .json('You already approved this model, wait for modeller and historian to fix their issues first');
        }
        object.isApprovedByApprover = approve;
        object.approverApproving = user;

        if (!approve) {
            object.status = TDObjectStatus.DECLINED;
            if (!commentText) {
                return this.response.status(400).json('You must provide a reason (comment) as to why are you declining the object');
            }
        } else {
            if (object.status === TDObjectStatus.SUBMITTED) {
                if (object.isApprovedByHistorian) {
                    object.status = TDObjectStatus.APPROVED;
                }
                // else: stay as you are and wait for historian
            } else if (object.status === TDObjectStatus.FIXED) {
                object.status = TDObjectStatus.SUBMITTED;
                if (object.isApprovedByHistorian) {
                    object.isApprovedByHistorian = false;
                    /*
                     * historian approved some previous version
                     * the object changed since then so it's necessary for him to check it again
                     * */
                }
            } else if (object.status === TDObjectStatus.DECLINED) {
                /*
                 * stay as you are, no status change
                 * happens only in case when historian checks the model first and rejects it
                 * therefore status is "declined", approving it as approver later does not change it
                 * it only saves the info that approver approved this model and
                 *   that's done on the 1st 2 lines of code in this function
                 *
                 * 2nd option when this might happen:
                 * sb declines the model by accident and then wants to fix it
                 *  --> too bad, you better be careful, the modeller will then have to submit it s fixed again and
                 *      you must control it again
                 *
                 * modeller submits it for control
                 * then the 1st approval changes the status to submitted and let's the 2nd one to look at it again
                 * */
            } else {
                /*
                 * aka the model is either "unfinished" or "approved" already
                 */
                return this.response.status(400).json('This TDObject is not to be approved now.');
            }
        }

        // TODO create comment if text is present

        await this.objectModel.update(object);
        return this.response.status(200).json(await this.getTDObject());
    }

    private async approvalProcessHistorian(user: User, object: TDObject, approve: boolean, commentText: string | false) {
        if (object.isApprovedByHistorian && approve) {
            return this.response
                .status(400)
                .json('You already approved this model, wait for modeller and approver to fix their issues first');
        }
        object.isApprovedByHistorian = approve;
        object.historianApproving = user;

        if (!approve) {
            object.status = TDObjectStatus.DECLINED;
            if (!commentText) {
                return this.response.status(400).json('You must provide a reason (comment) as to why are you declining the object');
            }
        } else {
            if (object.status === TDObjectStatus.SUBMITTED) {
                if (object.isApprovedByApprover) {
                    object.status = TDObjectStatus.APPROVED;
                }
                // else: stay as you are and wait for approver
            } else if (object.status === TDObjectStatus.FIXED) {
                object.status = TDObjectStatus.SUBMITTED;
                if (object.isApprovedByApprover) {
                    object.isApprovedByApprover = false;
                    /*
                     * approver approved some previous version
                     * the object changed since then so it's necessary for him to check it again
                     * */
                }
            } else if (object.status === TDObjectStatus.DECLINED) {
                /*
                 * stay as you are, no status change
                 * happens in case when approver checks the model first and rejects it
                 * therefore status is "declined", approving it as historian later does not change it
                 * it only saves the info that historian approved this model and
                 *   that's done on the 1st 2 lines of code in this function
                 *
                 * 2nd option when this might happen:
                 * sb declines the model by accident and then wants to fix it
                 *  --> too bad, you better be careful, the modeller will then have to submit it s fixed again and
                 *      you must control it again
                 *
                 * modeller submits it for control
                 * then the 1st approval changes the status to submitted and let's the 2nd one to look at it again
                 * */
            } else {
                /*
                 * aka the model is either "unfinished" or "approved" already
                 */
                return this.response.status(400).json('This TDObject is not to be approved now.');
            }
        }

        // TODO create comment if text is present

        await this.objectModel.update(object);
        return this.response.status(200).json(await this.getTDObject());
    }

    private validateAndLoadModelFile() {
        const allowedFormats = toStringArray(TDObjectFormat);
        const format = validateField(this.request, 'format', [
            (value) => !validator.isEmpty(value),
            (value) => includes(allowedFormats, value),
        ]) as TDObjectFormat | false;

        const allowedExtensions: Extension[] = !format ? [] : ExtensionsTDFormatsRelations[format];
        const mimeType = !format ? MimeType.BINARY : MimeTypeTDFormatsRelations[format];
        return validateFile(this.request, 'model', mimeType, allowedExtensions);
    }

    private async validate3DObjectUpload() {
        let valid = true;
        let message = 'Everything is valid';
        let code = 200;
        const name = validateField(this.request, 'name', [(value) => !validator.isEmpty(value)]);
        const modelFile = await this.validateAndLoadModelFile();

        if (!name || !modelFile) {
            valid = false;
            message = 'Invalid input data: name and attached Model file are required';
            code = 400;
            return { valid, code, message, name, modelFile };
        }

        return { valid, code, message, name, modelFile };
    }

    /*
     * is called in endpoints whose urls looks like: ${TDObjectsUrl}/:id/:version
     * thus both id and version are in params, both validated with 'versionWithIdValidators'
     * 'this.getTDObject()' uses id from params
     * */
    private async validateAndFindTDObjectVersion(): Promise<{
        versionFound: boolean;
        msg: string;
        statusCode: number;
        version: TDObject | undefined;
    }> {
        let versionFound = true;
        let msg = 'All OK';
        let statusCode = 200;
        let version;

        const object: TDObject | undefined = await this.getTDObject();
        if (!object) {
            versionFound = false;
            msg = '3D object with given ID not found';
            statusCode = 404;
            return { versionFound, msg, statusCode, version };
        }

        const allVersions: TDObject[] = await this.objectModel.getAllVersions(object);

        /*
         * versionWithIdValidators in route definition checks that the format is x.y.z
         * */
        const versionArray: number[] = await this.request.params.version.split('.').map(Number);

        /*
         * pretty much only viable array comparison.
         * basic '==' returns false even if they're equal as it compares instances, not members
         * */
        version = await allVersions.find((tdObj) => {
            for (let i = 0; i < 3; i++) if (tdObj.version[i] !== versionArray[i]) return false;
            return true;
        });

        if (!version) {
            versionFound = false;
            msg = 'Version of that value not found in said 3DObjects history';
            statusCode = 404;
            return { versionFound, msg, statusCode, version };
        }
        return { versionFound, msg, statusCode, version };
    }

    private async projectTDObject(object: TDObject) {
        const version = `${object.version[0]}.${object.version[1]}.${object.version[2]}`;
        /*
         * TODO change to:
         *  const { not, wanted, columns, ...wantedColumns } = object;
         *  return { wantedColumns, version};
         * */
        const {
            id,
            name,
            current,
            href,
            createdDate,
            status,
            structureId,
            transformation,
            model,
            assets,
            textures,
            username,
            isApprovedByApprover,
            isApprovedByHistorian,
        } = object;
        return {
            id,
            name,
            current,
            href,
            createdDate,
            version,
            status,
            structureId,
            transformation,
            model,
            assets,
            textures,
            username,
            isApprovedByApprover,
            isApprovedByHistorian,
        };
    }

    private getStructure() {
        return this.structureModel.getById(this.request.params.structureId);
    }

    private getTDObject() {
        return this.objectModel.getById(this.request.params.id);
    }

    /*
     * creates a new 3DObject
     * structure has to be created first, so basically this just "adds" newly created TDO to a structure
     *
     * ! ATTENTION !
     * the 3DObject requires a model (file) to be created with
     * using the same file to create multiple 3DObjects in VMCK project is considered a fatal mistake
     * though this API+DB are considered to be used during lectures
     * -> aka students will be saving here their works (+ created 3D models)
     * -> when the assignment is created each student might get their own 3DObject to start working on
     * -> the starting files might be the same
     * -> the students will be only creating new versions of their 3DObject
     *      in such a case using the same file for more 3DObjects should be allowed
     *
     * TODO dilemma (allowed for now, the 'check' is in a comment)
     * */
    @Route(`${StructureUrl}/:structureId${TDObjectsUrl}`, [multipartFormDataValidator, structureIdValidator], [Method.POST])
    public async createNew3DObject() {
        // TODO allow users to upload an array of textures & assets in 3Object creation and set them immediately?
        const structure = await this.getStructure();
        if (!structure) return this.response.status(404).send(`Structure with id '${this.request.params.structureId}' does not exist`);

        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE || user.role === UserRole.COMMON) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const { valid, code, message, name, modelFile } = await this.validate3DObjectUpload();
        if (!valid || !name || !modelFile) return this.response.status(code).send(message);

        /*
         * saving the file to mongoDB,  if there is one already, it's info are returned
         * -> the files are shared between versions / 3DObjects to save space
         * */
        const mongoFile = await this.fileModel.save(modelFile);

        /*
        // TODO check comment above this route definition -> should be allowed or not?
        const modelExists = await this.modelModel.getById(mongoFile.id);
        if(modelExists !== undefined){
            return this.response.status(400)
                        .send("This file has been used before, unable to create new 3DObject. Create new version instead.");
        }*/

        /*
         * as models can be (currently) shared between TDObjects (versions or totally different objects)
         * 'modelModel.create(...)' behaves as 'findOrCreate'
         * */
        const model = await this.modelModel.create(modelFile.name, modelFile.size, mongoFile.id, this.request.user);
        const object = await this.objectModel.create(name, structure.id, model, this.request.user);
        /*
         * TDObject is created, add it to the structure
         *
         * by default, if structure does not have a default object assigned the first created is assumed as one
         * (either it doesn't have it or it was previously deleted)
         * */
        structure.allVariants.push(object);
        if (!structure.defaultObject) {
            structure.defaultObject = object;
        }
        await this.structureModel.update(structure);

        return this.response.status(201).json(await this.projectTDObject(object));
    }

    /*
     * Gets a list of TDObjects, sorted alphabetically by their name
     * if username is provided in query, only these users TDObjects are returned (he's the author)
     *
     * TODO further filtering is required. e.g. filtering by properties
     * */
    @Route(
        TDObjectsUrl,
        [
            query('username')
                .optional()
                .isString(),
            query('waitingForApproval')
                .optional()
                .isBoolean(),
        ],
        [Method.GET],
    )
    public async listTDObjects() {
        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const omitCurrent = ({ current, ...object }: { current: boolean }) => object;
        const username: string = this.request.query.username as string;
        let waitingForApproval = this.request.query.waitingForApproval as string | boolean;
        waitingForApproval = waitingForApproval === 'true';

        if (waitingForApproval) {
            if (user.status !== UserStatus.ACTIVE || user.role === UserRole.COMMON) {
                return this.response.status(403).json('You are not allowed to perform this action.');
            }
            const resultingArrayOfTDObjects: TDObject[] = await this.objectModel.getWaitingForApproval(user);
            return this.response.json(
                (await Promise.all(resultingArrayOfTDObjects.map((object) => this.projectTDObject(object)))).map(omitCurrent),
            );
        }

        const objects = await (() => {
            if (!!username) return this.objectModel.getByUsername(username);
            return this.objectModel.getAll();
        })();

        /*
         * applying the 'omitCurrent' function to all found results
         * */
        return this.response.json((await Promise.all(objects.map((object) => this.projectTDObject(object)))).map(omitCurrent));
    }

    /*
     * Listing all versions of the specific object
     * TODO change this url to /3DObjects/:id/allVersions and return 3DObject detail on /3DObjects/:id instead
     *      -> getting a detail of 3DObject would be much simpler and faster then
     * */
    @Route(`${TDObjectsUrl}/:id`, [idValidator], [Method.GET])
    public async listVersions() {
        const object = await this.getTDObject();
        if (!object) return this.response.status(404).send('3D object with given ID not found');

        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const versions = await this.objectModel.getAllVersions(object);
        this.response.json(await Promise.all(versions.map((version) => this.projectTDObject(version))));
    }

    /*
     * creates a new version of the TDObject
     * usually used during approval process:
     * historian / approver declines a model, write a comment with what's wrong
     * modeller fixes the model, uploads new version and approval process starts again
     * the diagram should be included in this project, under 'diagrams' or 'schemas'
     * if not, see vancudan's bachelors thesis:
     * https://alfresco.fit.cvut.cz/share/proxy/alfresco/api/node/content/workspace/SpacesStore/f07bbdbb-e1e9-4545-86fc-666f2c548b05
     *
     * ! ATTENTION !
     * the 3DObject requires a model (file) to be created with
     * using the same file to create multiple 3DObjects in VMCK project is considered a fatal mistake
     * though this API+DB are considered to be used during lectures
     * -> aka students will be saving here their works (+ created 3D models)
     * -> when the assignment is created each student might get their own 3DObject to start working on
     * -> the starting files might be the same
     * -> the students will be only creating new versions of their 3DObject
     *      in such a case using the same file for more 3DObjects should be allowed
     *
     * TODO dilemma (allowed for now, the 'check' is in a comment)
     * */
    @Route(`${TDObjectsUrl}/:id`, uploadWithIdValidators, [Method.POST])
    public async createNewTDObjectVersion() {
        const object = await this.getTDObject();
        if (!object) return this.response.status(404).send('3D object with given ID not found');

        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE || user.role === UserRole.COMMON) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        if (!object.current) return this.response.status(400).send('New version can be created only on the newest 3DObject.');

        const modelFile = await this.validateAndLoadModelFile();
        if (!modelFile) return this.response.status(400).send('Invalid ID of 3D object supplied');

        /*
        // TODO allow users to upload an array of textures & assets in 3Object update & change them
        const assets = validateField(this.request, 'assets', [(value) => !validator.isEmpty(value)]);
        const textures = validateField(this.request, 'textures', [(value) => !validator.isEmpty(value)]);
        if (!assets || !textures) return this.response.status(400).send('Textures and assets are required.');

        // array of ids (string, MongoID) should be sent via JSON.stringify()
        // aka now we try to load them with JSON.parse()
        const assetsParsedFromString = JSON.parse(assets);
        const texturesParsedFromString = JSON.parse(textures);
        if (typeof assetsParsedFromString !== 'object' || typeof texturesParsedFromString !== 'object') {
            return this.response.status(400).send('Textures and assets are required to be array (of IDs).');
        }

        const assetsToUpdate: Asset[] = [];
        const texturesToUpdate: Texture[] = [];

        // find Assets and Textures matching to sent IDs
        // (values that were not MongoID are ignored)

        for (const assetId of assetsParsedFromString) {
            if (typeof assetId !== 'string' || !validator.isMongoId(assetId)) continue;
            const asset: Asset | undefined = await this.assetModel.getById(assetId);
            if (asset === undefined) continue;
            assetsToUpdate.push(asset);
        }

        for (const textureId of texturesParsedFromString) {
            if (typeof textureId !== 'string' || !validator.isMongoId(textureId)) continue;
            const texture: Texture | undefined = await this.textureModel.getById(textureId);
            if (texture === undefined) continue;
            texturesToUpdate.push(texture);
        }
        */

        const mongoFile = await this.fileModel.save(modelFile);

        /*
         * 'modelModel.create(...)' behaves like 'findOrCreate()' as the records are shared
         * */
        const model = await this.modelModel.create(modelFile.name, modelFile.size, mongoFile.id, this.request.user);

        const newVersion = await this.objectModel.createNextVersion(object, this.request.user, model);
        this.response.json(await this.projectTDObject(newVersion));
    }

    /*
     * returns a specific version of the TDObject
     * */
    @Route(`${TDObjectsUrl}/:id/:version`, versionWithIdValidators, [Method.GET])
    public async getVersion() {
        const { versionFound, msg, statusCode, version } = await this.validateAndFindTDObjectVersion();
        if (!versionFound || version === undefined) return this.response.status(statusCode).json(msg);
        /*
         * version is 'undefined' only in case version was not found
         * checking the value here is only because TS thinks it can still be both
         * and announces an error while accessing 'version.id'
         * */

        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        /*
         * searching in the objects history is slow enough and this operation requires another select to db
         * but in case it hadn't been done this way only the raw sql results would be returned
         * -> no 'assets', 'textures' etc. would be present
         *           (which is required as this should be the 'detail' of said TDObject version)
         * */
        this.response.status(200).json(await this.objectModel.getById(version.id));
    }

    /*
     * updates a specific version of the TDObject
     * */
    @Route(`${TDObjectsUrl}/:id/:version`, versionWithIdValidators, [Method.PUT])
    public async updateVersion() {
        /*
        TODO update transformation, user, structure
            xchludil: allow everything to be updated ->
             structureId CAN be changed -> then all versions will have it changed,
             nextId, previousId etc. all can be changed
             -> these can be done only by admin !
             all 'normal' changes are done by author (texture / assets changes, renaming, ...)
         */
        const { versionFound, msg, statusCode, version } = await this.validateAndFindTDObjectVersion();
        if (!versionFound || !version) return this.response.status(statusCode).json(msg);

        const user: User = this.request.user;
        if (
            user.status !== UserStatus.ACTIVE ||
            user.role === UserRole.COMMON ||
            (user.username !== version.username && user.role !== UserRole.ADMIN)
        ) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const { valid, code, message, name, modelFile } = await this.validate3DObjectUpload();
        if (!valid || !name || !modelFile) return this.response.status(code).send(message);

        /*
         * now that everything else is loaded, let's load the assets & textures
         * in this case it's only an array of ids
         * */
        const assets = validateField(this.request, 'assets', [(value) => !validator.isEmpty(value)]);
        const textures = validateField(this.request, 'textures', [(value) => !validator.isEmpty(value)]);
        if (!assets || !textures) return this.response.status(400).send('Textures and assets are required.');

        /*
         * array of ids (string, MongoID) should be sent via JSON.stringify()
         * aka now we try to load them with JSON.parse()
         * */
        const assetsParsedFromString = JSON.parse(assets);
        const texturesParsedFromString = JSON.parse(textures);
        if (typeof assetsParsedFromString !== 'object' || typeof texturesParsedFromString !== 'object') {
            return this.response.status(400).send('Textures and assets are required to be array (of IDs).');
        }

        /*
         * now both are verified to be an array
         * validation basically ends, from now on the 'non-valid' ids etc. are simply ignored
         * */
        const assetsToUpdate: Asset[] = [];
        const texturesToUpdate: Texture[] = [];

        /*
         * find Assets and Textures matching to sent IDs
         * (values that were not MongoID are ignored)
         * */
        for (const assetId of assetsParsedFromString) {
            if (typeof assetId !== 'string' || !validator.isMongoId(assetId)) continue;
            const asset: Asset | undefined = await this.assetModel.getById(assetId);
            if (asset === undefined) continue;
            assetsToUpdate.push(asset);
        }

        for (const textureId of texturesParsedFromString) {
            if (typeof textureId !== 'string' || !validator.isMongoId(textureId)) continue;
            const texture: Texture | undefined = await this.textureModel.getById(textureId);
            if (texture === undefined) continue;
            texturesToUpdate.push(texture);
        }

        /*
         * both of these behave like 'findOrCreate'()
         * so in case the records already exist they're only returned (and shared, no error)
         * */
        const mongoFile = await this.fileModel.save(modelFile);
        const model = await this.modelModel.create(modelFile.name, modelFile.size, mongoFile.id, this.request.user);

        version.name = name;
        version.model = model;
        version.assets = assetsToUpdate;
        version.textures = texturesToUpdate;

        await this.objectModel.update(version);

        const result = await this.objectModel.getById(version.id);
        if (!result) {
            return this.response.status(500).json('Error on server side, the updated version somehow got deleted');
        }
        return this.response.status(200).json(await this.projectTDObject(result));
    }

    /*
     * deletes a specific version of the TDObject
     * Authors note: (vancudan)
     * as of 13th of June 2020 I've no idea when this'll be used
     * */
    @Route(`${TDObjectsUrl}/:id/:version`, versionWithIdValidators, [Method.DELETE])
    public async deleteVersion() {
        const { versionFound, msg, statusCode, version } = await this.validateAndFindTDObjectVersion();
        if (!versionFound || !version) return this.response.status(statusCode).json(msg);

        const user: User = this.request.user;
        if (user.status !== UserStatus.ACTIVE || user.role !== UserRole.ADMIN) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        const previousObjId = version.previousId;
        const nextObjId = version.nextId;
        /*
         * if we tried to outright delete the object we'd be violating a FK constraint (on prev.nextId and next.prevId)
         * if we did not set nextId and previousId to null, we'd be violating the UNIQUE constraint
         * (when saving neighbours)
         * hence this update is currently necessary:
         * TODO or find a way to save all 3 of them in 1 transaction
         *   -> they're to be in ManyToMany relationship, so just delete the record of it being here
         *       instead of deleting the whole Asset record
         * not resolve yet?
         * */
        version.previousId = null;
        version.nextId = null;
        version.assets = [];
        version.textures = [];
        await this.objectModel.update(version);

        /*
         * now the "fix" of the necessary "pointers"
         * (remember, the history is saved in a form of a two-way-connected-list)
         * */
        if (nextObjId !== null) {
            const nextObj = await this.objectModel.getById(nextObjId);
            if (nextObj) {
                nextObj.previousId = previousObjId;
                await this.objectModel.update(nextObj);
            }
        }
        if (previousObjId !== null) {
            const previousObj = await this.objectModel.getById(previousObjId);
            if (previousObj) {
                previousObj.nextId = nextObjId;
                if (version.current) {
                    previousObj.current = true;
                }
                await this.objectModel.update(previousObj);
            }
        }
        await this.objectModel.delete(version.id);
        return this.response.status(204).json();
    }

    /*
     * Adds an asset file to the specific 3DObject
     * these files are thought to store e.g. config files required to successfully compose a texture and so on
     * there were thoughts of it also containing e.g.a preview of said TDObject
     * but after lenghty discussion this idea was dismissed
     * (it was said that IF that were to be implemented it should be separate from assets)
     * */
    @Route(`${TDObjectsUrl}/:id/assets`, uploadWithIdValidators, [Method.POST])
    public async uploadAsset() {
        const tdObject: TDObject | undefined = await this.getTDObject();
        if (!tdObject) {
            return this.response.status(404).send('3DObject not found.');
        }

        const user: User = this.request.user;
        if (
            user.status !== UserStatus.ACTIVE ||
            user.role === UserRole.COMMON ||
            (user.username !== tdObject.username && user.role !== UserRole.ADMIN)
        ) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const assetFile = validateFile(this.request, 'asset', MimeType.BINARY);
        if (!assetFile) {
            return this.response.status(400).send('Asset file is required.');
        }

        const mongoFile = await this.fileModel.save(assetFile);

        if (tdObject.assets.some((value) => value.id === mongoFile.id.toHexString())) {
            return this.response.status(400).send('This file is already assigned to this TDObject.');
        }

        /*
         * create method behaves like FindOrCreate so no problem
         * */
        const asset: Asset = await this.assetModel.create(assetFile.name, assetFile.size, mongoFile.id, user);
        tdObject.assets.push(asset);
        await this.objectModel.update(tdObject);

        return this.response.status(200).json(asset);
    }

    /*
     * Adds a texture file to the specific TDObject
     * it is thought that there'll be multiple texture in multiple formats
     * (e.g. web browser and mobile device requires fundamentally different formats to work with)
     * some textures need configs in assets to work properly, some does not
     * interpretation of which client needs what is to be implemented on client side of app
     * API is thought to NOT be responsible for recognizing a device and provide the optimal format
     * */
    @Route(`${TDObjectsUrl}/:id/textures`, uploadWithIdValidators, [Method.POST])
    public async uploadTexture() {
        const tdObject: TDObject | undefined = await this.getTDObject();
        if (!tdObject) {
            return this.response.status(404).send('3DObject not found.');
        }

        const user: User = this.request.user;
        if (
            user.status !== UserStatus.ACTIVE ||
            user.role === UserRole.COMMON ||
            (user.username !== tdObject.username && user.role !== UserRole.ADMIN)
        ) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        // other texture formats are to be added in the future
        const textureFile =
            validateFile(this.request, 'texture', MimeType.JPG_IMAGE, [Extension.JPEG, Extension.JPG]) ||
            validateFile(this.request, 'texture', MimeType.PNG_IMAGE, [Extension.PNG]);
        if (!textureFile) {
            return this.response.status(400).send('Texture file is required.');
        }

        const mongoFile = await this.fileModel.save(textureFile);

        if (tdObject.textures.some((value) => value.id === mongoFile.id.toHexString())) {
            return this.response.status(400).send('This file is already assigned to this TDObject.');
        }

        /*
         * create method behaves like FindOrCreate so no problem
         * */
        const texture: Texture = await this.textureModel.create(textureFile.name, textureFile.size, mongoFile.id, user);
        tdObject.textures.push(texture);
        await this.objectModel.update(tdObject);

        this.response.json(texture);
    }

    @Route(`${TDObjectsUrl}/:id/approval`, [idValidator], [Method.PATCH])
    public async approveOrDeclineTDObject() {
        const user: User = this.request.user;
        const object = await this.getTDObject();
        if (!object) return this.response.status(404).send('3D object with given ID not found');

        /*
         * new users are not activated, no permissions whatsoever
         * common users have only read-only access, aka no business here
         * */
        if (user.status !== UserStatus.ACTIVE || user.role === UserRole.COMMON) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        /*
         * if a modeller calls this endpoint -> submits it for approval (no more data needed)
         * */
        if (user.role === UserRole.MODELLER) {
            if (user.username === object.username) {
                return this.approvalProcessModeller(object);
            } else {
                return this.response.status(403).json('Only TDObject author can send it for approval');
            }
        }

        /*
         * from here on only ADMIN, APPROVER and HISTORIAN users are considered
         * aka not modeller here:
         * */
        if (object.status === TDObjectStatus.UNFINISHED && user.role !== UserRole.ADMIN) {
            return this.response.status(403).json('Only modeller and admin can send a model for approval.');
        }

        let approve: string | boolean = validateField(this.request, 'approve', [
            (value) => !validator.isEmpty(value),
            (value) => validator.isBoolean(value),
        ]);
        const role = validateField(this.request, 'role', [
            (value) => !validator.isEmpty(value),
            (value) => includes(toStringArray(UserRole), value),
        ]);
        /*
         * text is optional, if there is text present then new comment to this TDObject is created
         * if not, only TDObjectStatus is changed
         * */
        const text = validateField(this.request, 'text', []);
        if (!approve || !role) {
            return this.response.status(400).json('approve (boolean) is required, role (UserRole) is required.');
        }
        approve = approve === 'true'; // ugly, but only way I found to get a boolean
        /*
         * approver / historian trying to approve their own work -> nope
         * admin can do pretty much everything -> their responsibility
         * */
        if (user.username === object.username && user.role !== UserRole.ADMIN) {
            return this.response.status(403).json('You can not approve your own work, ask somebody else.');
        }

        /*
         * now the approval process itself
         * */
        if (role === UserRole.APPROVER && (user.role === UserRole.APPROVER || user.role === UserRole.ADMIN)) {
            return this.approvalProcessApprover(user, object, approve, text);
        } else if (role === UserRole.HISTORIAN && (user.role === UserRole.HISTORIAN || user.role === UserRole.ADMIN)) {
            return this.approvalProcessHistorian(user, object, approve, text);
        } else if (role === UserRole.MODELLER && user.role === UserRole.ADMIN) {
            return this.approvalProcessModeller(object);
        } else {
            return this.response.status(400).json('Invalid approval data: approver and historian approve for their respective roles only');
        }
    }

    /***********************************************************************************
     * TODO !!!! all of these are in Comment controller, should they be here or there?
     ***********************************************************************************/

    /*
     * lists comments related to said 3D Object and all its versions
     * ordered by date of creation of the comment
     * */
    @Route(`${TDObjectsUrl}/:id/comments`, [idValidator], [Method.GET])
    public async listTDObjectComments() {
        // TODO
        return this.response.status(501).send('Endpoint not implemented yet');
    }

    /*
     * lists comments related to said 3D Object and all its versions
     * ordered by date of creation of the comment
     * */
    @Route(`${TDObjectsUrl}/:id/:version/comments`, versionWithIdValidators, [Method.GET])
    public async listVersionComments() {
        // TODO
        return this.response.status(501).send('Endpoint not implemented yet');
    }

    /*
     * comment a 3DObject
     * */
    @Route(`${TDObjectsUrl}/:id/comments`, [idValidator], [Method.POST])
    public async createTDObjectComment() {
        // TODO
        return this.response.status(501).send('Endpoint not implemented yet');
    }

    /*
     * updates a comment (typos, additional infos)
     * */
    @Route(`${TDObjectsUrl}/:id/comments/:commentId`, [idValidator, commentIdValidator], [Method.PUT])
    public async updateComment() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * deletes a comment
     * */
    @Route(`${TDObjectsUrl}/:id/comments/:commentId`, [idValidator, commentIdValidator], [Method.DELETE])
    public async deleteComment() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }
}
