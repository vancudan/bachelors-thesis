import { param } from 'express-validator';
import Inject from '../di/Inject';
import PropertyModel from '../models/PropertyModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { PropertiesUrl } from '../routes/Urls';
import Controller from './Controller';

const idValidator = param('id', 'Invalid ID of Property supplied').isUUID();

export default class PropertyController extends Controller {
    @Inject(PropertyModel)
    private propertyModel: PropertyModel;

    /*
     * returns a list of all properties
     * TODO add ordering / filtering?
     * */
    @Route(`${PropertiesUrl}/`, [], [Method.GET])
    public async listProperties() {
        const properties = await this.propertyModel.getAll();
        this.response.status(200).json(properties);
    }

    /*
     * creates a new property
     * */
    @Route(`${PropertiesUrl}/`, [], [Method.POST])
    public async createProperty() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * returns a property detail
     * TODO is this endpoint necessary? what else will it return?
     *  */
    @Route(`${PropertiesUrl}/:id`, [idValidator], [Method.GET])
    public async retrievePropertyDetail() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * updates a property - new description?
     * */
    @Route(`${PropertiesUrl}/:id`, [idValidator], [Method.PUT])
    public async updateProperty() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }

    /*
     * deletes a property
     * TODO all TDObjects / textures / models should have this property removed - not keeping the id
     * */
    @Route(`${PropertiesUrl}/:id`, [idValidator], [Method.DELETE])
    public async deleteProperty() {
        // TODO
        this.response.status(501).json('Endpoint not implemented yet');
    }
}
