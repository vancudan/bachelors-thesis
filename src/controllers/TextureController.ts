import { param, query } from 'express-validator';
import { File } from 'formidable';
import Inject from '../di/Inject';
import { User, UserRole, UserStatus } from '../entity/User';
import FileModel from '../models/FileModel';
import TextureModel from '../models/TextureModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { TexturesUrl } from '../routes/Urls';
import { Extension } from '../utils/Extension';
import { MimeType } from '../utils/MimeType';
import { validateFile } from '../utils/MultipartFormDataValidators';
import FileDownloadController from './FileDownloadController';

const idValidator = param('id', 'Invalid ID of Texture supplied').isMongoId();

/*
 * Creating Textures is done via POST to 3DObjects/:id:/textures
 * */
@Inject(FileModel)
export default class TextureController extends FileDownloadController {
    @Inject(TextureModel)
    private textureModel: TextureModel;

    public constructor(fileModel: FileModel) {
        super(fileModel);
    }

    private getTexture() {
        return this.textureModel.getById(this.request.params.id);
    }

    private validateAndLoadTextureFile(): File | false {
        return (
            validateFile(this.request, 'texture', MimeType.JPG_IMAGE, [Extension.JPEG, Extension.JPG]) ||
            validateFile(this.request, 'texture', MimeType.PNG_IMAGE, [Extension.PNG])
        );
    }

    /*
     * TODO probably add some sort of filtering?
     * */
    @Route(
        TexturesUrl,
        [
            query('username')
                .optional()
                .isString(),
        ],
        [Method.GET],
    )
    public async listTextureFiles() {
        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        const textures = await this.textureModel.getAll();
        this.response.status(200).json(textures);
    }

    /*
     * initializes texture file download from the mongo database
     * */
    @Route(`${TexturesUrl}/:id`, [idValidator], [Method.GET])
    public async downloadTextureFile() {
        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        // 404 is handled 'inside' the downloadFile method
        return this.downloadFile(this.request.params.id, 'Texture file with given ID not found');
    }

    /*
     * uploads a new texture file
     * on upload new record of texture is created (if this file is in system already -> fail & 400)
     * this new record replaces all usage of the old one (aka links with TDObjects etc.)
     * and the old one should be deleted
     *
     * slightly complicated, possibly lots of updates (TDObjects) & only admin has permissions
     * TODO: implement
     * */
    @Route(`${TexturesUrl}/:id`, [idValidator], [Method.PUT])
    public async updateTextureFile() {
        const texture = await this.getTexture();
        if (!texture) return this.response.status(404).send('Texture file with given ID not found');

        const requestUser: User = this.request.user;
        if (requestUser.status !== UserStatus.ACTIVE || requestUser.role !== UserRole.ADMIN) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }

        const textureFile = this.validateAndLoadTextureFile();
        if (!textureFile) return this.response.status(400).json('Texture file and format are required');
        // TODO how the update behaves? -> check comment above the method

        this.response.status(501).json('Endpoint not finished yet');
    }

    /*
     * deletes the texture file
     * */
    @Route(`${TexturesUrl}/:id`, [idValidator], [Method.DELETE])
    public async deleteTextureFile() {
        const texture = await this.getTexture();
        if (!texture) return this.response.status(404).send('Texture file with given ID not found');

        const user: User = this.request.user;
        if (
            user.status !== UserStatus.ACTIVE ||
            user.role === UserRole.COMMON ||
            (user.username !== texture.username && user.role !== UserRole.ADMIN)
        ) {
            return this.response.status(403).json('You are not allowed to perform this action.');
        }
        await this.textureModel.delete(texture.id);
        this.response.status(204).send('Texture deleted');
    }
}
