import Inject from '../di/Inject';
import ChunkModel from '../models/ChunkModel';
import Method from '../routes/Method';
import Route from '../routes/Route';
import { ChunksUrl } from '../routes/Urls';
import Controller from './Controller';

/*
 * TODO this is super-basic form to work with, check other Controllers for more detail
 *       dividing Map into Chunks and their inner representation is not part of vancudan's bachelors thesis
 *       and is to be done in the future, quite probably by SP1 / SP2 teams on FIT ČVUT
 *       this here is only basic layout to be expanded
 * */
export default class ChunkController extends Controller {
    @Inject(ChunkModel)
    private chunkModel: ChunkModel;

    @Route(`${ChunksUrl}/`, [], [Method.GET])
    public async listChunks() {
        const chunks = await this.chunkModel.getAll();
        this.response.status(200).json(chunks);
    }
}
