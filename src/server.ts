import { MongoClient } from 'mongodb';
import { createConnection, getConnection } from 'typeorm';
import app from './app';
import injector from './di/Injector';

// Create TypeORM database connections pool
const initTypeORM = createConnection;

// Create MongoDB database connection
const mongoClient = new MongoClient(process.env.MONGO_URL!, { useNewUrlParser: true, useUnifiedTopology: true });
injector.registerServiceInstance(MongoClient.name, mongoClient);
const initMongoDB = () => mongoClient.connect();

// Start Express server
const server = app.listen(app.get('port'), async () => {
    await initTypeORM();
    await initMongoDB();

    console.info('App is running at http://localhost:%d in %s mode', app.get('port'), app.get('env'));
    console.info('Press CTRL-C to stop');
});

// Close database connections before exiting
process.on('exit', async () => {
    console.info('Closing connections...');
    await getConnection().close();
    await mongoClient.close();
    server.close();
});
