/*
 * This file is primary created to store the basic structure of VMCK endpoints
 * The main benefit (as of June 2020) is avoiding circular dependencies causing a crash on undefined values
 *
 * primary issue as of today:
 * these urls are being included in half of the project
 * by originally placing them in controllers there were tons of dependencies on these controllers
 * this pretty much simplifies the dependency structure and avoid the issue said above
 * */

export const AssetsUrl = '/assets';
// TODO maybe `${MapsUrl}/:id_map/chunks` ?
export const ChunksUrl = '/chunks';
export const EffectsUrl = '/effects';
export const MapsUrl = '/maps';
export const ModelsUrl = '/models';
// TODO what about `${UsersUrl}/:id_user/permissions` ?;
export const PermissionsUrl = '/permissions';
export const PluginsUrl = '/plugins';
export const PropertiesUrl = '/properties';
export const StructureUrl = '/structures';
export const TDObjectsUrl = '/3Dobjects';
export const TexturesUrl = '/textures';
export const UsersUrl = '/users';

// TODO should we keep it as simple words -> /comments and "assemble" it in the controllers? ?
export const CommentsUrl = `${TDObjectsUrl}/:id_TDObject/comments`;
