import { ValidationChain } from 'express-validator';
import { Middleware } from 'express-validator/src/base';
import Controller from '../controllers/Controller';
import injector from '../di/Injector';
import Method from './Method';
import router from './Router';

export default function Route(route: string, validators: Middleware[] | ValidationChain[] = [], methods: Method[] = [Method.GET]) {
    return (target: Object, propertyKey: string) => {
        const controllerRoute = Controller.createRoute(injector.register(target).name, propertyKey);
        // if (!(methods instanceof Array)) methods = [methods];
        // methods used to be of type Method | Method[]
        // and as it'll be still made an Array type here, I made it mandatory to be used in array
        // TODO in case of empty array used in validators any call results in 404
        for (const method of methods) router[method](route, validators, controllerRoute);
    };
}
