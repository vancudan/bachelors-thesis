import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, ObjectIdColumn } from 'typeorm';
import { User } from './User';

@Entity()
export class Asset {
    @ObjectIdColumn({ type: 'varchar' })
    id: string;

    @Column({ type: 'varchar', length: 100 })
    filename: string;

    @Column({ type: 'int' })
    fileSize: number;

    @CreateDateColumn()
    uploadedDate: Date;

    @Column({ type: 'varchar', length: 250, nullable: true })
    href: string | null;

    @Column({ type: 'varchar', length: 50 })
    username: string;

    @ManyToOne(() => User, { nullable: false })
    @JoinColumn({ name: 'username' })
    user: User;

    /*
    // changed to ManyToMany
    @Column({ type: 'uuid', nullable: true })
    tdobjectId: string | null;

    @ManyToOne(
        () => TDObject,
        (tdobject: TDObject) => tdobject.assets,
    )
    tdobject: TDObject;

    @Column({ type: 'uuid', nullable: true })
    structureId: string | null;

    @ManyToOne(
        () => Structure,
        (structure: Structure) => structure.assets,
    )
    structure: Structure;*/
}
