import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, ObjectIdColumn, OneToMany } from 'typeorm';
import { TDObject } from './TDObject';
import { User } from './User';

@Entity()
export class Model {
    @ObjectIdColumn({ type: 'varchar' })
    id: string;

    @Column({ type: 'varchar', length: 100 })
    filename: string;

    @Column({ type: 'int' })
    fileSize: number;

    @CreateDateColumn()
    uploadedDate: Date;

    @Column({ type: 'varchar', length: 250, nullable: true })
    href: string | null;

    @Column({ type: 'varchar', length: 50 })
    username: string;

    @ManyToOne(() => User, { nullable: false })
    @JoinColumn({ name: 'username' })
    user: User;

    @OneToMany(
        () => TDObject,
        (tdobject: TDObject) => tdobject.model,
        { eager: false },
    )
    tdobjects: TDObject[];
}
