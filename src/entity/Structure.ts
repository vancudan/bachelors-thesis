import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { Asset } from './Asset';
import { TDObject } from './TDObject';
import { User } from './User';

@Entity()
export class Structure {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'boolean', default: false })
    public: boolean;

    @Index()
    @Column({ type: 'varchar', length: 50 })
    city: string;

    @Column({ type: 'varchar', length: 100 })
    name: string;

    @Column({ type: 'varchar', length: 1000 })
    description: string;

    @CreateDateColumn()
    createdDate: Date;

    @Column({ type: 'varchar', length: 250, nullable: true })
    href: string | null;

    /*
    // TODO what about this solution? no transformation in controller needed
    // need to use JSON.parse to get the values back from db though
    @Column("simple-json")
    location: { lon: number, lat: number, alt: number };*/

    @Column({ type: 'real', array: true, nullable: true })
    location: number[] | null;

    @Column({ type: 'varchar', length: 50 })
    username: string;

    @ManyToOne(() => User, { nullable: false })
    @JoinColumn({ name: 'username' })
    user: User;

    @OneToOne(() => TDObject, { eager: true })
    @JoinColumn()
    defaultObject: TDObject;

    @OneToMany(
        () => TDObject,
        (tdObject) => tdObject.structure,
        { cascade: ['insert', 'update'], eager: true },
    )
    allVariants: TDObject[];

    @ManyToMany(() => Asset, { cascade: ['insert', 'update'], eager: true })
    @JoinTable()
    assets: Asset[];

    /*@ManyToOne((type) => Structure, (structure) => structure.chunk)
    chunk: Chunk;*/

    /*
     * TODO nice-to-have, will wait: add sth like "border" -> bounding box - seriously not me implementing it
     *  shared space, students working all at once in the space, working on their own TObject or sth
     * */
}
