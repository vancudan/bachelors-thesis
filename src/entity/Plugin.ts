import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Plugin {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 100 })
    name: string;

    @Column({ type: 'varchar', length: 250 })
    description: string;
}
