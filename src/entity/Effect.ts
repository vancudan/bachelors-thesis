import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Effect {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 100 })
    name: string;

    @Column({ type: 'varchar', length: 250 })
    description: string;
}
