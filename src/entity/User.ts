import { Column, CreateDateColumn, Entity, PrimaryColumn } from 'typeorm';

export enum UserStatus {
    NEW = 'new', // not verified by admin or whoever
    ACTIVE = 'active', // verified & active
    INACTIVE = 'inactive', // haven't been online for a specified amount of time, needs activation before usage
    DELETED = 'deleted', // speaks for itself
}

export enum UserRole {
    COMMON = 'commonUser', // normal user, read only permissions
    MODELLER = 'modeller', // verified & active, can create models etc.
    APPROVER = 'approver', // graphic designer, approves models created by modeller during approval process
    HISTORIAN = 'historian', // historian, approves models created by modeller during approval process
    ADMIN = 'admin', // speaks for itself
}

@Entity()
export class User {
    @PrimaryColumn({ type: 'varchar', length: 50 })
    username: string;

    @Column({ type: 'text' })
    token: string;

    @CreateDateColumn()
    createdDate: Date;

    @Column({ type: 'varchar', length: 50 })
    name: string;

    /*
     * if we use the 'role: UserRole' as docs suggests 'update' operation is pretty much impossible
     * repository update always throws an error that we can't use 'string' to update column of type 'enum'
     * */
    @Column({ type: 'enum', enum: UserRole, default: UserRole.COMMON })
    role: string;

    @Column({ type: 'enum', enum: UserStatus, default: UserStatus.NEW })
    status: string;

    @Column({ type: 'timestamp', nullable: true })
    deletedDate: Date | undefined;
}
