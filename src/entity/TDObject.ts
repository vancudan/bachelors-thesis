import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { Asset } from './Asset';
import { Model } from './Model';
import { Structure } from './Structure';
import { Texture } from './Texture';
import { User } from './User';

export enum TDObjectStatus {
    UNFINISHED = 'unfinished',
    SUBMITTED = 'submitted',
    DECLINED = 'declined',
    FIXED = 'fixed',
    APPROVED = 'approved',
}

@Entity({ name: 'TDObject' })
export class TDObject {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Index()
    @Column({ type: 'text' })
    name: string;

    @Column({ type: 'boolean', default: true })
    current: boolean;

    @Column({ type: 'boolean', default: false })
    isApprovedByApprover: boolean;

    @ManyToOne(() => User, { nullable: true })
    approverApproving: User;

    @Column({ type: 'boolean', default: false })
    isApprovedByHistorian: boolean;

    @ManyToOne(() => User, { nullable: true })
    historianApproving: User;

    @CreateDateColumn()
    createdDate: Date;

    // TODO check -> for what do I need href at TDObject and Structure -> only Model & Texture should matter
    // -> should this be the link to download the default model?
    @Column({ type: 'varchar', length: 250, nullable: true })
    href: string | null;

    @Column({ type: 'real', array: true, default: '{1,1,1}', nullable: false })
    version: number[];

    /*
     * if we use the 'status: TDObjectStatus' as docs suggests 'update' operation is pretty much impossible
     * repository update always throws an error that we can't use 'string' to update column of type 'enum'
     * */
    @Column({ type: 'enum', enum: TDObjectStatus, default: TDObjectStatus.UNFINISHED })
    status: string;

    @Column({ type: 'uuid', nullable: false })
    structureId: string;

    @JoinColumn({ name: 'structureId' })
    @ManyToOne(
        () => Structure,
        (structure: Structure) => structure.allVariants,
        { nullable: false, eager: false },
    )
    structure: Structure;

    @Column({ type: 'varchar', nullable: false })
    modelId: string;

    @ManyToOne(() => Model)
    @JoinColumn({ name: 'modelId' })
    model: Model;

    // TODO check manipulation with this as by default it's 'undefined' instead of empty array
    // TODO change to ManyToMany as assets and textures will be shared between versions
    @ManyToMany(() => Asset, { cascade: ['insert', 'update'], eager: true })
    @JoinTable()
    assets: Asset[];

    @ManyToMany(() => Texture, { cascade: ['insert', 'update'], eager: true })
    @JoinTable()
    textures: Texture[];

    @Column({ type: 'real', array: true, default: '{{0,0,0},{0,0,0},{0,0,0}}', nullable: false })
    transformation: number[][];

    @Column({ type: 'varchar', length: 50 })
    username: string;

    @ManyToOne(() => User, { nullable: false })
    @JoinColumn({ name: 'username' })
    user: User;

    @Column({ type: 'uuid', nullable: true })
    nextId: string | null;

    @OneToOne(() => TDObject, { nullable: true, eager: false })
    @JoinColumn({ name: 'nextId' })
    next: TDObject;

    @Column({ type: 'uuid', nullable: true })
    previousId: string | null;

    @OneToOne(() => TDObject, { nullable: true, eager: false })
    @JoinColumn({ name: 'previousId' })
    previous: TDObject;
}
