import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Map {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 100 })
    locality: string;

    /*@OneToMany(type => Map, map => map.chunks)
    chunks: Chunk[];*/
}
