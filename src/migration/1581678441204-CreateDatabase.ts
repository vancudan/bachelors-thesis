import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDatabase1581678441204 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        /*
        // original types: (formats were moved into records in table "property")
        await queryRunner.query(`CREATE TYPE "3d_object_format_enum" AS ENUM('3DS', 'BLEND', 'FBX', 'MAX', 'OBJ', 'SFB')`, undefined);
        await queryRunner.query(`CREATE TYPE "3d_object_status_enum" AS ENUM('incomplete', 'complete', 'approved')`, undefined);
        */
        await queryRunner.query(
            `CREATE TYPE "model_texture_status_enum" AS ENUM('unfinished', 'submitted', 'declined', 'fixed', 'approved');`,
            undefined,
        );

        /*
        // original user by J. Máca
        await queryRunner.query( `CREATE TABLE "user" (
            "username" character varying NOT NULL,
            "token" text NOT NULL,
            CONSTRAINT "PK_78a916df40e02a9deb1c4b75edb" PRIMARY KEY ("username")
            )`,
            undefined,
        );
        */
        // table name "user" is unusable as "user" table refers to db users (separate matter from intended vmck users)
        await queryRunner.query(
            `CREATE TABLE "user" (
                        "username"       VARCHAR(50) NOT NULL,
                        "token"          VARCHAR(200) NOT NULL,
                        "name"           VARCHAR(50) NOT NULL,
                        "createdDate"    TIMESTAMP NOT NULL DEFAULT now(),
                        CONSTRAINT "user_pk" PRIMARY KEY ( "username" ),
                        CONSTRAINT "user_token_un" UNIQUE ( "token" ),
                        CONSTRAINT "user_username_un" unique ( "username" )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "permission" (
                        "id"            SERIAL NOT NULL,
                        "description"   VARCHAR(250) NOT NULL,
                        CONSTRAINT "permission_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "permission_description_un" UNIQUE ( "description" )
                    )`,
            undefined,
        );

        /*
        in future, there will be a map (aka a city) and that'll be divided into chunks
        the map and chunks appearance and storing will be decided in future SP1/2 projects
        */
        await queryRunner.query(
            `CREATE TABLE "map" (
                        "id"         SERIAL NOT NULL,
                        "locality"   VARCHAR(100) NOT NULL,
                        CONSTRAINT "map_pk" PRIMARY KEY ( id )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "chunk" (
                        "id"       SERIAL NOT NULL,
                        "mapId"    INTEGER NOT NULL,
                        CONSTRAINT "chunk_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "chunk_map_fk" FOREIGN KEY ( "mapId" ) REFERENCES "map" ( "id" )
                    )`,
            undefined,
        );

        /*
        structure is basically a superobject to 3DObject, containing most of the needed metadata
        3DObject is understood as a variant of a structure
        */
        await queryRunner.query(
            `CREATE TABLE "structure" (
                        "id"              UUID NOT NULL DEFAULT uuid_generate_v4(),
                        "public"          BOOLEAN NOT NULL DEFAULT FALSE,
                        "city"            VARCHAR(50) NOT NULL,
                        "name"            VARCHAR(100) NOT NULL,
                        "description"     VARCHAR(1000) NOT NULL,
                        "createdDate"     TIMESTAMP NOT NULL DEFAULT now(),
                        "uploadedDate"    TIMESTAMP NOT NULL DEFAULT now(),
                        "href"            VARCHAR(250),
                        "location"        REAL[3],
                        "username"        VARCHAR(50) NOT NULL,
                        "chunkId"         INTEGER,
                        CONSTRAINT "structure_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "structure_chunk_fk" FOREIGN KEY ( "chunkId" ) REFERENCES "chunk" ( "id" ),
                        CONSTRAINT "structure_user_fk" FOREIGN KEY ( "username" ) REFERENCES "user" ( "username" )
                    )`,
            undefined,
        );

        /*
        // original 3DObject (not counting with "structure" back then)
        // now (May 2020) the 3DObject is understood as a variant of the structure ->
        // aka structure == castle (aka meta info about it)
        // 3DObject == the castle while it's snowing in winter at 6 in the morning (example)
        await queryRunner.query(
            `CREATE TABLE "3d_object" (
                    "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
                    "name" text NOT NULL,
                    "format" "3d_object_format_enum" NOT NULL,
                    "model" character(24) NOT NULL,
                    "createdDate" TIMESTAMP NOT NULL DEFAULT now(),
                    "version" integer NOT NULL DEFAULT 1,
                    "status" "3d_object_status_enum" NOT NULL DEFAULT 'incomplete',
                    "assets" character(24) array NOT NULL DEFAULT '{}'::char[],
                    "textures" character(24) array NOT NULL DEFAULT '{}'::char[],
                    "current" boolean NOT NULL DEFAULT true,
                    "username" character varying NOT NULL,
                    "previousId" uuid,
                    CONSTRAINT "REL_0ba41d8c87bf010bad09e80047" UNIQUE ("previousId"),
                    CONSTRAINT "PK_d2ab7ee1c3148a3a5399414b7ec" PRIMARY KEY ("id")
                    )`,
            undefined,
        );
        await queryRunner.query(`CREATE INDEX "IDX_517159819e04b7a3391cf49605" ON "3d_object" ("name") `, undefined);
        await queryRunner.query( `
            ALTER TABLE "3d_object"
            ADD CONSTRAINT "FK_0ba41d8c87bf010bad09e800479"
                FOREIGN KEY ("previousId")
                REFERENCES "3d_object"("id")
                ON DELETE NO ACTION ON UPDATE NO ACTION`,
             undefined,
        );
        await queryRunner.query( `
            ALTER TABLE "3d_object"
                ADD CONSTRAINT "FK_b9ef00c2d2bc7be904a9a169dd2"
                FOREIGN KEY ("username")
                REFERENCES "user"("username")
                ON DELETE NO ACTION ON UPDATE NO ACTION`,
            undefined,
         );
        */
        await queryRunner.query(
            `CREATE TABLE "TDObject" (
                        "id"              UUID NOT NULL DEFAULT uuid_generate_v4(),
                        "name"            TEXT NOT NULL,
                        "current"         BOOLEAN NOT NULL DEFAULT true,
                        "createdDate"     TIMESTAMP NOT NULL DEFAULT now(),
                        "href"            VARCHAR(250),
                        "version"         INTEGER[3] NOT NULL DEFAULT '{1, 1, 1}',
                        "structureId"     UUID NOT NULL,
                        "transformation"  REAL[3][3] DEFAULT '{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}',
                        "username"        VARCHAR(50) NOT NULL,
                        "previousId"      uuid,
                        CONSTRAINT "TDObject_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "TDObject_structure_fk" FOREIGN KEY ( "structureId" ) REFERENCES "structure" ( "id" )
                    )`,
            undefined,
        );

        /*
        model, texture and asset -> 3 tables representing uploaded files of different purposes
        all tables contain "href" attribute to download the files themselves
        the table is used for storing metadata only
        model == the 3D model itself - created via blender / 3DStudioMax etc
        texture == speaks for itself, needed with model
        asset == a config file of similar files needed for correct composition of textures on model
            -> asset file is not needed every time, f.e. phones etc ill need a .sfa config to compose a .sfb file
               but no config file is needed when composing a .obj file
        */
        await queryRunner.query(
            `CREATE TABLE "model" (
                        "id"              SERIAL NOT NULL,
                        "status"          "model_texture_status_enum" NOT NULL DEFAULT 'unfinished',
                        "filename"        VARCHAR(100) NOT NULL,
                        "fileSize"        INTEGER NOT NULL,
                        "uploadedDate"    TIMESTAMP NOT NULL DEFAULT now(),
                        "href"            VARCHAR(250) NOT NULL,
                        "username"        VARCHAR(50) NOT NULL,
                        "TDObjectId"      UUID NOT NULL,
                        CONSTRAINT "model_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "model_user_fk" FOREIGN KEY ( "username" ) REFERENCES "user" ( "username" ),
                        CONSTRAINT "model_TDObject_fk" FOREIGN KEY ( "TDObjectId" ) REFERENCES "TDObject" ( "id" )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "texture" (
                        "id"              SERIAL NOT NULL,
                        "status"          "model_texture_status_enum" NOT NULL DEFAULT 'unfinished',
                        "filename"        VARCHAR(100) NOT NULL,
                        "fileSize"        INTEGER NOT NULL,
                        "uploadedDate"    TIMESTAMP NOT NULL DEFAULT now(),
                        "href"            VARCHAR(250) NOT NULL,
                        "username"        VARCHAR(50) NOT NULL,
                        "TDObjectId"      UUID NOT NULL,
                        CONSTRAINT "texture_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "texture_user_fk" FOREIGN KEY ( "username" ) REFERENCES "user" ( "username" ),
                        CONSTRAINT "texture_TDObject_fk" FOREIGN KEY ( "TDObjectId" ) REFERENCES "TDObject" ( "id" )
                    )`,
            undefined,
        );

        // TODO: probably a constraint will be needed, as one asset file should quite probably belong to EITHER structure OR 3DObject
        await queryRunner.query(
            `CREATE TABLE "asset" (
                        "id"              SERIAL NOT NULL,
                        "filename"        VARCHAR(100) NOT NULL,
                        "fileSize"        INTEGER NOT NULL,
                        "uploadedDate"    TIMESTAMP NOT NULL DEFAULT now(),
                        "href"            VARCHAR(250) NOT NULL,
                        "structureId"     UUID NOT NULL,
                        "TDObjectId"      UUID NOT NULL,
                        CONSTRAINT "asset_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "asset_structure_fk" FOREIGN KEY ( "structureId" ) REFERENCES "structure" ( "id" ),
                        CONSTRAINT "asset_TDObject_fk" FOREIGN KEY ( "TDObjectId" ) REFERENCES "TDObject" ( "id" )
                    )`,
            undefined,
        );
        /*
        comments are often needed during approval process, as of reasons of rejecting the model and writing required modifications
        TODO: should be expanded to be able to comment during texture approval as well
        */
        await queryRunner.query(
            `CREATE TABLE "comment" (
                        "id"          SERIAL NOT NULL,
                        "text"        VARCHAR(250) NOT NULL,
                        "modelId"     INTEGER NOT NULL,
                        "username"    VARCHAR(50) NOT NULL,
                        CONSTRAINT "comment_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "comment_model_fk" FOREIGN KEY ( "modelId" ) REFERENCES "model" ( "id" ),
                        CONSTRAINT "comment_user_fk" FOREIGN KEY ( "username" ) REFERENCES "user" ( "username" )
                    )`,
            undefined,
        );

        /*
        these tables are used for storing information about used plugins to generate different textures
        these records will be used to compare performance of different plugins and their optimization
        */
        await queryRunner.query(
            `CREATE TABLE "effect" (
                        "id"            SERIAL NOT NULL,
                        "name"          VARCHAR(100) NOT NULL,
                        "description"   VARCHAR(250) NOT NULL,
                        CONSTRAINT "effect_pk" PRIMARY KEY ( "id" )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "plugin" (
                        "id"            SERIAL NOT NULL,
                        "name"          VARCHAR(100) NOT NULL,
                        "description"   VARCHAR(250) NOT NULL,
                        CONSTRAINT "plugin_pk" PRIMARY KEY ( "id" )
                    )`,
            undefined,
        );

        /*
        Instead of putting all potential search parameters into the objects themselves they're now put in the "Property" table
        those being things like time of day, season, weather, level of detail, etc.
        this list is to be changed in the future, but as we're unable to predict how, we used this variant of handling it
        when all these params are in property table, there's no need to change the API behaviour or db scheme
        whenever some are deemed obsolete or created anew, it's just a creation of a new record
        */
        await queryRunner.query(
            `CREATE TABLE "property" (
                        "id"      SERIAL NOT NULL,
                        "key"     VARCHAR(50) NOT NULL,
                        "value"   VARCHAR(100) NOT NULL,
                        CONSTRAINT "property_pk" PRIMARY KEY ( "id" ),
                        CONSTRAINT "property_key_value_un" UNIQUE ( "key", "value" )
                    )`,
            undefined,
        );

        /*
        from here on the tables are just simple decompositions of the m:n relationships in the db
        see db schema for details
        */
        // TODO - are these actually necessary? -> aka won't handling FKs in entities solve it on its own?
        /*await queryRunner.query(
            `CREATE TABLE "effect_created_by_plugin" (
                        "effect_id"   INTEGER NOT NULL,
                        "plugin_id"   INTEGER NOT NULL,
                        CONSTRAINT "effect_created_by_plugin_pk" PRIMARY KEY ( "effect_id", "plugin_id" ),
                        CONSTRAINT "effect_created_plugin_eff_fk" FOREIGN KEY ( "effect_id" ) REFERENCES "effect" ( "id" ),
                        CONSTRAINT "effect_created_plugin_plug_fk" FOREIGN KEY ( "plugin_id" ) REFERENCES "plugin" ( "id" )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "structure_has_properties" (
                        "structure_id"   UUID NOT NULL,
                        "property_id"    INTEGER NOT NULL,
                        CONSTRAINT "structure_has_properties_pk" PRIMARY KEY ( "structure_id", "property_id" ),
                        CONSTRAINT "structure_properties_prop_fk" FOREIGN KEY ( "property_id" ) REFERENCES "property" ( "id" ),
                        CONSTRAINT "structure_properties_struct_fk" FOREIGN KEY ( "structure_id" ) REFERENCES "structure" ( "id" )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "texture_has_effects" (
                        "texture_id"   INTEGER NOT NULL,
                        "effect_id"    INTEGER NOT NULL,
                        CONSTRAINT "texture_has_effects_pk" PRIMARY KEY ( "texture_id", "effect_id" ),
                        CONSTRAINT "texture_effects_effect_fk" FOREIGN KEY ( "effect_id" ) REFERENCES "effect" ( "id" ),
                        CONSTRAINT "texture_effects_texture_fk" FOREIGN KEY ( "texture_id" ) REFERENCES "texture" ( "id" )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "texture_has_properties" (
                        "texture_id"    INTEGER NOT NULL,
                        "property_id"   INTEGER NOT NULL,
                        CONSTRAINT "texture_has_properties_pk" PRIMARY KEY ( "texture_id", "property_id" ),
                        CONSTRAINT "texture_properties_prop_fk" FOREIGN KEY ( "property_id" ) REFERENCES "property" ( "id" ),
                        CONSTRAINT "texture_properties_text_fk" FOREIGN KEY ( "texture_id" ) REFERENCES "texture" ( "id" )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "user_has_permissions" (
                        "username"        VARCHAR(50) NOT NULL,
                        "permission_id"   INTEGER NOT NULL,
                        CONSTRAINT "user_has_permissions_pk" PRIMARY KEY ( "username", "permission_id" ),
                        CONSTRAINT "user_permissions_perm_fk" FOREIGN KEY ( "permission_id" ) REFERENCES "permission" ( "id" ),
                        CONSTRAINT "user_permissions_user_fk" FOREIGN KEY ( "username" ) REFERENCES "user" ( "username" )
                    )`,
            undefined,
        );
        await queryRunner.query(
            `CREATE TABLE "TDObject_has_properties" (
                        "TDObject_id"   UUID NOT NULL,
                        "property_id"   INTEGER NOT NULL,
                        CONSTRAINT "TDObject_properties_pk" PRIMARY KEY ( "TDObject_id", "property_id" ),
                        CONSTRAINT "TDObject_properties_prop_fk" FOREIGN KEY ( "property_id" ) REFERENCES "property" ( "id" ),
                        CONSTRAINT "TDObject_properties_TDO_fk" FOREIGN KEY ( "TDObject_id" ) REFERENCES "TDObject" ( "id" )
                    )`,
            undefined,
        );*/
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        /*await queryRunner.query(`DROP TABLE "effect_created_by_plugin"`, undefined);
        await queryRunner.query(`DROP TABLE "structure_has_properties"`, undefined);
        await queryRunner.query(`DROP TABLE "texture_has_effects"`, undefined);
        await queryRunner.query(`DROP TABLE "texture_has_properties"`, undefined);
        await queryRunner.query(`DROP TABLE "user_has_permissions"`, undefined);
        await queryRunner.query(`DROP TABLE "TDObject_has_properties"`, undefined);*/

        await queryRunner.query(`DROP TABLE "property"`, undefined);
        await queryRunner.query(`DROP TABLE "permission"`, undefined);
        await queryRunner.query(`DROP TABLE "effect"`, undefined);
        await queryRunner.query(`DROP TABLE "plugin"`, undefined);

        await queryRunner.query(`DROP TABLE "comment"`, undefined);

        await queryRunner.query(`DROP TABLE "model"`, undefined);
        await queryRunner.query(`DROP TABLE "texture"`, undefined);
        await queryRunner.query(`DROP TABLE "asset"`, undefined);

        await queryRunner.query(`DROP TABLE "TDObject"`, undefined);
        await queryRunner.query(`DROP TABLE "structure"`, undefined);

        await queryRunner.query(`DROP TABLE "user"`, undefined);

        await queryRunner.query(`DROP TABLE "chunk"`, undefined);
        await queryRunner.query(`DROP TABLE "map"`, undefined);

        await queryRunner.query(`DROP TYPE "model_texture_status_enum"`, undefined);
        /*await queryRunner.query(`ALTER TABLE "3d_object" DROP CONSTRAINT "FK_b9ef00c2d2bc7be904a9a169dd2"`, undefined);
        await queryRunner.query(`ALTER TABLE "3d_object" DROP CONSTRAINT "FK_0ba41d8c87bf010bad09e800479"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_517159819e04b7a3391cf49605"`, undefined);
        await queryRunner.query(`DROP TABLE "3d_object"`, undefined);
        await queryRunner.query(`DROP TYPE "3d_object_status_enum"`, undefined);
        await queryRunner.query(`DROP TYPE "3d_object_format_enum"`, undefined);
        await queryRunner.query(`DROP TABLE "user"`, undefined);*/
    }
}
