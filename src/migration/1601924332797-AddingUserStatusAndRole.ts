import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddingUserStatusAndRole1601924332797 implements MigrationInterface {
    name = 'AddingUserStatusAndRole1601924332797';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "user_role_enum" AS ENUM('commonUser', 'modeller', 'approver', 'historian', 'admin')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "role" "user_role_enum" NOT NULL DEFAULT 'commonUser'`);
        await queryRunner.query(`CREATE TYPE "user_status_enum" AS ENUM('new', 'active', 'inactive', 'deleted')`);
        await queryRunner.query(`ALTER TABLE "user" ADD "status" "user_status_enum" NOT NULL DEFAULT 'new'`);
        await queryRunner.query(`ALTER TABLE "user" ADD "deletedDate" TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'::real[]`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'::real[]`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "deletedDate"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "status"`);
        await queryRunner.query(`DROP TYPE "user_status_enum"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "role"`);
        await queryRunner.query(`DROP TYPE "user_role_enum"`);
    }
}
