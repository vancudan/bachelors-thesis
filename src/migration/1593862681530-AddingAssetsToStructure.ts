import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddingAssetsToStructure1593862681530 implements MigrationInterface {
    name = 'AddingAssetsToStructure1593862681530';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" ADD "structureId" uuid`);
        await queryRunner.query(
            `ALTER TABLE "asset" ADD CONSTRAINT "FK_15de9ac7ab04da87311e4d0c00c" FOREIGN KEY ("structureId") REFERENCES "structure"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "FK_15de9ac7ab04da87311e4d0c00c"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "structureId"`);
    }
}
