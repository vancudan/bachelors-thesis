import { MigrationInterface, QueryRunner } from 'typeorm';

export class DbUpdateVancudanBPStart1592830459559 implements MigrationInterface {
    name = 'DbUpdateVancudanBPStart1592830459559';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "asset_structure_fk"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "asset_TDObject_fk"`);
        await queryRunner.query(`ALTER TABLE "chunk" DROP CONSTRAINT "chunk_map_fk"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "comment_model_fk"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "comment_user_fk"`);
        await queryRunner.query(`ALTER TABLE "model" DROP CONSTRAINT "model_user_fk"`);
        await queryRunner.query(`ALTER TABLE "model" DROP CONSTRAINT "model_TDObject_fk"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP CONSTRAINT "structure_user_fk"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP CONSTRAINT "structure_chunk_fk"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "TDObject_structure_fk"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP CONSTRAINT "texture_user_fk"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP CONSTRAINT "texture_TDObject_fk"`);
        await queryRunner.query(`ALTER TABLE "property" DROP CONSTRAINT "property_key_value_un"`);
        await queryRunner.query(`ALTER TABLE "permission" DROP CONSTRAINT "permission_description_un"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "user_token_un"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT IF EXISTS "PK_78a916df40e02a9deb1c4b75edb"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "structureId"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "TDObjectId"`);
        await queryRunner.query(`ALTER TABLE "chunk" DROP COLUMN "mapId"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "modelId"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "TDObjectId"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "uploadedDate"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "chunkId"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "status"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "TDObjectId"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "filename" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "text"`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "text" character(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "effect" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "effect" ADD "name" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "effect" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "effect" ADD "description" character(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "map" DROP COLUMN "locality"`);
        await queryRunner.query(`ALTER TABLE "map" ADD "locality" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "filename" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TYPE "public"."model_texture_status_enum" RENAME TO "model_status_enum_old"`);
        await queryRunner.query(`CREATE TYPE "model_status_enum" AS ENUM('unfinished', 'submitted', 'declined', 'fixed', 'approved')`);
        await queryRunner.query(`ALTER TABLE "model" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(
            `ALTER TABLE "model" ALTER COLUMN "status" TYPE "model_status_enum" USING "status"::"text"::"model_status_enum"`,
        );
        await queryRunner.query(`ALTER TABLE "model" ALTER COLUMN "status" SET DEFAULT 'unfinished'`);
        await queryRunner.query(`DROP TYPE "model_status_enum_old"`);
        await queryRunner.query(`ALTER TABLE "permission" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "permission" ADD "description" character(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "plugin" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "plugin" ADD "name" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "plugin" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "plugin" ADD "description" character(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "property" DROP COLUMN "key"`);
        await queryRunner.query(`ALTER TABLE "property" ADD "key" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "property" DROP COLUMN "value"`);
        await queryRunner.query(`ALTER TABLE "property" ADD "value" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "city"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "city" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "name" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "description" character(1000) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "username" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "version"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "version" real array NOT NULL DEFAULT '{1,1,1}'::real[]`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'::real[]`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "username" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD CONSTRAINT "UQ_89d84f35a0c1a4af39a79e65c5a" UNIQUE ("previousId")`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "filename" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "username" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "PK_78a916df40e02a9deb1c4b75edb" PRIMARY KEY ("username")`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "token"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "token" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "name" character(50) NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_c8b1148ac5be5a544faba06c5d" ON "structure" ("city") `);
        await queryRunner.query(`CREATE INDEX "IDX_2fdc7a8f7749be8d90a530317a" ON "TDObject" ("name") `);
        await queryRunner.query(
            `ALTER TABLE "structure" ADD CONSTRAINT "FK_6ac420c80527f93119a309d2e08" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_2892276fb85cf7dc726d7689a74" FOREIGN KEY ("structureId") REFERENCES "structure"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_b329d37177582f70c4d93ac77af" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_89d84f35a0c1a4af39a79e65c5a" FOREIGN KEY ("previousId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_89d84f35a0c1a4af39a79e65c5a"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_b329d37177582f70c4d93ac77af"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_2892276fb85cf7dc726d7689a74"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP CONSTRAINT "FK_6ac420c80527f93119a309d2e08"`);
        await queryRunner.query(`DROP INDEX "IDX_2fdc7a8f7749be8d90a530317a"`);
        await queryRunner.query(`DROP INDEX "IDX_c8b1148ac5be5a544faba06c5d"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "name" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "token"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "token" character varying(200) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "user_token_un" UNIQUE ("token")`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "PK_78a916df40e02a9deb1c4b75edb"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "PK_78a916df40e02a9deb1c4b75edb" PRIMARY KEY ("username")`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "filename" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "UQ_89d84f35a0c1a4af39a79e65c5a"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "version"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "version" integer array NOT NULL DEFAULT '{1,1,1}'`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "description" character varying(1000) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "name" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "city"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "city" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "property" DROP COLUMN "value"`);
        await queryRunner.query(`ALTER TABLE "property" ADD "value" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "property" DROP COLUMN "key"`);
        await queryRunner.query(`ALTER TABLE "property" ADD "key" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "plugin" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "plugin" ADD "description" character varying(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "plugin" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "plugin" ADD "name" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "permission" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "permission" ADD "description" character varying(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "permission" ADD CONSTRAINT "permission_description_un" UNIQUE ("description")`);
        await queryRunner.query(`CREATE TYPE "model_status_enum_old" AS ENUM('unfinished', 'submitted', 'declined', 'fixed', 'approved')`);
        await queryRunner.query(`ALTER TABLE "model" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(
            `ALTER TABLE "model" ALTER COLUMN "status" TYPE "model_status_enum_old" USING "status"::"text"::"model_status_enum_old"`,
        );
        await queryRunner.query(`ALTER TABLE "model" ALTER COLUMN "status" SET DEFAULT 'unfinished'`);
        await queryRunner.query(`DROP TYPE "model_status_enum"`);
        await queryRunner.query(`ALTER TYPE "model_status_enum_old" RENAME TO  "model_texture_status_enum"`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "filename" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "map" DROP COLUMN "locality"`);
        await queryRunner.query(`ALTER TABLE "map" ADD "locality" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "effect" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "effect" ADD "description" character varying(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "effect" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "effect" ADD "name" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "text"`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "text" character varying(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "filename" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "TDObjectId" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "status" "model_texture_status_enum" NOT NULL DEFAULT 'unfinished'`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "chunkId" integer`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "uploadedDate" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "model" ADD "TDObjectId" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "modelId" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "chunk" ADD "mapId" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "TDObjectId" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "structureId" uuid NOT NULL`);
        await queryRunner.query(`ALTER TABLE "property" ADD CONSTRAINT "property_key_value_un" UNIQUE ("key", "value")`);
        await queryRunner.query(
            `ALTER TABLE "texture" ADD CONSTRAINT "texture_TDObject_fk" FOREIGN KEY ("TDObjectId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "texture" ADD CONSTRAINT "texture_user_fk" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "TDObject_structure_fk" FOREIGN KEY ("structureId") REFERENCES "structure"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "structure" ADD CONSTRAINT "structure_chunk_fk" FOREIGN KEY ("chunkId") REFERENCES "chunk"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "structure" ADD CONSTRAINT "structure_user_fk" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "model" ADD CONSTRAINT "model_TDObject_fk" FOREIGN KEY ("TDObjectId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "model" ADD CONSTRAINT "model_user_fk" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "comment" ADD CONSTRAINT "comment_user_fk" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "comment" ADD CONSTRAINT "comment_model_fk" FOREIGN KEY ("modelId") REFERENCES "model"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "chunk" ADD CONSTRAINT "chunk_map_fk" FOREIGN KEY ("mapId") REFERENCES "map"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "asset" ADD CONSTRAINT "asset_TDObject_fk" FOREIGN KEY ("TDObjectId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "asset" ADD CONSTRAINT "asset_structure_fk" FOREIGN KEY ("structureId") REFERENCES "structure"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }
}
