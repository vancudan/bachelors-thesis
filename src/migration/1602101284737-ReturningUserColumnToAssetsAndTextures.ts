import { MigrationInterface, QueryRunner } from 'typeorm';

export class ReturningUserColumnToAssetsAndTextures1602101284737 implements MigrationInterface {
    name = 'ReturningUserColumnToAssetsAndTextures1602101284737';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'::real[]`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'::real[]`);
        await queryRunner.query(
            `ALTER TABLE "asset" ADD CONSTRAINT "FK_438d1afff674a4b0d55ff5ed4f3" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "texture" ADD CONSTRAINT "FK_7d2c0f31cb0c83330943a142b69" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "model" ADD CONSTRAINT "FK_ac481add37a1e8793b7b52b8e0f" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "model" DROP CONSTRAINT "FK_ac481add37a1e8793b7b52b8e0f"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP CONSTRAINT "FK_7d2c0f31cb0c83330943a142b69"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "FK_438d1afff674a4b0d55ff5ed4f3"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "username"`);
    }
}
