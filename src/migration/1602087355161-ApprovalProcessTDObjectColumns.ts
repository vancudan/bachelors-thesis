import { MigrationInterface, QueryRunner } from 'typeorm';

export class ApprovalProcessTDObjectColumns1602087355161 implements MigrationInterface {
    name = 'ApprovalProcessTDObjectColumns1602087355161';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "isApprovedByApprover" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "isApprovedByHistorian" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "approverApprovingUsername" character varying(50)`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "historianApprovingUsername" character varying(50)`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'::real[]`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'::real[]`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_8bd9aca26d5686f4a84281fee3e" FOREIGN KEY ("approverApprovingUsername") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_439e36a616560d1f73a27b1bc8d" FOREIGN KEY ("historianApprovingUsername") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_439e36a616560d1f73a27b1bc8d"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_8bd9aca26d5686f4a84281fee3e"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "historianApprovingUsername"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "approverApprovingUsername"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "isApprovedByHistorian"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "isApprovedByApprover"`);
    }
}
