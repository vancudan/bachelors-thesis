import { MigrationInterface, QueryRunner } from 'typeorm';

export class VersioningTexturesAndAssets1601390979913 implements MigrationInterface {
    name = 'VersioningTexturesAndAssets1601390979913';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "FK_15de9ac7ab04da87311e4d0c00c"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "FK_8f6cc0c5ebf13585d2a01426477"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP CONSTRAINT "FK_fe6f78da89a25574b736cd872cd"`);
        await queryRunner.query(
            `CREATE TABLE "td_object_assets_asset" ("tDObjectId" uuid NOT NULL, "asset_id" character varying NOT NULL, CONSTRAINT "PK_8cfc4d4afcfe7101da7aff578ca" PRIMARY KEY ("tDObjectId", "asset_id"))`,
        );
        await queryRunner.query(`CREATE INDEX "IDX_faaa50d743735bafa1caf51082" ON "td_object_assets_asset" ("tDObjectId") `);
        await queryRunner.query(`CREATE INDEX "IDX_01ac4ab7ee862d2159c1848217" ON "td_object_assets_asset" ("asset_id") `);
        await queryRunner.query(
            `CREATE TABLE "td_object_textures_texture" ("tDObjectId" uuid NOT NULL, "texture_id" character varying NOT NULL, CONSTRAINT "PK_c0abfdf10e981356603c5306d0f" PRIMARY KEY ("tDObjectId", "texture_id"))`,
        );
        await queryRunner.query(`CREATE INDEX "IDX_f8e2a8d67181a1959dc4989769" ON "td_object_textures_texture" ("tDObjectId") `);
        await queryRunner.query(`CREATE INDEX "IDX_14e60e31de8aea4216f284c7c0" ON "td_object_textures_texture" ("texture_id") `);
        await queryRunner.query(
            `CREATE TABLE "structure_assets_asset" ("structureId" uuid NOT NULL, "asset_id" character varying NOT NULL, CONSTRAINT "PK_dc2738d6d41a94ab1db8374ee0e" PRIMARY KEY ("structureId", "asset_id"))`,
        );
        await queryRunner.query(`CREATE INDEX "IDX_afd6dc8b3bc401dd318f44acfe" ON "structure_assets_asset" ("structureId") `);
        await queryRunner.query(`CREATE INDEX "IDX_6eec03e8ad80d3e80058021ed9" ON "structure_assets_asset" ("asset_id") `);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "tdobjectId"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "structureId"`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "status"`);
        await queryRunner.query(`DROP TYPE "public"."model_status_enum"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "tdobjectId"`);
        await queryRunner.query(`CREATE TYPE "TDObject_status_enum" AS ENUM('unfinished', 'submitted', 'declined', 'fixed', 'approved')`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "status" "TDObject_status_enum" NOT NULL DEFAULT 'unfinished'`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'::real[]`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "modelId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "UQ_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'::real[]`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de" FOREIGN KEY ("modelId") REFERENCES "model"("_id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "td_object_assets_asset" ADD CONSTRAINT "FK_faaa50d743735bafa1caf510828" FOREIGN KEY ("tDObjectId") REFERENCES "TDObject"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "td_object_assets_asset" ADD CONSTRAINT "FK_01ac4ab7ee862d2159c18482171" FOREIGN KEY ("asset_id") REFERENCES "asset"("_id") ON DELETE CASCADE ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "td_object_textures_texture" ADD CONSTRAINT "FK_f8e2a8d67181a1959dc4989769a" FOREIGN KEY ("tDObjectId") REFERENCES "TDObject"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "td_object_textures_texture" ADD CONSTRAINT "FK_14e60e31de8aea4216f284c7c0e" FOREIGN KEY ("texture_id") REFERENCES "texture"("_id") ON DELETE CASCADE ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "structure_assets_asset" ADD CONSTRAINT "FK_afd6dc8b3bc401dd318f44acfe2" FOREIGN KEY ("structureId") REFERENCES "structure"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "structure_assets_asset" ADD CONSTRAINT "FK_6eec03e8ad80d3e80058021ed93" FOREIGN KEY ("asset_id") REFERENCES "asset"("_id") ON DELETE CASCADE ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "structure_assets_asset" DROP CONSTRAINT "FK_6eec03e8ad80d3e80058021ed93"`);
        await queryRunner.query(`ALTER TABLE "structure_assets_asset" DROP CONSTRAINT "FK_afd6dc8b3bc401dd318f44acfe2"`);
        await queryRunner.query(`ALTER TABLE "td_object_textures_texture" DROP CONSTRAINT "FK_14e60e31de8aea4216f284c7c0e"`);
        await queryRunner.query(`ALTER TABLE "td_object_textures_texture" DROP CONSTRAINT "FK_f8e2a8d67181a1959dc4989769a"`);
        await queryRunner.query(`ALTER TABLE "td_object_assets_asset" DROP CONSTRAINT "FK_01ac4ab7ee862d2159c18482171"`);
        await queryRunner.query(`ALTER TABLE "td_object_assets_asset" DROP CONSTRAINT "FK_faaa50d743735bafa1caf510828"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "transformation" SET DEFAULT '{{0,0,0},{0,0,0},{0,0,0}}'`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD CONSTRAINT "UQ_66b42e40df550bd6cdba6e5c4de" UNIQUE ("modelId")`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "modelId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" ALTER COLUMN "version" SET DEFAULT '{1,1,1}'`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de" FOREIGN KEY ("modelId") REFERENCES "model"("_id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "status"`);
        await queryRunner.query(`DROP TYPE "TDObject_status_enum"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "tdobjectId" uuid`);
        await queryRunner.query(
            `CREATE TYPE "public"."model_status_enum" AS ENUM('unfinished', 'submitted', 'declined', 'fixed', 'approved')`,
        );
        await queryRunner.query(`ALTER TABLE "model" ADD "status" "model_status_enum" NOT NULL DEFAULT 'unfinished'`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "structureId" uuid`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "tdobjectId" uuid`);
        await queryRunner.query(`DROP INDEX "IDX_6eec03e8ad80d3e80058021ed9"`);
        await queryRunner.query(`DROP INDEX "IDX_afd6dc8b3bc401dd318f44acfe"`);
        await queryRunner.query(`DROP TABLE "structure_assets_asset"`);
        await queryRunner.query(`DROP INDEX "IDX_14e60e31de8aea4216f284c7c0"`);
        await queryRunner.query(`DROP INDEX "IDX_f8e2a8d67181a1959dc4989769"`);
        await queryRunner.query(`DROP TABLE "td_object_textures_texture"`);
        await queryRunner.query(`DROP INDEX "IDX_01ac4ab7ee862d2159c1848217"`);
        await queryRunner.query(`DROP INDEX "IDX_faaa50d743735bafa1caf51082"`);
        await queryRunner.query(`DROP TABLE "td_object_assets_asset"`);
        await queryRunner.query(
            `ALTER TABLE "texture" ADD CONSTRAINT "FK_fe6f78da89a25574b736cd872cd" FOREIGN KEY ("tdobjectId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "asset" ADD CONSTRAINT "FK_8f6cc0c5ebf13585d2a01426477" FOREIGN KEY ("tdobjectId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "asset" ADD CONSTRAINT "FK_15de9ac7ab04da87311e4d0c00c" FOREIGN KEY ("structureId") REFERENCES "structure"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }
}
