import { MigrationInterface, QueryRunner } from 'typeorm';

export class TDObjectAddLinkToNewerOnes1593620031956 implements MigrationInterface {
    name = 'TDObjectAddLinkToNewerOnes1593620031956';

    // created for a 2-way searching in versions, now you can get even newer versions listed (until better way is found)
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "nextId" uuid`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD CONSTRAINT "UQ_4202444bd701be66f56473836b3" UNIQUE ("nextId")`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_4202444bd701be66f56473836b3" FOREIGN KEY ("nextId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_4202444bd701be66f56473836b3"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "UQ_4202444bd701be66f56473836b3"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "nextId"`);
    }
}
