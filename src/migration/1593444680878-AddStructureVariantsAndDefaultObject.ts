import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddStructureVariantsAndDefaultObject1593444680878 implements MigrationInterface {
    name = 'AddStructureVariantsAndDefaultObject1593444680878';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "structure" ADD "defaultObjectId" uuid`);
        await queryRunner.query(`ALTER TABLE "structure" ADD CONSTRAINT "UQ_194493b9e3b435b7a57bd9da24a" UNIQUE ("defaultObjectId")`);
        await queryRunner.query(
            `ALTER TABLE "structure" ADD CONSTRAINT "FK_194493b9e3b435b7a57bd9da24a" FOREIGN KEY ("defaultObjectId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_2892276fb85cf7dc726d7689a74" FOREIGN KEY ("structureId") REFERENCES "structure"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_2892276fb85cf7dc726d7689a74"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP CONSTRAINT "FK_194493b9e3b435b7a57bd9da24a"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP CONSTRAINT "UQ_194493b9e3b435b7a57bd9da24a"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "defaultObjectId"`);
    }
}
