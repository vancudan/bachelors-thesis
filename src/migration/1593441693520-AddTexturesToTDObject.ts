import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddTexturesToTDObject1593441693520 implements MigrationInterface {
    name = 'AddTexturesToTDObject1593441693520';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "texture" ADD "tdobjectId" uuid`);
        await queryRunner.query(
            `ALTER TABLE "texture" ADD CONSTRAINT "FK_fe6f78da89a25574b736cd872cd" FOREIGN KEY ("tdobjectId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "texture" DROP CONSTRAINT "FK_fe6f78da89a25574b736cd872cd"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "tdobjectId"`);
    }
}
