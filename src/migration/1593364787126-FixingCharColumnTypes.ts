import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixingCharColumnTypes1593364787126 implements MigrationInterface {
    name = 'FixingCharColumnTypes1593364787126';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_2892276fb85cf7dc726d7689a74"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "filename" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "text"`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "text" character varying(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "effect" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "effect" ADD "name" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "effect" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "effect" ADD "description" character varying(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "map" DROP COLUMN "locality"`);
        await queryRunner.query(`ALTER TABLE "map" ADD "locality" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "filename" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "permission" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "permission" ADD "description" character varying(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "plugin" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "plugin" ADD "name" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "plugin" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "plugin" ADD "description" character varying(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "property" DROP COLUMN "key"`);
        await queryRunner.query(`ALTER TABLE "property" ADD "key" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "property" DROP COLUMN "value"`);
        await queryRunner.query(`ALTER TABLE "property" ADD "value" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP CONSTRAINT "FK_6ac420c80527f93119a309d2e08"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_b329d37177582f70c4d93ac77af"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "PK_78a916df40e02a9deb1c4b75edb"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "PK_78a916df40e02a9deb1c4b75edb" PRIMARY KEY ("username")`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "name" character varying(50) NOT NULL`);
        await queryRunner.query(`DROP INDEX "IDX_c8b1148ac5be5a544faba06c5d"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "city"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "city" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "name" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "description" character varying(1000) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "href" character varying(250)`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "username" character varying(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "filename" character varying(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "href" character varying(250)`);
        await queryRunner.query(`CREATE INDEX "IDX_c8b1148ac5be5a544faba06c5d" ON "structure" ("city") `);
        await queryRunner.query(
            `ALTER TABLE "structure" ADD CONSTRAINT "FK_6ac420c80527f93119a309d2e08" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_b329d37177582f70c4d93ac77af" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_b329d37177582f70c4d93ac77af"`);
        await queryRunner.query(`ALTER TABLE "structure" DROP CONSTRAINT "FK_6ac420c80527f93119a309d2e08"`);
        await queryRunner.query(`DROP INDEX "IDX_c8b1148ac5be5a544faba06c5d"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "filename" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "username" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "username" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "description" character(1000) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "name" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "structure" DROP COLUMN "city"`);
        await queryRunner.query(`ALTER TABLE "structure" ADD "city" character(50) NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_c8b1148ac5be5a544faba06c5d" ON "structure" ("city") `);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "name" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "PK_78a916df40e02a9deb1c4b75edb"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "username"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "username" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "PK_78a916df40e02a9deb1c4b75edb" PRIMARY KEY ("username")`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_b329d37177582f70c4d93ac77af" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "structure" ADD CONSTRAINT "FK_6ac420c80527f93119a309d2e08" FOREIGN KEY ("username") REFERENCES "user"("username") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(`ALTER TABLE "property" DROP COLUMN "value"`);
        await queryRunner.query(`ALTER TABLE "property" ADD "value" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "property" DROP COLUMN "key"`);
        await queryRunner.query(`ALTER TABLE "property" ADD "key" character(50) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "plugin" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "plugin" ADD "description" character(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "plugin" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "plugin" ADD "name" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "permission" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "permission" ADD "description" character(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "filename" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "map" DROP COLUMN "locality"`);
        await queryRunner.query(`ALTER TABLE "map" ADD "locality" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "effect" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "effect" ADD "description" character(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "effect" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "effect" ADD "name" character(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "text"`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "text" character(250) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "href"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "href" character(250)`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "filename"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "filename" character(100) NOT NULL`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_2892276fb85cf7dc726d7689a74" FOREIGN KEY ("structureId") REFERENCES "structure"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }
}
