import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddingAssetTo3DObject1593384143752 implements MigrationInterface {
    name = 'AddingAssetTo3DObject1593384143752';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" ADD "tdobjectId" uuid`);
        await queryRunner.query(
            `ALTER TABLE "asset" ADD CONSTRAINT "FK_8f6cc0c5ebf13585d2a01426477" FOREIGN KEY ("tdobjectId") REFERENCES "TDObject"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "FK_8f6cc0c5ebf13585d2a01426477"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "tdobjectId"`);
    }
}
