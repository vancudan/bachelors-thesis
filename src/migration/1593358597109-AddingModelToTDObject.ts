import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddingModelToTDObject1593358597109 implements MigrationInterface {
    name = 'AddingModelToTDObject1593358597109';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "modelId" integer`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD CONSTRAINT "UQ_66b42e40df550bd6cdba6e5c4de" UNIQUE ("modelId")`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de" FOREIGN KEY ("modelId") REFERENCES "model"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "UQ_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "modelId"`);
    }
}
