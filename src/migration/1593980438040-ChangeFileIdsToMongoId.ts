import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeFileIdsToMongoId1593980438040 implements MigrationInterface {
    name = 'ChangeFileIdsToMongoId1593980438040';

    public async up(queryRunner: QueryRunner): Promise<void> {
        // dropping constraints so that the table ids can be dropped and created anew ( == changed)
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "UQ_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "model" RENAME CONSTRAINT "model_pk" TO "PK_47f00b41fde7b041c0493b3c3fc"`);
        await queryRunner.query(`ALTER TABLE "model" DROP CONSTRAINT "PK_47f00b41fde7b041c0493b3c3fc"`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "id" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" ADD CONSTRAINT "PK_47f00b41fde7b041c0493b3c3fc" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "model" RENAME COLUMN "id" TO "_id"`);
        await queryRunner.query(`ALTER TABLE "texture" RENAME CONSTRAINT "texture_pk" TO "PK_35cdcfe627a701e087ed5d03f20"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP CONSTRAINT "PK_35cdcfe627a701e087ed5d03f20"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "id" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" ADD CONSTRAINT "PK_35cdcfe627a701e087ed5d03f20" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "texture" RENAME COLUMN "id" TO "_id"`);
        await queryRunner.query(`ALTER TABLE "asset" RENAME CONSTRAINT "asset_pk" TO "PK_7e7500d060b233dbafa7e9cccdd"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "PK_7e7500d060b233dbafa7e9cccdd"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "id" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "asset" ADD CONSTRAINT "PK_7e7500d060b233dbafa7e9cccdd" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "asset" RENAME COLUMN "id" TO "_id"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "modelId"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "modelId" character varying`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD CONSTRAINT "UQ_66b42e40df550bd6cdba6e5c4de" UNIQUE ("modelId")`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de" FOREIGN KEY ("modelId") REFERENCES "model"("_id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP CONSTRAINT "UQ_66b42e40df550bd6cdba6e5c4de"`);
        await queryRunner.query(`ALTER TABLE "TDObject" DROP COLUMN "modelId"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD "modelId" integer`);
        await queryRunner.query(`ALTER TABLE "asset" RENAME COLUMN "_id" TO "id"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP CONSTRAINT "PK_7e7500d060b233dbafa7e9cccdd"`);
        await queryRunner.query(`ALTER TABLE "asset" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "asset" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "asset" ADD CONSTRAINT "PK_7e7500d060b233dbafa7e9cccdd" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "asset" RENAME CONSTRAINT "PK_7e7500d060b233dbafa7e9cccdd" TO "asset_pk"`);
        await queryRunner.query(`ALTER TABLE "texture" RENAME COLUMN "_id" TO "id"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP CONSTRAINT "PK_35cdcfe627a701e087ed5d03f20"`);
        await queryRunner.query(`ALTER TABLE "texture" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "texture" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "texture" ADD CONSTRAINT "PK_35cdcfe627a701e087ed5d03f20" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "texture" RENAME CONSTRAINT "PK_35cdcfe627a701e087ed5d03f20" TO "texture_pk"`);
        await queryRunner.query(`ALTER TABLE "model" RENAME COLUMN "_id" TO "id"`);
        await queryRunner.query(`ALTER TABLE "model" DROP CONSTRAINT "PK_47f00b41fde7b041c0493b3c3fc"`);
        await queryRunner.query(`ALTER TABLE "model" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "model" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "model" ADD CONSTRAINT "PK_47f00b41fde7b041c0493b3c3fc" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "model" RENAME CONSTRAINT "PK_47f00b41fde7b041c0493b3c3fc" TO "model_pk"`);
        await queryRunner.query(`ALTER TABLE "TDObject" ADD CONSTRAINT "UQ_66b42e40df550bd6cdba6e5c4de" UNIQUE ("modelId")`);
        await queryRunner.query(
            `ALTER TABLE "TDObject" ADD CONSTRAINT "FK_66b42e40df550bd6cdba6e5c4de" FOREIGN KEY ("modelId") REFERENCES "model"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }
}
