// tslint:disable-next-line:no-namespace
declare namespace Express {
    interface Request {
        user: import('./entity/User').User;
        token: string;
    }
}
