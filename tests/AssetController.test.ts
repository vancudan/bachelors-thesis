import request from 'supertest';
import app from '../src/app';
import { AssetsUrl } from '../src/routes/Urls';
import { Services, setupRandomUser, setupServices, teardownServices } from './common';

describe('Asset Controller test', () => {
    let services: Services;
    let token: string;

    beforeAll(async () => {
        services = await setupServices();
        token = await setupRandomUser(services.connection);
    });

    afterAll(async () => {
        await teardownServices(services);
    });

    it('list all test', (done) => {
        request(app)
            .get(AssetsUrl)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                // this way we can chain the expects ("asserts")
                expect(res.status).toBe(200);
                done();
            });
    });

    // TODO
});
