import request from 'supertest';
import app from '../src/app';
import { Structure } from '../src/entity/Structure';
import { TDObject } from '../src/entity/TDObject';
import { Texture } from '../src/entity/Texture';
import { StructureUrl, TDObjectsUrl, TexturesUrl } from '../src/routes/Urls';
import { TDObjectFormat } from '../src/utils/MimeType';
import { createStructure, Services, setupAdminUser, setupServices, teardownServices } from './common';

describe('Texture Controller test', () => {
    let services: Services;
    let token: string;
    let username: string;
    let name: string;
    let texture: Texture;
    let structure: Structure;
    let tdobject: TDObject;
    let textureFilename1: string;
    let textureFilename2: string;

    beforeAll(async () => {
        username = 'user_for_texture_testing';
        name = 'Random Guy';
        services = await setupServices();
        token = await setupAdminUser(username, name, services.connection);
        structure = await createStructure(
            'A city',
            'A random tower',
            'A description for testing',
            username,
            services.connection,
        );
        textureFilename1 = 'kropatschka_LOD1_AO.png';
        textureFilename2 = 'kropatschka_LOD1_BaseColor.png';
    });

    afterAll(async () => {
        await teardownServices(services);
    });

    it('Create new 3DObject - for Texture testing', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(token, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/Kropatschka01.FBX`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                // the rest of checking for correct values is done in TDObjectController.test
                expect(res.status).toBe(201);
                done();
            });
    });

    it('Add a texture file to the 3DObject', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}/textures`)
            .auth(token, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${textureFilename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                texture = res.body;
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(textureFilename1);
                done();
            });
    });

    it('list all test - no auth provided', (done) => {
        request(app)
            .get(TexturesUrl)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(401);
                done();
            });
    });

    it('list all test - there should be 1 texture created', (done) => {
        request(app)
            .get(TexturesUrl)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.length).toBe(1);
                done();
            });
    });

    it('Download texture test - id is not MongoID', (done) => {
        request(app)
            .get(`${TexturesUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Download texture test - id is MongoId, but not of an existing record', (done) => {
        request(app)
            .get(`${TexturesUrl}/5f043f0c0f6a0ba655d33133`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Download texture test - all ok', (done) => {
        request(app)
            .get(`${TexturesUrl}/${texture.id}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Update texture test - id is not MongoID', (done) => {
        request(app)
            .put(`${TexturesUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Update texture test - id is MongoId, but not of an existing record', (done) => {
        request(app)
            .put(`${TexturesUrl}/5f043f0c0f6a0ba655d33133`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Update texture test - file not attached', (done) => {
        request(app)
            .put(`${TexturesUrl}/${texture.id}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Update texture test - all ok', (done) => {
        request(app)
            .put(`${TexturesUrl}/${texture.id}`)
            .auth(token, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${textureFilename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Check the 3DObject created -> should contain the texture -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${tdobject.id}/${tdobject.version}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.textures.length).toEqual(1);
                done();
            });
    });

    it('Delete texture test - id is not MongoID', (done) => {
        request(app)
            .delete(`${TexturesUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Delete texture test - id is MongoId, but not of an existing record', (done) => {
        request(app)
            .delete(`${TexturesUrl}/5f043f0c0f6a0ba655d33133`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Delete texture test - all ok', (done) => {
        request(app)
            .delete(`${TexturesUrl}/${texture.id}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(204);
                done();
            });
    });

    it('list all textures - no more textures should be present', (done) => {
        request(app)
            .get(TexturesUrl)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.length).toBe(0);
                done();
            });
    });

    it('Check the 3DObject created -> should not contain the deleted texture -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${tdobject.id}/${tdobject.version}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.textures.length).toEqual(0);
                done();
            });
    });
});
