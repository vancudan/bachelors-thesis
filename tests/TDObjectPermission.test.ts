import request from 'supertest';
import app from '../src/app';
import { Structure } from '../src/entity/Structure';
import { TDObject } from '../src/entity/TDObject';
import { UserRole, UserStatus } from '../src/entity/User';
import { StructureUrl, TDObjectsUrl } from '../src/routes/Urls';
import { TDObjectFormat } from '../src/utils/MimeType';
import {
    createStructure,
    Services,
    setupAdminUser,
    setupServices,
    setupUser,
    setupUserWithRoleAndStatus,
    teardownServices,
} from './common';

describe('Permission access', () => {
    let services: Services;

    let newUserToken: string;
    let commonUserToken: string;
    let modellerToken: string;
    let approverToken: string;
    let historianToken: string;
    let deletedAdminToken: string;
    let adminToken: string;

    let usernameNewUser: string;
    let usernameCommonUser: string;
    let usernameModeller: string;
    let usernameApprover: string;
    let usernameHistorian: string;
    let usernameAdmin: string;
    let deletedUsername: string;

    let filename1: string;
    let filename2: string;
    let filename3: string;
    let filename4: string;
    let texture1: string;
    let texture2: string;

    let structure: Structure;

    let modellerTDObject: TDObject;
    let approverTDObject: TDObject;
    let historianTDObject: TDObject;
    let adminTDObject: TDObject;

    beforeAll(async () => {
        services = await setupServices();

        usernameNewUser = 'newUserUsername';
        usernameCommonUser = 'activeCommonUserUsername';
        usernameModeller = 'activeModellerUsername';
        usernameApprover = 'activeApproverUsername';
        usernameHistorian = 'activeHistorianUsername';
        usernameAdmin = 'activeAdminUsername';
        deletedUsername = 'deletedAdminUsername';

        commonUserToken = await setupUser(usernameCommonUser, 'common user name', services.connection);
        adminToken = await setupAdminUser(usernameAdmin, 'admin name', services.connection);
        newUserToken = await setupUserWithRoleAndStatus(usernameNewUser, 'new user name',
                                    UserRole.COMMON, UserStatus.NEW, services.connection);
        modellerToken = await setupUserWithRoleAndStatus(usernameModeller, 'modeller name',
                                    UserRole.MODELLER, UserStatus.ACTIVE, services.connection);
        approverToken = await setupUserWithRoleAndStatus(usernameApprover, 'approver name',
                                    UserRole.APPROVER, UserStatus.ACTIVE, services.connection);
        historianToken = await setupUserWithRoleAndStatus(usernameHistorian, 'historian name',
                                    UserRole.HISTORIAN, UserStatus.ACTIVE, services.connection);
        deletedAdminToken = await setupUserWithRoleAndStatus(deletedUsername, 'deleted admin name',
                                    UserRole.ADMIN, UserStatus.DELETED, services.connection);

        filename1 = 'Kropatschka01.FBX';
        filename2 = 'kropatschka_LOD1.FBX';
        filename3 = 'kropatschka_LOD2.FBX';
        filename4 = 'kropatschka_LOD3.FBX';
        texture1 = 'kropatschka_LOD1_AO.png';
        texture2 = 'kropatschka_LOD1_BaseColor.png';

        structure = await createStructure(
            'A city',
            'A random tower',
            'A description for testing',
            usernameAdmin,
            services.connection,
        );
    });

    afterAll(async () => {
        await teardownServices(services);
    });

    it('Create new 3DObject - new user (not active) -> 403', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(newUserToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX - new user created',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Create new 3DObject - common active user -> 403', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(commonUserToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX - common user created',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Create new 3DObject - modeller -> 201', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX - modeller created',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                modellerTDObject = res.body;
                expect(res.status).toBe(201);
                done();
            });
    });

    it('Create new 3DObject - approver -> 201', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX - approver created',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                approverTDObject = res.body;
                expect(res.status).toBe(201);
                done();
            });
    });

    it('Create new 3DObject - historian -> 201', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX - historian created',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename3}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                historianTDObject = res.body;
                expect(res.status).toBe(201);
                done();
            });
    });

    it('Create new 3DObject - deleted user (even though it is admin) -> 403', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(deletedAdminToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX - deleted admin',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Create new 3DObject - admin -> 201', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX - admin created',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                adminTDObject = res.body;
                expect(res.status).toBe(201);
                done();
            });
    });

    it('Add an asset file to the 3DObject - new user (not active) -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/assets`)
            .auth(newUserToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an asset file to the 3DObject - common active user -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/assets`)
            .auth(commonUserToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an asset file to the 3DObject - modeller but not author -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${approverTDObject.id}/assets`)
            .auth(modellerToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an asset file to the 3DObject - modeller -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/assets`)
            .auth(modellerToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${filename2}`);
                done();
            });
    });

    it('Add an asset file to the 3DObject - approver but not author -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/assets`)
            .auth(approverToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an asset file to the 3DObject - approver -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${approverTDObject.id}/assets`)
            .auth(approverToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${filename2}`);
                done();
            });
    });

    it('Add an asset file to the 3DObject - historian but not author -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/assets`)
            .auth(historianToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an asset file to the 3DObject - historian -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${historianTDObject.id}/assets`)
            .auth(historianToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${filename2}`);
                done();
            });
    });

    it('Add an asset file to the 3DObject - deleted user (admin role) -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/assets`)
            .auth(deletedAdminToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename3}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an asset file to the 3DObject - admin (is not author) -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/assets`)
            .auth(adminToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename3}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${filename3}`);
                done();
            });
    });

    it('Add an asset file to the 3DObject - admin -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${adminTDObject.id}/assets`)
            .auth(adminToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${filename2}`);
                done();
            });
    });

    it('Add an texture file to the 3DObject - new user (not active) -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/textures`)
            .auth(newUserToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an texture file to the 3DObject - common active user -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/textures`)
            .auth(commonUserToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an texture file to the 3DObject - modeller but not author -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${approverTDObject.id}/textures`)
            .auth(modellerToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an texture file to the 3DObject - modeller -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/textures`)
            .auth(modellerToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${texture1}`);
                done();
            });
    });

    it('Add an texture file to the 3DObject - approver but not author -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/textures`)
            .auth(approverToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an texture file to the 3DObject - approver -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${approverTDObject.id}/textures`)
            .auth(approverToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${texture1}`);
                done();
            });
    });

    it('Add an texture file to the 3DObject - historian but not author -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/textures`)
            .auth(historianToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an texture file to the 3DObject - historian -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${historianTDObject.id}/textures`)
            .auth(historianToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${texture1}`);
                done();
            });
    });

    it('Add an texture file to the 3DObject - deleted user (admin role) -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/textures`)
            .auth(deletedAdminToken, { type: 'bearer' })
            .attach('textures', `${__dirname}/textures/${texture2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Add an texture file to the 3DObject - admin (is not author) -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}/textures`)
            .auth(adminToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${texture2}`);
                done();
            });
    });

    it('Add an texture file to the 3DObject - admin -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${adminTDObject.id}/textures`)
            .auth(adminToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${texture1}`);
                done();
            });
    });

    it('List all versions of a 3DObject - new user-> 403', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(newUserToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('List all versions of a 3DObject - not new user (common user is enough) -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(commonUserToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Upload a new version of a 3DObject - new user (not active) -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(newUserToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Upload a new version of a 3DObject - common active user -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(commonUserToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Upload a new version of a 3DObject - modeller but not author -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${approverTDObject.id}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                approverTDObject = res.body; // to keep track of newest version
                done();
            });
    });

    it('Upload a new version of a 3DObject - approver but not author -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                modellerTDObject = res.body; // to keep track of newest version
                done();
            });
    });

    it('Upload a new version of a 3DObject - historian but not author -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                modellerTDObject = res.body;
                done();
            });
    });

    it('Upload a new version of a 3DObject - admin (is not author) -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                modellerTDObject = res.body; // to keep track of newest version
                done();
            });
    });

    it('Upload a new version of a 3DObject - modeller -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                modellerTDObject = res.body; // to keep track of newest version
                done();
            });
    });

    it('Upload a new version of a 3DObject - approver -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${approverTDObject.id}`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                approverTDObject = res.body; // to keep track of newest version
                done();
            });
    });

    it('Upload a new version of a 3DObject - historian -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${historianTDObject.id}`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                historianTDObject = res.body; // to keep track of newest version
                done();
            });
    });

    it('Upload a new version of a 3DObject - deleted user (admin role) -> 403', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${modellerTDObject.id}`)
            .auth(deletedAdminToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Upload a new version of a 3DObject - admin -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${adminTDObject.id}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                adminTDObject = res.body; // to keep track of newest version
                done();
            });
    });

    it('GET a version of 3DObject - new user (not active) -> 403', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(newUserToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('GET a version of 3DObject - any non-new user -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(commonUserToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Update a version of 3DObject - new user (not active) -> 403', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(newUserToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Update a version of 3DObject - common active user -> 403', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(commonUserToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Update a version of 3DObject - modeller but not author -> 403', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${approverTDObject.id}/${approverTDObject.version}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Update a version of 3DObject - modeller -> 200', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Update a version of 3DObject - approver but not author -> 403', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Update a version of 3DObject - approver -> 200', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${approverTDObject.id}/${approverTDObject.version}`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Update a version of 3DObject - historian but not author -> 403', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Update a version of 3DObject - historian -> 200', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${historianTDObject.id}/${historianTDObject.version}`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Update a version of 3DObject - deleted user (admin role) -> 403', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(deletedAdminToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Update a version of 3DObject - admin (is not author) -> 200', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${modellerTDObject.id}/${modellerTDObject.version}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Update a version of 3DObject - admin -> 200', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${adminTDObject.id}/${adminTDObject.version}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'updated Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify([]),
                'textures': JSON.stringify([]),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Delete a version of 3DObject - non-active user -> 403', (done) => {
        request(app)
            .delete(`${TDObjectsUrl}/${adminTDObject.id}/${adminTDObject.version}`)
            .auth(newUserToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Delete a version of 3DObject - non-admin user -> 403', (done) => {
        request(app)
            .delete(`${TDObjectsUrl}/${adminTDObject.id}/${adminTDObject.version}`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Delete a version of 3DObject - active admin -> 204', (done) => {
        request(app)
            .delete(`${TDObjectsUrl}/${adminTDObject.id}/${adminTDObject.version}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(204);
                done();
            });
    });
    // TODO check all permissions
    //      approval process + comments
});
