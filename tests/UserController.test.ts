import request from 'supertest';
import app from '../src/app';
import { User, UserRole, UserStatus } from '../src/entity/User';
import { UsersUrl } from '../src/routes/Urls';
import { Services, setupAdminUser, setupRandomUser, setupServices, setupUser, teardownServices } from './common';

describe('User Controller test', () => {
    let services: Services;
    let token: string;
    let adminToken: string;
    let createdUser: User;

    beforeAll(async () => {
        services = await setupServices();
        token = await setupRandomUser(services.connection);
        adminToken = await setupAdminUser('xchludil', 'Jiří Chludil', services.connection);
    });

    afterAll(async () => {
        await teardownServices(services);
    });

    it('list all test -> 200', (done) => {
        request(app)
            .get(UsersUrl)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('create new user - missing name -> 400', (done) => {
        request(app)
            .post(UsersUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'username': 'randomUsername',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('create new user - missing username -> 400', (done) => {
        request(app)
            .post(UsersUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'name': 'random name',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('create new user - all ok -> 201', (done) => {
        request(app)
            .post(UsersUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'name': 'random name',
                'username': 'randomUsername',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                createdUser = res.body;
                expect(res.status).toBe(201);
                done();
            });
    });

    it('create new user - already existing username -> 400', (done) => {
        request(app)
            .post(UsersUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'name': 'random name',
                'username': 'randomUsername',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('retrieve user details - username is not alphanumeric -> some garbage -> 400', (done) => {
        request(app)
            .get(`${UsersUrl}/asd{\#&@`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('retrieve user details - non-existent username -> 404', (done) => {
        request(app)
            .get(`${UsersUrl}/nonExistentUsername`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('retrieve user details - all ok -> 200', (done) => {
        request(app)
            .get(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('update user - username is used already -> 400', (done) => {
        setupUser('randomUsedUsername','Random User 15',  services.connection);
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'new name',
                'username': 'randomUsedUsername',
                'role': createdUser.role,
                'status': createdUser.status,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('update user - missing username -> 400', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'new name',
                'role': createdUser.role,
                'status': createdUser.status,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('update user - missing name -> 400', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'username': 'randomUsedUsername',
                'role': createdUser.role,
                'status': createdUser.status,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('update user - missing role -> 400', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'new name',
                'username': 'NewUsername',
                'status': createdUser.status,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('update user - missing status -> 400', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'new name',
                'username': 'NewUsername',
                'role': createdUser.role,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('update user - username is not alphaNumeric -> 400', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'new name',
                'username': 'random username',
                'role': createdUser.role,
                'status': createdUser.status,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('update user - all data present, role & status not changed -> 200', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'new name',
                'username': 'NewUsername',
                'role': createdUser.role,
                'status': createdUser.status,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                createdUser = res.body;
                expect(res.body.username).toEqual('NewUsername');
                expect(res.body.name).toEqual('new name');
                expect(res.body.role).toEqual(UserRole.COMMON);
                expect(res.body.status).toEqual(UserStatus.NEW);
                done();
            });
    });

    it('update user - all data present, changing role & status, but user is not admin -> 403', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(token, { type: 'bearer' })
            .field({
                'name': 'new name',
                'username': 'NewUsername',
                'role': UserRole.APPROVER,
                'status': UserStatus.ACTIVE,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('update user - all data present, admin is changing role & status, but role & status are garbage -> 400', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'yet another name',
                'username': 'anotherUsername',
                'role': 'garbage role',
                'status': 'garbage status',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('update user - all data present, admin is changing role & status -> 200', (done) => {
        request(app)
            .put(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'yet another name',
                'username': 'anotherUsername',
                'role': UserRole.APPROVER,
                'status': UserStatus.ACTIVE,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                createdUser = res.body;
                expect(res.body.username).toEqual('anotherUsername');
                expect(res.body.name).toEqual('yet another name');
                expect(res.body.role).toEqual(UserRole.APPROVER);
                expect(res.body.status).toEqual(UserStatus.ACTIVE);
                done();
            });
    });

    it('delete non-existent user -> 404', (done) => {
        request(app)
            .delete(`${UsersUrl}/nonExistentUsername`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('delete user -> 204 (only logical delete, the user status is changed to deleted)', (done) => {
        request(app)
            .delete(`${UsersUrl}/${createdUser.username}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(204);
                done();
            });
    });
});
