import { Server } from 'http';
import { sign } from 'jsonwebtoken';
import { MongoClient } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Connection, createConnection } from 'typeorm';
import app from '../src/app';
import injector from '../src/di/Injector';
import { Structure } from '../src/entity/Structure';
import { User, UserRole, UserStatus } from '../src/entity/User';
import { StructureUrl } from '../src/routes/Urls';

// Create TypeORM database connections pool
async function initTypeORM() {
    return createConnection();
}

// Create MongoDB database connection to a MongoDB in memory server
async function initMongoDB() {
    const mongoMemoryServer = new MongoMemoryServer();
    const mongoClient = new MongoClient(await mongoMemoryServer.getUri(), { useNewUrlParser: true, useUnifiedTopology: true });
    injector.registerServiceInstance(MongoClient.name, mongoClient);
    return {
        mongoMemoryServer,
        mongoClient: await mongoClient.connect(),
    };
}

// Start Express server
async function initExpress() {
    return new Promise<Server>((resolve) => {
        const server = app.listen(() => resolve(server));
    });
}

export interface Services {
    connection: Connection;
    mongoMemoryServer: MongoMemoryServer;
    mongoClient: MongoClient;
    server: Server;
}

export async function setupServices(): Promise<Services> {
    const { mongoClient, mongoMemoryServer } = await initMongoDB();
    return {
        connection: await initTypeORM(),
        mongoMemoryServer,
        mongoClient,
        server: await initExpress(),
    };
}

export async function teardownServices({ connection, mongoMemoryServer, mongoClient, server }: Services) {
    // Close all connections
    await connection.close();
    await mongoClient.close();
    await server.close();

    // Stop MongoDB in memory server
    await mongoMemoryServer.stop();
}

function randomString(length: number): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i += 1) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

export async function setupRandomUser(connection: Connection): Promise<string> {
    return setupUser(randomString(15), 'Test User', connection);
}

export async function setupAdminUser(username: string, name: string, connection: Connection): Promise<string> {
    return setupUserWithRoleAndStatus(username, name, UserRole.ADMIN, UserStatus.ACTIVE, connection);
}

export async function setupUser(username: string, name: string, connection: Connection): Promise<string> {
    return setupUserWithRoleAndStatus(username, name, UserRole.COMMON, UserStatus.ACTIVE, connection);
}

export async function setupUserWithRoleAndStatus(username: string,
                                                 name: string,
                                                 role: string,
                                                 status: string,
                                                 connection: Connection): Promise<string> {
    // Create JWT
    const secret = process.env.JWT_SECRET!;
    const token = sign({ username }, secret);

    // Save user into the database
    const user = new User();
    user.username = username;
    user.name = name;
    user.token = token;
    user.status = status;
    user.role = role;
    await connection.manager.save(user);

    return token;
}

export async function createStructure(
        city: string,
        name: string,
        description: string,
        username: string,
        connection: Connection): Promise<Structure> {
    let structure = new Structure();
    structure.city = city;
    structure.name = name;
    structure.description = description;
    structure.username = username;

    structure = await connection.manager.save(structure);
    structure.href = `${StructureUrl}/${structure.id}`;
    return connection.manager.save(structure);
}
