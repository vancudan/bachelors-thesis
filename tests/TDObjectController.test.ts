import request from 'supertest';
import app from '../src/app';
import { Structure } from '../src/entity/Structure';
import { TDObject } from '../src/entity/TDObject';
import { StructureUrl, TDObjectsUrl } from '../src/routes/Urls';
import { TDObjectFormat } from '../src/utils/MimeType';
import {
    createStructure,
    Services,
    setupAdminUser,
    setupServices,
    teardownServices,
} from './common';

describe('3D Object Controller test - endpoints behaviour (is as intended?, admin access, validation)', () => {
    let services: Services;
    let adminToken: string;
    let username: string;
    let name: string;
    let structure: Structure;
    let tdobject: TDObject;
    let newVersion: TDObject;
    let newestVersion: TDObject;
    let filename1: string;
    let filename2: string;
    let filename3: string;
    let filename4: string;
    let filenameGlb: string;
    let filenameGltf: string;
    let texture1: string;

    beforeAll(async () => {
        filename1 = 'Kropatschka01.FBX';
        filename2 = 'kropatschka_LOD1.FBX';
        filename3 = 'kropatschka_LOD2.FBX';
        filename4 = 'kropatschka_LOD3.FBX';
        filenameGlb = 'Fox.glb';
        filenameGltf = 'kropatschka_GLB.GLTF';
        texture1 = 'kropatschka_LOD1_AO.png';
        username = 'test_3DObjects_username';
        name = 'Test user';
        services = await setupServices();
        adminToken = await setupAdminUser(username, name, services.connection);
        structure = await createStructure(
            'A city',
            'A random tower',
            'A description for testing',
            username,
            services.connection,
        );
    });

    afterAll(async () => {
        await teardownServices(services);
    });

    it('should return an error when user is not authenticated -> 401', (done) => {
        request(app)
            .get(TDObjectsUrl)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(401);
                expect(res.text).toBe('Access token is missing or invalid');
                done();
            });
    });

    it('Create new 3DObject - missing name -> 400', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .field({
                'format': TDObjectFormat.FBX,
            })
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Create new 3DObject - missing structureId -> structures//3dobjects -> 404', (done) => {
        request(app)
            .post(`${StructureUrl}/${TDObjectsUrl}`)
            .field({
                'format': TDObjectFormat.FBX,
                'name': 'Kropatschka',
            })
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Create new 3DObject - no model file attached -> 400', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Create new 3DObject - non-existent structure (structureId) -> 404', (done) => {
        request(app)
            .post(`${StructureUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e${TDObjectsUrl}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
                'name': 'Kropatschka - FBX',
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Create new 3DObject - all ok -> 201', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(201);
                expect(tdobject.version).toBe('1.1.1');
                expect(tdobject.structureId).toBe(structure.id);
                expect(tdobject.assets).toEqual([]);
                expect(tdobject.textures).toEqual([]);
                done();
            });
    });

    it('Add an asset file to the 3DObject - id is not uuid -> 400', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/some_bogus_id/assets`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('asset', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Add an asset file to the 3DObject - non existent uuid (of 3D Object) -> 404', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e/assets`)
            .auth(adminToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Add an asset file to the 3DObject - all ok -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}/assets`)
            .auth(adminToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${filename2}`);
                done();
            });
    });

    it('Add an asset file to the 3DObject - file already in this record once -> 400', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}/assets`)
            .auth(adminToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Add a texture file to the 3DObject - id is not uuid -> 400', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/some_bogus_id/textures`)
            .auth(adminToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Add a texture file to the 3DObject - non existent uuid (of 3D Object) -> 404', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e/textures`)
            .auth(adminToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Add a texture file to the 3DObject - all ok -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}/textures`)
            .auth(adminToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${texture1}`);
                done();
            });
    });

    it('Add a texture file to the 3DObject - file already in this record once -> 400', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}/textures`)
            .auth(adminToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Get details of a structure - 3DObject with a texture and an asset were added (should be default object) -> 200', (done) => {
        request(app)
            .get(`${StructureUrl}/${structure.id}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                structure = res.body;
                expect(res.status).toBe(200);
                expect(res.body.id).toEqual(structure.id);
                // this expect is here mainly to check if all relevant info were loaded (& sent)
                expect(res.body.defaultObject.textures[0]).toEqual(res.body.allVariants[0].textures[0]);
                done();
            });
    });

    it('List all versions of a 3DObject -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${tdobject.id}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                const versions = res.body;
                expect(res.status).toBe(200);
                // as for right now there's just 1 version of the 3D object
                expect(versions.length).toBe(1);
                expect(versions[0].id).toBe(tdobject.id);
                done();
            });
    });

    // TODO check the data -> textures and assets?
    it('Upload a new version of a 3DObject -> only new model file -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename3}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                newVersion = res.body;

                expect(res.status).toBe(200);
                done();
            });
    });

    it('List all versions of a 3DObject after one new is created -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newVersion.id}`) // so all it's versions are listed
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                const versions = res.body;
                expect(res.status).toBe(200);
                expect(versions.length).toBe(2);
                expect(versions[0].version).toBe('1.1.2'); // aka the results are sorted by version from newest
                expect(versions[1].version).toBe('1.1.1');
                expect(versions[0].id).toBe(newVersion.id);
                expect(versions[1].id).toBe(tdobject.id);
                done();
            });
    });

    it('List all versions of a 3DObject - the 3DObject is not the newest one -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${tdobject.id}`) // so all it's versions are listed
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                const versions = res.body;
                expect(res.status).toBe(200);
                expect(versions.length).toBe(2);
                expect(versions[0].version).toBe('1.1.2');
                expect(versions[1].version).toBe('1.1.1');
                expect(versions[0].id).toBe(newVersion.id);
                expect(versions[1].id).toBe(tdobject.id);
                done();
            });
    });

    it('Upload a new version of a 3DObject -> on a non-current one -> 400', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}`)
            .auth(adminToken, { type: 'bearer' })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Upload a new version of a 3DObject -> so there is 3 of them - for better testing -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${newVersion.id}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                newestVersion = res.body;
                expect(res.status).toBe(200);
                expect(newestVersion.version).toBe('1.1.3');
                done();
            });
    });

    it('List all versions of a 3DObject - check the 2nd out of 3, should return all in order -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newVersion.id}`) // 2nd out of 3 versions, should return both older and newer versions
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                const versions = res.body;
                expect(res.status).toBe(200);
                expect(versions.length).toBe(3);
                expect(versions[0].id).toBe(newestVersion.id);
                expect(versions[0].version).toBe(newestVersion.version); // 1.1.3
                expect(versions[1].id).toBe(newVersion.id);
                expect(versions[1].version).toBe(newVersion.version); // 1.1.2
                expect(versions[2].id).toBe(tdobject.id);
                expect(versions[2].version).toBe(tdobject.version); // 1.1.1
                done();
            });
    });

    /*
    * all 'version specific' actions are going to be performed on 'newVersion' as delete is slightly more complicated
    * delete should update the previousId and nextId of it's neighbours so getAllVersions() still finds them
    * update does not really matter, still will be used on newVersion for consistency
    * */
    it('GET a version of 3DObject - version is random garbage -> 400', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newVersion.id}/1y.5.3a4s.5yg5q`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('GET a version of 3DObject - version is too long, format a.b.c.d instead of a.b.c -> 400', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newVersion.id}/1.2.3.4`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('GET a version of 3DObject - absurdly big version number' +
        '(checked to be max: aaaa.bbbb.cccc to avoid useless selects on db) -> 400', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newVersion.id}/11111.222222.3333333333333333333`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('GET a version (non-existent) of 3DObject -> 404', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newVersion.id}/5.3.5555`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('GET a version of a non-existent 3DObject -> 404', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e/${newVersion.version}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('GET a version of 3DObject - all ok -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newVersion.id}/${newVersion.version}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Update a non-existent version of 3DObject -> 404', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/${newVersion.id}/5.3.5`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Update a specific version of a non-existent 3DObject -> 404', (done) => {
        request(app)
            .put(`${TDObjectsUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e/${newVersion.version}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    // TODO add a few tests where some of the data is missing
    it('Update a specific version of 3DObject - textures / assets are not arrays -> 400', (done) => {
        const assetsToUpdate = 'string';
        const texturesToUpdate = 2123456;
        request(app)
            .put(`${TDObjectsUrl}/${newVersion.id}/${newVersion.version}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'new version name Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify(assetsToUpdate),
                'textures': JSON.stringify(texturesToUpdate),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Update a specific version of 3DObject - textures / assets are arrays, but without any UUID ' +
        '-> result: 200 + textures & assets are empty -> non UUID fields are ignored', (done) => {
        const assetsToUpdate = ['string'];
        const texturesToUpdate = ['2ajjhcbash', 2123456, {'2key': '2value'}, ['2random', '2stuff'], 'random string aihbalcb'];
        request(app)
            .put(`${TDObjectsUrl}/${newVersion.id}/${newVersion.version}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'new version name Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify(assetsToUpdate),
                'textures': JSON.stringify(texturesToUpdate),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.textures).toEqual([]);
                expect(res.body.assets).toEqual([]);
                done();
            });
    });

    it('Update a specific version of 3DObject - all ok - previous update ' +
        'changed the textures and assets to be empty -> result: 200 + not empty', (done) => {
        const assetsToUpdate = structure.defaultObject.assets.map(((value) => value.id));
        const texturesToUpdate = structure.defaultObject.textures.map(((value) => value.id));
        request(app)
            .put(`${TDObjectsUrl}/${newVersion.id}/${newVersion.version}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'another updated version name Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify(assetsToUpdate),
                'textures': JSON.stringify(texturesToUpdate),
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.assets[0].id).toEqual(assetsToUpdate[0]);
                expect(res.body.textures[0].id).toEqual(texturesToUpdate[0]);
                done();
            });
    });

    it('Delete a non-existent version of 3DObject -> 404', (done) => {
        request(app)
            .delete(`${TDObjectsUrl}/${newVersion.id}/5.3.5`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Delete a specific version of a non-existent 3DObject -> 404', (done) => {
        request(app)
            .delete(`${TDObjectsUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e/${newVersion.version}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Delete a specific version of 3DObject - all ok -> 204', (done) => {
        request(app)
            .delete(`${TDObjectsUrl}/${newVersion.id}/${newVersion.version}`)
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(204);
                done();
            });
    });

    it('List all versions of a 3DObject - check the deleted one, should not work -> 404', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newVersion.id}`) // 2nd out of 3 versions, should return both older and newer versions
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('List all versions of a 3DObject - check the newest one, version 1.1.2 should be deleted -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}/${newestVersion.id}`) // 2nd out of 3 versions, should return both older and newer versions
            .auth(adminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                const versions = res.body;
                expect(res.status).toBe(200);
                expect(versions.length).toBe(2);
                done();
            });
    });

    it('Create new 3DObject (new format - GLB)- all ok -> 201', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - GLB',
                'format': TDObjectFormat.GLB,
            })
            .attach('model', `${__dirname}/assets/${filenameGlb}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(201);
                done();
            });
    });

    it('Create new 3DObject (new format - GLTF)- all ok -> 201', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - GLTF',
                'format': TDObjectFormat.GLTF,
            })
            .attach('model', `${__dirname}/assets/${filenameGltf}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(201);
                done();
            });
    });

});
