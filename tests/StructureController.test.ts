import request from 'supertest';
import app from '../src/app';
import { Structure } from '../src/entity/Structure';
import { StructureUrl } from '../src/routes/Urls';
import { createStructure, Services, setupAdminUser, setupServices, teardownServices } from './common';

describe('Structure Controller test', () => {
    let services: Services;
    let token: string;
    let token2: string;
    let username: string;
    let name: string;
    let structure: Structure;

    beforeAll(async () => {
        username = 'test_structures_username';
        name = 'User for Checking Structures';
        services = await setupServices();
        token = await setupAdminUser(username, name, services.connection);
        token2 = await setupAdminUser('another_user', 'random name', services.connection);
        structure = await createStructure(
            'A city',
            'A random tower',
            'A description for testing',
            'another_user',
            services.connection,
        );
    });

    afterAll(async () => {
        await teardownServices(services);
    });

    it('Create new structure - all correct but not authenticated -> 401', (done) => {
        request(app)
            .post(StructureUrl)
            .field({
                'city': 'Random Town',
                'name': 'Basic Kropatschka tower',
                'description': 'Just some random tower, nothing particularly interesting.',
                'location': JSON.stringify({'lon': 51.421,'lat': 38.457,'alt': 200}),
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(401);
                done();
            });
    });

    it('Create new structure, location is missing a value -> 400', (done) => {
        request(app)
            .post(StructureUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'city': 'Random Town',
                'name': 'Basic Kropatschka tower',
                'description': 'Just some random tower, nothing particularly interesting.',
                'location': JSON.stringify({'lon': 51.421,'lat': 38.457}),
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Create new structure, location is having non numbers as values -> 400', (done) => {
        request(app)
            .post(StructureUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'city': 'Random Town',
                'name': 'Basic Kropatschka tower',
                'description': 'Just some random tower, nothing particularly interesting.',
                'location': JSON.stringify({'lon': 51.421,'lat': 38.457, 'alt': 'random altitude'}),
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Create new structure - missing required data - city -> 400', (done) => {
        request(app)
            .post(StructureUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'name': 'Basic Kropatschka tower',
                'description': 'Just some random tower, nothing particularly interesting.',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Create new structure - missing required data - name -> 400', (done) => {
        request(app)
            .post(StructureUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'city': 'Random Town',
                'description': 'Just some random tower, nothing particularly interesting.',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Create new structure - missing required data - description -> 400', (done) => {
        request(app)
            .post(StructureUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'city': 'Random Town',
                'name': 'Basic Kropatschka tower',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Create new structure - with provided location (GPS) -> 201', (done) => {
        // when using .field and/or .attach you must NOT set 'Content-type' - it'll be set automatically
        request(app)
            .post(StructureUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'city': 'Random Town',
                'name': 'Just some Basic Kropatschka tower',
                'description': 'Just some random tower, nothing particularly interesting.',
                'location': JSON.stringify({'lon': 51.421,'lat': 38.457,'alt': 200}),
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(201);
                expect(res.body.city).toBe('Random Town');
                expect(res.body.name).toBe('Just some Basic Kropatschka tower');
                expect(res.body.description).toBe('Just some random tower, nothing particularly interesting.');
                expect(res.body.location).toEqual({
                    'lon': 51.421,
                    'lat': 38.457,
                    'alt': 200,
                });
                done();
            });
    });

    it('Create new structure, all required data present (no location) -> 201', (done) => {
        request(app)
            .post(StructureUrl)
            .auth(token2, { type: 'bearer' })
            .field({
                'city': 'Random Town',
                'name': 'Basic Kropatschka tower',
                'description': 'Just some random tower, nothing particularly interesting.',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(201);
                expect(res.body.location).toBe(null);
                done();
            });
    });

    it('Create new structure - location has additional garbage keys - only lon lat alt are taken -> 201', (done) => {
        request(app)
            .post(StructureUrl)
            .auth(token, { type: 'bearer' })
            .field({
                'city': 'Random Town',
                'name': 'Dat Kropatschka tower',
                'description': 'Just some random tower, nothing particularly interesting.',
                'location': JSON.stringify({'lon': 51.421,'lat': 38.457,'alt': 200, 'key': 20.154, 'garbage': 'total'}),
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(201);
                expect(res.body.location).toEqual({
                    'lon': 51.421,
                    'lat': 38.457,
                    'alt': 200,
                });
                done();
            });
    });

    it('Add an asset file to a Structure - id is not uuid -> 400', (done) => {
        request(app)
            .post(`${StructureUrl}/some_bogus_id/assets`)
            .auth(token, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/Kropatschka01.FBX`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Add an asset file to a Structure - non existent uuid (of Structure) -> 404', (done) => {
        request(app)
            .post(`${StructureUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e/assets`)
            .auth(token, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/Kropatschka01.FBX`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Add an asset file to a Structure - normal -> 200', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}/assets`)
            .auth(token, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/Kropatschka01.FBX`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe('Kropatschka01.FBX');
                done();
            });
    });

    it('Get details of a structure - asset was added and should be present -> 200', (done) => {
        request(app)
            .get(`${StructureUrl}/${structure.id}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                const struct = res.body;
                expect(res.status).toBe(200);
                expect(struct.id).toEqual(structure.id);
                // this expect is here mainly to check if all relevant info were loaded (& sent)
                expect(struct.assets.length).toEqual(1);
                expect(struct.assets[0].filename).toEqual('Kropatschka01.FBX');
                structure = struct;
                done();
            });
    });

    it('Get list of all structures', (done) => {
        request(app)
            .get(StructureUrl)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                // as of 4 structures were created via POST above
                expect(res.body.length).toEqual(4);
                // structures are ordered by name - first is the structure created in 'BeforeAll'
                expect(res.body[0].id).toEqual(structure.id);
                done();
            });
    });

    it('Get list of all structures - with username in query -> 200', (done) => {
        request(app)
            .get(StructureUrl)
            .auth(token, { type: 'bearer' })
            .query({'username': 'another_user'})
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.length).toEqual(2);
                // as the user with token2 was the one who created it
                expect(res.body[0].id).toEqual(structure.id);
                done();
            });
    });

    it('Get list of all structures - with username but PUBLIC ONLY (aka none now) -> 200 but empty', (done) => {
        request(app)
            .get(StructureUrl)
            .auth(token, { type: 'bearer' })
            .query({'username': username, 'public': true})
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                // there shouldn't be any 'public' (aka approved) models
                expect(res.body.length).toEqual(0);
                done();
            });
    });

    it('Get list of all structures - non string query -> 400', (done) => {
        request(app)
            .get(StructureUrl)
            .auth(token, { type: 'bearer' })
            .query({'username': {'troll': 'dict'}})
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                // 'query[username]: Invalid value'
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Get list of all structures - non existent username -> 200 but empty', (done) => {
        request(app)
            .get(StructureUrl)
            .auth(token, { type: 'bearer' })
            .query({'username': 'non_existent_username'})
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.length).toEqual(0);
                done();
            });
    });

    it('Get details of a non-existent structure (bad ID format) -> 400', (done) => {
        request(app)
            .get(`${StructureUrl}/randomId`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Get details of a non-existent structure (ID is uuid) -> 404', (done) => {
        request(app)
            .get(`${StructureUrl}/23f0790e-ad73-4bca-9655-3ed44672987c`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Get details of a structure - all ok -> 200', (done) => {
        request(app)
            .get(`${StructureUrl}/${structure.id}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.id).toEqual(structure.id);
                done();
            });
    });

    it('Update a non-existent structure (bad ID format) -> 400', (done) => {
        request(app)
            .put(`${StructureUrl}/invalidId`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Update a non-existent structure (ID is uuid) -> 404', (done) => {
        request(app)
            .put(`${StructureUrl}/23f0790e-ad73-4bca-9655-3ed44672987c`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Update a structure - all ok -> 200', (done) => {
        const assetsToUpdate = structure.assets.map(((value) => value.id));
        request(app)
            .put(`${StructureUrl}/${structure.id}`)
            .auth(token2, { type: 'bearer' })
            .field({
                'city': 'Updated Random Town',
                'name': 'Updated Kropatschka tower',
                'description': 'Updated tower',
                'location': JSON.stringify({'lon': 50.123,'lat': 30.123,'alt': 150}),
                'assets': JSON.stringify(assetsToUpdate),
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.id).toEqual(structure.id);
                expect(res.body.city).toEqual('Updated Random Town');
                expect(res.body.name).toEqual('Updated Kropatschka tower');
                expect(res.body.description).toEqual('Updated tower');
                expect(res.body.location).toEqual({'lon': 50.123,'lat': 30.123,'alt': 150});
                done();
            });
    });

    it('Get details of a structure - after successful update -> 200', (done) => {
        request(app)
            .get(`${StructureUrl}/${structure.id}`)
            .auth(token2, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.id).toEqual(structure.id);
                expect(res.body.city).toEqual('Updated Random Town');
                done();
            });
    });

    it('Delete a non-existent structure (bad ID format) -> 400', (done) => {
        request(app)
            .delete(`${StructureUrl}/invalidId`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Delete a non-existent structure (ID is uuid) -> 404', (done) => {
        request(app)
            .delete(`${StructureUrl}/23f0790e-ad73-4bca-9655-3ed44672987c`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Delete a structure - all ok -> 204', (done) => {
        request(app)
            .delete(`${StructureUrl}/${structure.id}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(204);
                done();
            });
    });

    it('Get details of a structure - after successful delete -> 404', (done) => {
        request(app)
            .get(`${StructureUrl}/${structure.id}`)
            .auth(token, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    /*
    * TODO create a structure, add 3DObject, check that defaultObject is set
    *  then try update the structure, delete the 3DObject etc. and see how it behaves
    * */
});
