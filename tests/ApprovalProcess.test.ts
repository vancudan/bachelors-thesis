import request from 'supertest';
import app from '../src/app';
import { Structure } from '../src/entity/Structure';
import { TDObject, TDObjectStatus } from '../src/entity/TDObject';
import { UserRole, UserStatus } from '../src/entity/User';
import { StructureUrl, TDObjectsUrl } from '../src/routes/Urls';
import { TDObjectFormat } from '../src/utils/MimeType';
import {
    createStructure,
    Services, setupAdminUser,
    setupServices, setupUser, setupUserWithRoleAndStatus,
    teardownServices,
} from './common';

describe('Approval process test', () => {
    let services: Services;

    let newUserToken: string;
    let commonUserToken: string;
    let modellerToken: string;
    let approverToken: string;
    let historianToken: string;
    let deletedAdminToken: string;
    let adminToken: string;

    let usernameNewUser: string;
    let usernameCommonUser: string;
    let usernameModeller: string;
    let usernameApprover: string;
    let usernameHistorian: string;
    let deletedUsername: string;
    let usernameAdmin: string;

    let filename1: string;
    let filename2: string;
    let filename3: string;
    let filename4: string;
    let texture1: string;
    /*let texture2: string;
    let texture3: string;*/

    let structure: Structure;
    let tdobject: TDObject;

    beforeAll(async () => {
        services = await setupServices();

        usernameNewUser = 'newUserUsername';
        usernameCommonUser = 'activeCommonUserUsername';
        usernameModeller = 'activeModellerUsername';
        usernameApprover = 'activeApproverUsername';
        usernameHistorian = 'activeHistorianUsername';
        deletedUsername = 'deletedAdminUsername';
        usernameAdmin = 'activeAdminUsername';

        adminToken = await setupAdminUser(usernameAdmin, 'admin name', services.connection);
        commonUserToken = await setupUser(usernameCommonUser, 'common user name', services.connection);
        newUserToken = await setupUserWithRoleAndStatus(usernameNewUser, 'new user name',
            UserRole.COMMON, UserStatus.NEW, services.connection);
        modellerToken = await setupUserWithRoleAndStatus(usernameModeller, 'modeller name',
            UserRole.MODELLER, UserStatus.ACTIVE, services.connection);
        approverToken = await setupUserWithRoleAndStatus(usernameApprover, 'approver name',
            UserRole.APPROVER, UserStatus.ACTIVE, services.connection);
        historianToken = await setupUserWithRoleAndStatus(usernameHistorian, 'historian name',
            UserRole.HISTORIAN, UserStatus.ACTIVE, services.connection);
        deletedAdminToken = await setupUserWithRoleAndStatus(deletedUsername, 'deleted admin name',
            UserRole.ADMIN, UserStatus.DELETED, services.connection);

        filename1 = 'Kropatschka01.FBX';
        filename2 = 'kropatschka_LOD1.FBX';
        filename3 = 'kropatschka_LOD2.FBX';
        filename4 = 'kropatschka_LOD3.FBX';

        texture1 = 'kropatschka_LOD1_AO.png';
        /*texture2 = 'kropatschka_LOD1_BaseColor.png';
        texture3 = 'kropatschka_LOD1_Normal.png';*/

        structure = await createStructure(
            'A city',
            'A random tower',
            'A description for testing',
            usernameModeller,
            services.connection,
        );
    });

    afterAll(async () => {
        await teardownServices(services);
    });

    it('Create new 3DObject - all ok -> 201', (done) => {
        request(app)
            .post(`${StructureUrl}/${structure.id}${TDObjectsUrl}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'name': 'Kropatschka - FBX',
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(201);
                expect(tdobject.version).toBe('1.1.1');
                expect(tdobject.structureId).toBe(structure.id);
                expect(tdobject.assets).toEqual([]);
                expect(tdobject.textures).toEqual([]);
                expect(tdobject.status).toBe(TDObjectStatus.UNFINISHED);
                done();
            });
    });

    it('Add an asset file to the 3DObject - all ok -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}/assets`)
            .auth(modellerToken, { type: 'bearer' })
            .attach('asset', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${filename2}`);
                done();
            });
    });

    it('Add a texture file to the 3DObject - all ok -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}/textures`)
            .auth(modellerToken, { type: 'bearer' })
            .attach('texture', `${__dirname}/textures/${texture1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(res.body.filename).toBe(`${texture1}`);
                done();
            });
    });

    it('Upload a new version of a 3DObject -> only new model file -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename3}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;

                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.UNFINISHED);
                done();
            });
    });

    it('Send tdobject to be approved - garbage id -> 400', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/random-stuff-id/approval`)
            .auth(approverToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Send tdobject to be approved - non-existent one -> 404', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/5f25291a-9a35-4b56-a1be-babd39c7a59e/approval`)
            .auth(approverToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(404);
                done();
            });
    });

    it('Send tdobject to be approved - new user (not activated) -> 403', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(newUserToken, { type: 'bearer' })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Send tdobject to be approved - common user (read only permissions) -> 403', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(commonUserToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Send tdobject to be approved - deleted user (even though its admin) -> 403', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(deletedAdminToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Send tdobject to be approved - non author (+ non-admin) trying it -> 403', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(403);
                done();
            });
    });

    it('Send tdobject to be approved - modeller -> (status changed to \"submitted\") -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.SUBMITTED);
                done();
            });
    });

    it('Upload a new version of a 3DObject (previous submission was a mistake) -> 200', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.version).toBe('1.1.3');
                done();
            });
    });

    it('Send tdobject to be approved - modeller -> \"submitted\" + 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.SUBMITTED);
                done();
            });
    });

    it('Historian lists 3D objects waiting for his approval -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}`)
            .auth(historianToken, { type: 'bearer' })
            .query({'waitingForApproval': true})
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Approver lists 3D objects waiting for his approval -> 200', (done) => {
        request(app)
            .get(`${TDObjectsUrl}`)
            .auth(approverToken, { type: 'bearer' })
            .query({'waitingForApproval': true})
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                done();
            });
    });

    it('Approver approves the model - missing role -> 400', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'approve': 'true',
                'text': 'no technical problems whatsoever with this version',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Approver approves the model - missing approve -> 400', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'text': 'no technical problems whatsoever with this version',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Historian tries to approve the model as approver == invalid data -> 400', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': true,
                'text': 'no technical problems whatsoever with this version',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Approver tries to approve the model as historian == invalid data -> 400', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': true,
                'text': 'no problems whatsoever with this version',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Approver approves the model (no technical errors)-> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': true,
                'text': 'no technical problems whatsoever with this version',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.SUBMITTED); // one approve does not change the status
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

    it('Approver approves the same model twice without change -> 400', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': true,
                'text': 'no technical problems whatsoever with this version',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                // "You already approved this model, wait for modeller and historian to fix their issues first"
                done();
            });
    });

    it('Historian declined the model -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': false,
                'text': 'windows are too close together, and some more issues',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }

                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                done();
            });
    });

    it('Historian approves the model he declined before -> 200 but model status stays DECLINED', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': true,
                'text': 'declined by mistake',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED);
                // once declined == it stays as DECLINED until modeller submits it again
                expect(tdobject.isApprovedByHistorian).toBe(true);
                done();
            });
    });

    it('Historian declines the model again (noone touched it in the meantime) -> 200 + still DECLINED', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': false,
                'text': 'previous approval was accidental (different TDDObject)',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED);
                // once declined == it stays as DECLINED until modeller submits it again
                expect(tdobject.isApprovedByHistorian).toBe(false);
                done();
            });
    });

    it('Modeller uploads a new version of a 3DObject -> 200 but stays DECLINED', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename1}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.version).toBe('1.1.4');
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED);
                done();
            });
    });

    it('Update a specific version of 3DObject ' +
        '(modeller wants to change the object status but it will not do) -> 200 + still DECLINED', (done) => {
        const assetsToUpdate: string[] = tdobject.assets.map(((value) => value.id)); // UUIDs
        const texturesToUpdate: string[] = tdobject.textures.map(((value) => value.id)); // UUIDs
        request(app)
            .put(`${TDObjectsUrl}/${tdobject.id}/${tdobject.version}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'name': 'new version name Kropatschka FBX',
                'format': TDObjectFormat.FBX,
                'assets': JSON.stringify(assetsToUpdate),
                'textures': JSON.stringify(texturesToUpdate),
                'status': TDObjectStatus.APPROVED,
            })
            .attach('model', `${__dirname}/assets/${filename4}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED);
                done();
            });
    });

    it('Send fixed tdobject to be approved -> \"fixed\" + 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.FIXED);
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

    it('Send tdobject to be approved again (no change, was in status FIXED) -> 400', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Approver approves the model after fix but he approved before' +
        '(should wait before the issues between other two are resolved) -> 400', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': true,
                'text': 'no technical problems whatsoever with this version',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                expect(res.status).toBe(400);
                done();
            });
    });

    it('Historian declines the fix -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': false,
                'text': 'roof is broken',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

    it('Modeller insists on it being right (sends for approval without change) -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.FIXED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

    it('Historian accepts the fix -> 200. Status changes to SUBMITTED and approvers approval of' +
        'previous version is nullified -> so he can control the new version.', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': true,
                'text': 'roof is broken',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.SUBMITTED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(true);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Approver declines the fix (there are some technical errors) -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': false,
                'text': 'errors in mesh on coords xyz',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(true);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Modeller fixes & sends for approval -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.FIXED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(true);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Approver approves the fix -> 200. Status changes to SUBMITTED and historians approval of' +
        'previous version is nullified -> so he can control the new version.', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': true,
                'text': 'all good now',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.SUBMITTED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

    it('Historian approves the model -> 200 + APPROVED', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': true,
                'text': 'is ok',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.APPROVED);
                expect(tdobject.isApprovedByHistorian).toBe(true);
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

    it('Modeller creates a new version (after some time a mistake was found) -> 200' +
        '+ status changed from APPROVED to UNFINISHED', (done) => {
        request(app)
            .post(`${TDObjectsUrl}/${tdobject.id}`)
            .auth(modellerToken, { type: 'bearer' })
            .field({
                'format': TDObjectFormat.FBX,
            })
            .attach('model', `${__dirname}/assets/${filename2}`)
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.version).toBe('1.1.5');
                expect(tdobject.status).toBe(TDObjectStatus.UNFINISHED);
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Modeller cannot be found so admin sends model for approval in his name -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(adminToken, { type: 'bearer' })
            .field({
                'role': UserRole.MODELLER,
                'approve': true,          // whatever is here doesn't matter in this case, it just has to be present
                'text': 'admin sent model for approval',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.SUBMITTED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Approver declines the new model -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': false,
                'text': 'errors in mesh on coords xyz',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Historian also declines the new model -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': false,
                'text': 'roof is broken',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Modeller fixes & sends for approval -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.FIXED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Historian declines again -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': false,
                'text': 'roof is broken',
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Approver accepts the fix -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': true,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.DECLINED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

    it('Modeller fixes again -> 200', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(modellerToken, { type: 'bearer' })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.FIXED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(false);
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

    it('Historian accepts the fix -> 200, status-> SUBMITTED, waiting for approver', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(historianToken, { type: 'bearer' })
            .field({
                'role': UserRole.HISTORIAN,
                'approve': true,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.SUBMITTED);
                expect(tdobject.isApprovedByHistorian).toBe(true);
                expect(tdobject.isApprovedByApprover).toBe(false);
                done();
            });
    });

    it('Approver is also satisfied -> 200, status:APPROVED', (done) => {
        request(app)
            .patch(`${TDObjectsUrl}/${tdobject.id}/approval`)
            .auth(approverToken, { type: 'bearer' })
            .field({
                'role': UserRole.APPROVER,
                'approve': true,
            })
            .end( (err, res) => {
                if (err) {
                    done.fail(err);
                }
                tdobject = res.body;
                expect(res.status).toBe(200);
                expect(tdobject.status).toBe(TDObjectStatus.APPROVED); // one approve does not change the status
                expect(tdobject.isApprovedByHistorian).toBe(true);
                expect(tdobject.isApprovedByApprover).toBe(true);
                done();
            });
    });

});
